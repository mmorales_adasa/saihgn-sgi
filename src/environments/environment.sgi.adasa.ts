export const environment = {
  production: false,
 // partial: false,
  RESTAPI_BASE_URL: 'http://10.10.5.176/wservices/index.php',  
  CAS_SERVER: 'https://10.10.5.176:8443/cas',
  URL_SIRA : 'https://saihguadiana.com:3443',
  // Enlaces a otras aplicaciones
  // Nombres de los servicios en CAS
  CAS_SERVICE_SGI : 'sgi',
  BY_PASS_SECURITY : false,
  
}; 