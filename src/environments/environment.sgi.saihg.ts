export const environment = {
  production: false,
  URL_REDIRECT_ERROR : 'https://saihguadiana.com/visor',
  RESTAPI_BASE_URL: 'https://saihguadiana.com/backend',
  CAS_SERVER: 'https://saihguadiana.com:8444/cas',
  URL_SIRA : 'https://saihguadiana.com',
  // Enlaces a otras aplicaciones  
  // Nombres de los servicios en CAS
  CAS_SERVICE_SGI : 'sgi',
  VISOR_SERVICE : 'visor',
  BY_PASS_SECURITY : false,
  
}; 