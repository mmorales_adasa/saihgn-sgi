export const environment = {
  production: false,
  //partial: false,
  RESTAPI_BASE_URL: 'http://127.0.0.1:80/back-end',
  CAS_SERVER: 'https://10.10.5.176:8443/cas',
  URL_SIRA : 'https://saihguadiana.com:3443',
  //URL_REDIRECT_ERROR : 'https://saihguadiana.com:3443/visor',
  //RESTAPI_BASE_URL: 'https://saihguadiana.com:3443/backend',
  //CAS_SERVER: 'https://saihguadiana.com:8444/cas',

   // Enlaces a otras aplicaciones
  // URL_PORTAL: 'http://localhost/home', 
  //URL_SGI: 'http://10.10.5.176/sgi',
  // Nombres de los servicios en CAS
  CAS_SERVICE_SGI : 'sgidev',
  VISOR_SERVICE : 'visordev',
  BY_PASS_SECURITY : true

  
}; 