
export const constants = {
  DEFAULT_LIMIT_PAGINATION: 50,
  NAME_COOKIE_SESSION : 'ticketCAS',
  VALIDITY_SESION_DAYS : 3,  
  /*WEB: { 
    ROUTE_PREFIX: 'intranet',
  },  */
  SGI: {
    COD_APP: 'SGI',
    ROUTE_PREFIX: '', 
    ROOT: 'adasa',
    NIVEL_ACCESO_PUBLIC : 0,
    ENTITIES: {
      CONSTANTES: 'CONSTANTES',
      ESTACIONES: 'ESTACIONES',
      PROCESOS : 'PROCESOS',
      OPERACIONES : 'OPERACIONES',
      ESTADO_ESTACIONES: 'ESTADO_ESTACIONES',
      TIPO_ESTACIONES: 'TIPO_ESTACIONES',
      COMUNIDAD: 'COMUNIDAD',
      PROVINCIA: 'PROVINCIA',
      COMARCA: 'COMARCA',
      MUNICIPIO: 'MUNICIPIO',
      VARIABLES: 'VARIABLES',
      ZONAS: 'ZONAS',
      AREAS: 'AREAS',
      TIPO_CONSTANTES: 'TIPOS_CONSTANTES',
      MODELOS: 'MODELOS',
      ESCENARIOS: 'ESCENARIOS',
      SISTEMAS: 'SISTEMAS',
      VALOR_REFERENCIA: 'VALOR_REFERENCIA',
      MODEL_NIVEL_TEMPORAL: 'MODEL_NIVEL_TEMPORAL',
      MODEL_INDICADOR_VARIABLE: 'MODEL_INDICADOR_VARIABLE',
      MODEL_INDICADOR_UMBRAL: 'MODEL_INDICADOR_UMBRAL',
      MODEL_UMBRAL_COMPUESTO: 'MODEL_UMBRAL_COMPUESTO',
      MODEL_VALORES_UMBRAL: 'MODEL_VALORES_UMBRAL',
      SISTEMAS_ESCENARIOS: 'SISTEMAS_ESCENARIOS',
      VAR_CALCULADAS: 'VAR_CALCULADAS',
      CURVAS_GASTO: 'CURVAS_GASTO',
      MOTIVOS_INVALIDACION: 'MOTIVOS_INVALIDACION',
      TIPOS_VARIABLE: 'TIPOS_VARIABLE',
      UNIDADES: 'UNIDADES',
      MODEL_VARIABLE_REF: 'MODEL_VARIABLE_REF',
      ORIGENES: 'ORIGENES',
      SUBCUENCAS: 'SUBCUENCAS',
      USUARIOS: 'USUARIOS',
      NIVELES_ACCESO : 'NIVELES_ACCESO',
      ELEMENTOS_VISOR : 'ELEMENTOS_VISOR',
      ROLES : 'ROLES',
      ESTACIONES_GENERAL : 'ESTACIONES_GENERAL',
      TIPO_ESTACIONES_GENERAL : 'TIPO_ESTACIONES_GENERAL',
      BIBLIOTECA_UBICACIONES : 'BIBLIOTECA_UBICACIONES',
      GEST_VALIDACION : 'GEST_VALIDACION',                                                                                                                     
      GEST_SUMARIZACION : 'GEST_SUMARIZACION',                                                                                                                     
      CAMARAS : 'CAMARAS',           
      ESQUEMAS : 'CAMARAS',           
      INFORMES : 'INFORMES',           
      
      // Entidades de la otra base de datos (YA HAN PASADO A LA NUEVA BB.DD)
      SONDAS_AQUADAM_27 : 'SONDAS_AQUADAM_27',
      DATOS_SONDAS_AQUADAM_27 : 'DATOS_SONDAS_AQUADAM_27',
      ESTACIONES_27 : 'ESTACIONES_27',
      ESTACIONES_PIEZO_27 : 'ESTACIONES_PIEZO_27',                                  
      VARIABLES_27 : 'VARIABLES_27', 
      VARIABLES_PIEZO_27 : 'VARIABLES_PIEZO_27', 
      MOTIVOS_INVALIDACION_27: 'MOTIVOS_INVALIDACION_27',
    },
    TIPO_RED_VALIDACION : {
        NORMAL: 1,
        SONDAS_MULTI: 2,
        AUSCULTACION : 'E',
        SAIHG : 'CR',
        REACAR : 'ECC',
        SAICA : 'EAA',
        PIEZO: 'PZ'
    },
    TYPE_STATION: {
      CR : "CR",
    },
    SECTIONS: {
      LOGIN: 'login',
      RECOVER: 'recover',
      HOME: 'home',
      VALIDACION_MANUAL: 'vm',
      EDICION_CONFIRMACION: 'ec',
      CONFIGURACION: 'conf',
      MONITORIZACION: 'mto',
      MODELOS_REFERENCIA: 'mr',
      ESQUEMAS: 'mesq',
      INFORMES: 'minf',
      VARIABLES_CALCULADAS: 'vc',
      CURVAS_GASTO: 'cg',
      USUARIOS: 'use',
      ROLES: 'role'
    },
    PERIODICIDAD_DATO:
    {
      HORARIO: 'H',
      DIARIO: 'D',
    }
  }
};

export const CUSTOM_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'DD/MM/YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'DD/MM/YYYY',
  },
};
