import { Component, OnInit } from '@angular/core';
import { SecurityService } from './services/common/security.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(public _securityService: SecurityService,) {
    }
    ngOnInit() {
        if (environment.BY_PASS_SECURITY){
            var mock = {cod_usuario: 2,
            email: "adasa@adasasistemas.com",
            nombre: "Adasa",
            sgi: 0,
            pentahoSesionId: null,
            permisos: [],
            sesionId: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWFkZXIiOnsidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifSwicGF5bG9hZCI6eyJzdWIiOjIsImV4cCI6MTYxNTQ3MTMzNSwiaWF0IjoxNjE0ODY2NTM1LCJqdGkiOjF9fQ.I4vT47VNqIRI5OMaYwzTQ0255SbFytNt1XrqzjA-wwU",
            username: "adasa"            ,
            };
            
            localStorage.setItem('profile',JSON.stringify(mock));

        }
    }
}
