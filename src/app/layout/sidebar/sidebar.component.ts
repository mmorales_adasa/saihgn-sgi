import { Component, OnInit } from '@angular/core';
import { SecurityService } from './../../services/common/security.service';
import { Router } from '@angular/router';
import { constants } from '../../config/constants';
import { conf_sgi} from './../layout.configuration';
import { LayoutService } from './../layout.service';
import { environment } from 'src/environments/environment';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    profile: any;
    sections = [];    
    constructor(public _securityService: SecurityService,
        private _router: Router,
        private layoutService: LayoutService
    ) {
        this.profile = this._securityService.getProfileFromLocalSesion();        
    }
    ngOnInit() {        
        var url = this._router.url;
        //if (url.includes(constants.SGI.ROUTE_PREFIX)) {
        this.sections = conf_sgi.sections;   
       // }                      
        this.layoutService.getRefreshLayoutCompoents().subscribe(section => {
            this.profile = this._securityService.getProfileFromLocalSesion();
        });
        this.layoutService.getMenuSelected().subscribe(selected => {
            //console.log(this.sections); 
            for (var item of this.sections) {
                //console.log("comparo " + item.route.includes() + " con " + selected);
                item.selected = (item.route.endsWith(selected));
            }
            //console.log(this.sections); 
             //console.log("MENU - SELECTED" + item.route);
              this.layoutService.refreshLayoutCompoents(null);
        });
    }
    selectOption(option) {
        if (option=="/sec_sira"){
            window.open(environment.URL_SIRA, "_blank");

        }
        else{            
            this.layoutService.refreshLayoutCompoents(null);
            this.layoutService.setMenuSelected(option);
            this._router.navigate([option]);
        }
    }
}
