import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

@Injectable()
export class LayoutService {
    public sectionSelected = new Subject<any>();
    public cacheLoaded = new Subject<any>();
    
    public changeLayoutComponents   = new Subject<any>();
    public setMenuSelected(op: any) {
        this.sectionSelected.next(op);
    }
    public getMenuSelected(): Observable<any> {
        return this.sectionSelected.asObservable();
    }
    public refreshLayoutCompoents(op: any) {
        this.changeLayoutComponents.next(op);
    }
    public getRefreshLayoutCompoents(): Observable<any> {
        return this.changeLayoutComponents.asObservable();
    }
    public updateCacheLoaded(op: any) {
        this.cacheLoaded.next(op);
    }
    public getCacheLoaded(): Observable<any> {
        return this.cacheLoaded.asObservable();
    }

}
