import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { LayoutService } from './layout.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ArcaCommonModule } from '@arca/common';
import { environment } from './../../environments/environment';
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        NgbModule,        
        Ng5SliderModule,
        ArcaCommonModule.forRoot({ 
            auditoriaEndPoint: environment.RESTAPI_BASE_URL,
            laboratorioEndPoint: environment.RESTAPI_BASE_URL,
            auditaActivated: false
        }),
    ],
    providers: [

        LayoutService
    ],
    declarations: [
        LayoutComponent,
        SidebarComponent,
        HeaderComponent        
    ],
    exports: [

        LayoutComponent,
        SidebarComponent,
        HeaderComponent
    ]
})
export class LayoutModule { }
