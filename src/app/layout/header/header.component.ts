import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { LayoutService } from './../layout.service';
import { SecurityService } from './../../services/common/security.service';
import { conf_sgi, conf_sgi_home } from './../layout.configuration';

import { constants } from './../../config/constants';
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Input()
    public profile: any;
    @Input()
    public version: any;
    public title: any;
    public url: any;
    public description: any;
    constructor(public _securityService: SecurityService,
        private _layoutService: LayoutService,
        private _router: Router) {
    }
    ngOnInit() {
        this.url = this._router.url;
            this.title = conf_sgi_home.header_title;
            this.description = conf_sgi_home.titleRight;
    }
    public logout(event) {
        this._securityService.closeSesion()
    }
}
