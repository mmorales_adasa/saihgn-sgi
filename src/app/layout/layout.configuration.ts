import { constants } from '../config/constants';

export const conf_sgi_home = {
    title: "Bienvenido a ARCA",
    description: "El portal desde el que podrá consultar y gestionar <br />la información de Calidad de Aguas de la <br />Guadiana",
    titleRight: "Sistema de Gestión de la Información <br />SIRA Guadiana",
    background: 'fixedBackgroundARCA',
    header_title: "",
}
export const conf_sgi =
{
    version: "1.0.0",
    header_title: "SGI",
    title: "Bienvenido al Sistema de gestión de la información ",
    titleRight: "Sistema de gestión de la información<br/>Confederación Hidrográfica del Guadiana",
    description: "El portal desde el que podrá consultar y gestionar la<br /> información del SAIH",
    background: 'fixedBackgroundLABO',
    sections: [{
        class: "fa fa-home",
        tooltip: "Bienvenida",
        section: 'home',
        route: constants.SGI.ROUTE_PREFIX + '/' + "home",
        public: true,
        selected: true,
        disabled: false
    }, {
        class: "fa fa-check",
        tooltip: "Validación manual",
        section: 'vm',
        route: constants.SGI.ROUTE_PREFIX + '/' + "vm",
        public: true,
        selected: false,
        disabled: false
    }
        , {
        class: "fa fa-pencil  ",
        tooltip: "Edición y confirmación",
        section: 'ec', 
        route: constants.SGI.ROUTE_PREFIX + '/' + "ec",
        public: true,
        selected: false,
        disabled: false
    }
        , {
        class: "fa fa-wrench  ",
        tooltip: "Configuración",
        section: 'conf_val',
        route: constants.SGI.ROUTE_PREFIX + '/' + "conf_val",
        public: true,
        selected: false,
        disabled: false
    }
        , {
        class: "fa fa-cube",
        tooltip: "Modelos de referencia",
        section: 'val',
        route: constants.SGI.ROUTE_PREFIX + '/' + "mr_mol",
        public: true,
        selected: false,
        disabled: false
    }
        , {
        class: "fa fa-calculator",
        tooltip: "Variables y fórmulas",
        section: 'gt',
        route: constants.SGI.ROUTE_PREFIX + '/' + "vc_vc",
        public: true,
        selected: false,
        disabled: false
    }
        , {
        class: "fa fa-line-chart",
        tooltip: "Curvas de gasto",
        section: 'gt',
        route: constants.SGI.ROUTE_PREFIX + '/' + "cg",
        public: true,
        selected: false,
        disabled: false
    }
    , {
        class: "fa fa-eye",
        tooltip: "Monitorización",
        section: 'mto',
        route: constants.SGI.ROUTE_PREFIX + '/' + "mto",
        public: true,
        selected: false,
        disabled: false
    }    
        , {
        class: "fa fa-key",
        tooltip: "Gestión de la seguridad",
        section: 'sec_users',
        route: constants.SGI.ROUTE_PREFIX + '/' + "sec_users",
        public: true,
        selected: false,
        disabled: false
    } 
    , {
        class: "fa fa-globe",
        tooltip: "Acceso a SIRA",
        section: 'sec_sira',
        route: constants.SGI.ROUTE_PREFIX + '/' + "sec_sira",
        public: true,
        selected: false,
        disabled: false
    }     
    , {
        class: "fa fa-sitemap",
        tooltip: "Gestión de esquemas",
        section: 'sec_esquemas',
        route: constants.SGI.ROUTE_PREFIX + '/' + "esquemas",
        public: true,
        selected: false,
        disabled: false
    }     
    , {
        class: "fa fa-file-pdf-o",
        tooltip: "Gestión de informes",
        section: 'sec_informes',
        route: constants.SGI.ROUTE_PREFIX + '/' + "informes",
        public: true,
        selected: false,
        disabled: false
    }      
    ]
};