import { Component, OnInit } from '@angular/core';
import { SecurityService } from './../services/common/security.service';
import { LayoutService } from './layout.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';

import { constants } from './../config/constants';
import { conf_sgi, conf_sgi_home } from './layout.configuration';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    mainMenuSelectedOption = 'login';
    background = 'fixedBackground';
    showWelcome = false;
    configuration: any;
    profile: any;
    constructor(public _securityService: SecurityService,
        public activatedRoute: ActivatedRoute,
        private layoutService: LayoutService, private router: Router, private cdRef: ChangeDetectorRef) { }
    ngOnInit() {
        this.configuration = conf_sgi;
        this.layoutService.getMenuSelected().subscribe(section => {
            this.mainMenuSelectedOption = section;

            if (this.mainMenuSelectedOption.includes("login") || this.mainMenuSelectedOption.includes("home") || this.mainMenuSelectedOption.includes("recover") ) {
                this.showWelcome = true;
                this.background = this.configuration.background;

            } else {
                this.background = "fixedBackgroundClear";
                this.showWelcome = false;
            }

            if (this.mainMenuSelectedOption.includes("recover")){
                this.configuration.title = "Recuperación de contraseña"
                this.configuration.description = "Introduce el nombre de usuario que usa para entrar en el sistema. <BR> Recibirá un correo electronico con la nueva contraseña.";
            }
        });

        this.profile = this._securityService.getProfileFromLocalSesion();

    }
    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
}
