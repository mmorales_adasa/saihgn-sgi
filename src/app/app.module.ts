import { CommonModule } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { LayoutModule } from './layout/layout.module';
import { RoutesModule } from './routes/routes.module';
import { BasicEntityService } from './services/common/basic.entity.service';
import { StaticListService } from './services/common/static.list.service';
import { SecurityService } from './services/common/security.service';
import { UtilService } from './services/util/util.service';
import { CasGuard } from './guard/cas.guard';
import { MatIconModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { AuditoriaService } from '@arca/common';

import {
    DateAdapter,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE
} from '@angular/material';
export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};
export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MM YYYY',
        dateA11yLabel: 'DD/MM/YYYY',
        monthYearA11yLabel: 'MM YYYY',
    },
};

@NgModule({
    declarations: [AppComponent],
    imports: [
        MatIconModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpModule,
        HttpClientModule,
        LayoutModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        RoutesModule,
        FormsModule, ReactiveFormsModule,
        SharedModule
    ],

    providers: [
        DatePipe, AuditoriaService, SecurityService, BasicEntityService, StaticListService, UtilService,  CasGuard,{
            provide: MAT_DATE_LOCALE,
            useValue: 'es'
        }
        
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }