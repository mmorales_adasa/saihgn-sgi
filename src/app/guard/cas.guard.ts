import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SecurityService } from './../services/common/security.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { environment } from 'src/environments/environment';
import jwt_decode from "jwt-decode";
@Injectable()
export class CasGuard implements CanActivate {
    constructor(private _router: Router, private _securityService: SecurityService) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (environment.BY_PASS_SECURITY) {
            return true;
        }
        else
        {
            var profile = this._securityService.getProfileFromLocalSesion();
            if (!profile) {
                window.location.href = environment.CAS_SERVER + "/login?service=" + environment.CAS_SERVICE_SGI;
                return false;
            }
            else {
                var decoded  : any = jwt_decode(profile.sesionId);            
                if (!decoded.payload.sgi){
                    alert('No tiene permisos para acceder');
                    window.location.href = environment.CAS_SERVER + "/login?service=" + environment.CAS_SERVICE_SGI;
                    return false;
                }                 
                else{
                    return true;
                }
            }
        }
    }
}