import { Component, OnInit, Injector, ChangeDetectorRef } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { DatoBrutoService } from '../../services/arca_lab/data.bruto.service';
import { DatoDiarioService } from '../../services/arca_lab/data.diario.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutService } from './../../layout/layout.service';
import { BasicEntityService } from '../../services/common/basic.entity.service';
import { StaticListService } from '../../services/common/static.list.service';
import { SecurityService } from '../../services/common/security.service';
import { UtilService } from '../../services/util/util.service';
import { GraphService } from '../../services/util/graph.service';
import { ActivatedRoute,Router } from '@angular/router';

import {
    MAT_MOMENT_DATE_FORMATS,
    MomentDateAdapter,
    MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  } from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

@Component({
    selector: 'app-form-base-list-sgi',
    template: '',
    providers: [
        // The locale would typically be provided on the root module of your application. We do it at
        // the component level here, due to limitations of our example generation script.
        {provide: MAT_DATE_LOCALE, useValue: 'es'},
    
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
      ],    
})
export class SGIListBaseComponent implements OnInit {
    public cacheService: any;
    public listFixerService: any;
    constructor(
        private _adapter: DateAdapter<any>,
        public _injector: Injector,
        public _datoBrutoService : DatoBrutoService,
        public _datoDiarioService : DatoDiarioService,
        public _basicEntityService: BasicEntityService,
        public _staticListService : StaticListService,
        public _auditoriaService: AuditoriaService,
        public _utilService: UtilService,
        public _activatedRoute: ActivatedRoute,
        public _graphService: GraphService,
        public _cd: ChangeDetectorRef,
        public modalService: NgbModal,
        public _layoutService: LayoutService,
        public _securityService: SecurityService,
        public router: Router
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {
            return false;
        };        
    }

    ngOnInit(): any {
        this._adapter.setLocale('es');
        return new Promise((resolve, reject) => {
            resolve()
        });
    }
}