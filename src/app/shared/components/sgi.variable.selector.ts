import { Component, OnInit, Injector, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { DatoBrutoService } from '../../services/arca_lab/data.bruto.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutService } from './../../layout/layout.service';
import { BasicEntityService } from '../../services/common/basic.entity.service';
import { StaticListService } from '../../services/common/static.list.service';
import { UtilService } from '../../services/util/util.service';
import { GraphService } from '../../services/util/graph.service';


import { constants } from './../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../environments/environment';
import { SGIListBaseComponent } from '../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel } from '@arca/common';

@Component({
    selector: 'app-sgi-variable-selector',
    templateUrl: './sgi.variable.selector.html',
    styleUrls: ['./sgi.variable.selector.scss'],
})
export class SGIVariableSelector extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    @Output() onSelect = new EventEmitter<any>();

    sort: string = "desc_variable ASC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listTiposConstantes = [];
    pageSize: 20;
    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            resolve();
        });
    }
    searchData(pagination) {
        var params = { entity: constants.SGI.ENTITIES.VARIABLES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {
            if (pagination) {
                this.tablaConstantes.setDataAndLoadTable(list);
            }
            else {
                var params = { entity: constants.SGI.ENTITIES.VARIABLES, filters: this.filterSelection }
                this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                    this.tablaConstantes.setTotalElements(total);
                    this.tablaConstantes.setDataAndLoadTable(list);
                });
            }

        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "cod_variable", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_variable", filter: true, displayed: true, translation: "Desc Variable" },
            //    { id: "cod_origen", filter: true, displayed: true, translation: "Origen" },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Estacion" },

            ],
            selectableUnique: true,
            selectable: true,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    configureFilters() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.considerEmptyError = false;
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            group: 1, id: 't1.cod_estacion', label: 'Estación', searchOption: false, width: 300, disabled: false, fun: function (params) {
                return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self);
            }
        });
        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPOS_VARIABLE }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 400,
                id: 't1.cod_tipo_variable',
                label: 'Tipo Variable',
                data: list,
                code: 'cod_tipo_variable',
                desc: 'desc_tipo_variable',
                searchOption: true
            });
        });
        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPO_ESTACIONES }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 400,
                id: 'cod_tipo_estacion',
                label: 'Tipo Estación',
                data: list,
                code: 'cod_tipo_estacion',
                desc: 'desc_tipo_estacion',
                searchOption: true
            });
        });

    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(false);
    }
    onSort(event) {
        //console.log(event);
        this.sort = event.active + " " + event.direction;
        this.searchData(true);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(true);
    }
    afterExecuteCrudAction(event) {
        console.log(event);
    }
    onChecked(event) {
        this.onSelect.emit({ cod_variable: event.row.cod_variable });


    }
}
