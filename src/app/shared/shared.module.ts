import { NgModule, ModuleWithProviders } from '@angular/core';
import { SGIListBaseComponent } from './components/sgi.list.base.component';
import { SGIVariableSelector } from './components/sgi.variable.selector';
import { ArcaCommonModule } from '@arca/common';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
    imports: [  
        ArcaCommonModule,
        BrowserModule,
        CommonModule
    ],
    providers: [
    ],
    declarations: [SGIListBaseComponent,SGIVariableSelector
    ],
    exports: [
        SGIListBaseComponent,SGIVariableSelector 
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}
