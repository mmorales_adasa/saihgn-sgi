import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BaseService } from '../util/base.service';
@Injectable()
export class BasicEntityService extends BaseService {
    public getData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/BasicCrud/getData';
        return this.callPost(url, params);
    }  
    public save(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/BasicCrud/save';
        return super.callPost(url, params);
    }      
    public getDataPagination(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/BasicCrud/getDataPagination';
        return super.callPost(url, params);
    }          
    public getDataPaginationCount(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/BasicCrud/getDataPaginationCount';
        return super.callPost(url, params);
    }         
 
}
