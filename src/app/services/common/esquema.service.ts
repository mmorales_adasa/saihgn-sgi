import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BaseService } from '../util/base.service';
@Injectable()
export class EsquenaService extends BaseService {
    public getData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Esquema/getData';
        return this.callPost(url, params);
    }  
    public removeFile(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Esquema/remove';
        return this.callPost(url, params);
    }        
    public downloadFile(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Esquema/download';
        return this.callPost(url, params);
    }    
    public uploadFile(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Esquema/upload';
        return this.callPost(url, params);
    }       
}
