import { Injectable } from '@angular/core';
@Injectable()
export class StaticListService  {
    getYesNoList() {
        return [{ COD: '0', DESCR: 'No' }, { COD: '1', DESCR: 'Sí' }];
    }
    getTipoValidacionAuto() {
        return [{ COD: 'PEND', DESCR: 'Por pendiente' }, { COD: 'RANG', DESCR: 'Por Rango' }];
    }    
}
