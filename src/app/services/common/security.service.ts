import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { constants } from '../../../app/config/constants';
import AES from 'crypto-js/aes';
import Utf8 from 'crypto-js/enc-utf8';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { BaseService } from '../util/base.service';
import jwt_decode from "jwt-decode";

@Injectable()
export class SecurityService extends BaseService {
    private keyLocalStorageProfile = 'profile';
    /*constructor(public _router: Router, private _http: Http, private ngxXml2jsonService: NgxXml2jsonService) {
    }*/
    public recover(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/recover';
        return this.callPost(url, params);        
    }

    public getPermisosNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosNivel';
        return this.callPost(url, params);        
    }
    public getVideosNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getVideosNivel';
        return this.callPost(url, params);        
    }    
    public getInformesNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getInformesNivel';
        return this.callPost(url, params);        
    }      
    public getPermisosDataNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosDataNivel';
        return this.callPost(url, params);        
    }    
    /*public getPermisosDataUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosDataUsuario';
        return this.callPost(url, params);        
    }*/    
    public getPermisosDataUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosDataUsuario2';
        return this.callPost(url, params);        
    }     
    public duplicateUser(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/duplicateUser';
        return this.callPost(url, params);        
    }    
    public duplicateNivelAcceso(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/duplicateNivelAcceso';
        return this.callPost(url, params);        
    }     
    public checkVisibleElement(section) {
        var profile = this.getProfileFromLocalSesion();
        if (profile && profile.sesionId){
            var decoded  : any = jwt_decode(profile.sesionId);
            //console.log(decoded.payload);
            return (decoded.payload.elementos.includes(section));
        }
        return false;        
    }    
    public decodeJWT2(token): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/decodeJWT';
        return this.callPost(url, { jwt : token});        
    }           
    public decodeJWT(): Observable<any> {
        var profile = this.getProfileFromLocalSesion();
        var url = environment.RESTAPI_BASE_URL + '/security/decodeJWT';

        return this.callPost(url, { jwt : profile.sesionId});        
    }      
    public getPermisosUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosUsuario';
        return this.callPost(url, params);    
    } 
    public getVideosUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getVideosUsuario';
        return this.callPost(url, params);    
    }  
    public getInformesUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getInformesUsuario';
        return this.callPost(url, params);    
    }      
    public getUbicacionesUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getUbicacionesUsuario';
        return this.callPost(url, params);    
    }                  
    public getPermisosElemento(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosElemento';
        return this.callPost(url, params);    
    }   
    public getPermisosVideo(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getPermisosVideo';
        return this.callPost(url, params);    
    }    
    public getMisInformes(): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/informes/getMisInformes';
        return this.callPost(url, null);    
    }              
    public savePermisosVideo(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/savePermisosVideo';
        return this.callPost(url, params);    
    }  
    public savePermisosUbicaciones(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/savePermisosUbicaciones';
        return this.callPost(url, params);    
    }          
    public savePermisosElementoNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/savePermisosElementoNivel';
        return this.callPost(url, params);
    }  
    public savePermisosUsuario(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/savePermisosUsuario';
        return this.callPost(url, params);
    }   
    public savePermisosNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/savePermisosNivel';
        return this.callPost(url, params);
    }              
    public getServiceCASName(){
        return environment.CAS_SERVICE_SGI;     
    }

    public validateTicketCAS(ticket): Observable<any> {
        var url = environment.CAS_SERVER + '/serviceValidate?service=' + this.getServiceCASName() + "&ticket=" + encodeURIComponent(ticket);
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append("Accept", 'application/x-www-form-urlencoded');
        var ro = new RequestOptions({ headers: headers });
        return this._http.get(url, ro).pipe(
            map(res => {
                var xml = res.text();
                //console.log(xml);
                const parser = new DOMParser();
                const ps = parser.parseFromString(xml, 'text/xml');
                const obj = this.ngxXml2jsonService.xmlToJson(ps);                
                if (obj["cas:serviceResponse"]["cas:authenticationSuccess"]) {
                    var user = obj["cas:serviceResponse"]["cas:authenticationSuccess"]["cas:user"];
                    var origin = obj["cas:serviceResponse"]["cas:authenticationSuccess"]["cas:attributes"]["cas:authenticationMethod"];
                    if (origin==='LdapAuthenticationHandler'){
                        origin = 'ldap';
                    }
                    else{
                        origin = 'db';
                    }
                    return  {user : user, origin :origin}
                }
                return null;
            })
        )
    }    
    public checkTicketFromURL() {
        return new Promise((resolve, reject) => {
            var self = this;
            var url = this._router.url;
            var index = url.indexOf("ticket=");            
            if (index > 0) {                
                var ticket = url.substr(index + 7);
                self.validateTicketCAS(ticket).subscribe(data => {
                    if (data && data.user) {
                        self.startLocalSesion(data.user, data.origin, ticket).subscribe(
                            result => {
                                resolve(result);
                            },
                            error => {
                                reject("No dispone de acceso a la aplicacion " + constants.SGI.COD_APP + ". Consulte con su administrador");
                            });
                    }
                    else {
                        resolve(self.getProfileFromLocalSesion());
                    }
                });
            } else {
                console.log('nok');
                resolve(self.getProfileFromLocalSesion());
            }
        });
    }
/*
    public closeSesionCAS2(local): Observable<any> {
        var self = this;
        var url = environment.CAS_SERVER + "/logout?service=";
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append("Accept", 'application/x-www-form-urlencoded');
        var ro = new RequestOptions({ headers: headers });
        return this._http.get(url, ro).pipe(
            map(res => {
                if (local) {
                    self.closeLocalSesion();
                }
                return res;
            })
        )
    }    */
    /*public getTicketGrantingCASFromApi(username, password): Observable<any> {
        var url = environment.CAS_SERVER + '/v1/tickets';
        const body = new HttpParams()
            .set('username', username)
            .set('password', password)
            .set('service', this.getServiceCASName());
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append("Accept", 'application/x-www-form-urlencoded');
        var ro = new RequestOptions({ headers: headers });
        return this._http.post(url, body.toString(), ro).pipe(
            map(res => {
                return res;
            })
        )
    }

    public getTicketServiceCASFromApi(tgt): Observable<any> {
        var url = environment.CAS_SERVER + '/v1/tickets/' + tgt;
        const body = new HttpParams()
            .set('service', this.getServiceCASName());
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append("Accept", 'application/x-www-form-urlencoded');
        var ro = new RequestOptions({ headers: headers });
        return this._http.post(url, body.toString(), ro).pipe(
            map(res => {
                console.log(res);
                return res;
            })
        )
    }*/

    public closeLocalSesion() {
        if (localStorage[this.keyLocalStorageProfile]) {
            localStorage.removeItem(this.keyLocalStorageProfile);
        }
    }
    public startLocalSesion(username, origin, ticket ): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getProfile';
        return this._http.post(url, { COD_USUARIO: username, ORIGIN : origin, APP :  constants.SGI.COD_APP , CASTICKET : ticket  }).pipe(
            map(res => {
                localStorage[this.keyLocalStorageProfile] = JSON.stringify(res.json());
                return res.json()
            })
        )
    }
    public getVisibleSection($section) {
        /*if (localStorage[this.keyLocalStorageProfile]) {
            return JSON.parse(localStorage[this.keyLocalStorageProfile]);
        }
        return null;*/
    }    
    public getProfileFromLocalSesion() {
        if (localStorage[this.keyLocalStorageProfile]) {
            var profile = JSON.parse(localStorage[this.keyLocalStorageProfile]);        
            return profile;
           
        }
        return null;
    }
/*
    public getProfileFromServer(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getProfile';
        return this._http.post(url, params).pipe(
            map(res => { return res.json() })
        )
    } */  
    /*public closeSesionCAS(ticket, local): Observable<any> {
        var url = environment.CAS_SERVER + '/v1/tickets/' + ticket;
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append("Accept", 'application/x-www-form-urlencoded');
        var ro = new RequestOptions({ headers: headers });
        return this._http.delete(url, ro).pipe(
            map(res => {
                if (local) {
                    this.closeLocalSesion();
                }
                return res;
            })
        )
    }*/
    public closeSesion() {
        this.closeLocalSesion();      
        window.location.href = environment.CAS_SERVER + "/logout?service = " + this.getServiceCASName();
        
    }
    public goTologinCAS() {
        window.location.href = environment.CAS_SERVER + "/login?service=" + this.getServiceCASName() ;
       //window.location.href = environment.CAS_SERVER + "/login?service=http%3A%2F%2Flocalhost%3A4200%2Fhome";        

    }/*
    public login(username, password) {
        var self = this;
        return new Promise((resolve, reject) => {
            this.getTicketGrantingCASFromApi(username, password).subscribe(
                result => {
                    var ticketGrant = result._body;
                    this.getTicketServiceCASFromApi(ticketGrant).subscribe(
                        result => {
                            var ticketService = result._body;
                            this.validateTicketCAS(encodeURIComponent(ticketService)).subscribe(
                                usernameCAS => {
                                    if (usernameCAS) {
                                        self.startLocalSesion(usernameCAS).subscribe(
                                            result => {
                                                resolve(result);
                                            },
                                            error => {
                                                reject();
                                            });                                        
                                    }
                                },error => {
                                    reject();
                                });
                        },
                        error => {
                            reject();
                        });
                },
                error => {
                    reject();
                }
            );
        });
    }*/
    /*
        public closeSesionCAS2(local): Observable<any> {
            var self = this;
            var url = environment.CAS_SERVER + "/logout?service=";
            const headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append("Accept", 'application/x-www-form-urlencoded');
            var ro = new RequestOptions({ headers: headers });
            return this._http.get(url, ro).pipe(
                map(res => {
                    if (local) {
                        self.closeLocalSesion();
                    }
                    return res;
                })
            )
        }*/

    /*public removeProfile(): any {
        localStorage.removeItem('profile');
    }
    public setUserProfile(profile): any {
        localStorage.profile = JSON.stringify(profile);
    }
    public getUserProfile(): any {
        if (localStorage.profile) {
            return JSON.parse(localStorage.profile);
        }
        return null;
    }*/



    /*public login(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/login';
        return this._http.post(url, params).pipe(
            map(res => { return res.json() })
        )
    }*/
    /*public openPentahoSession(user, pass): Observable<any> {
        var url = 'http://10.10.5.176:8081/pentaho/Home?userid=' + user + "&password=" + pass;
        return this._http.get(url);
    }    */
    /*
    public getSessionInfoServer(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/security/getSessionInfo';
        return this._http.post(url, params).pipe(
            map(res => { return res.json() })
        )
    }


    public getSession() {
        return new Promise((resolve, reject) => {
            var sesionId = this.getCookie(constants.NAME_COOKIE_SESSION);
            if (!localStorage.profile) {
                var params = { SESSIONID: sesionId };
                this.getSessionInfoServer(params).subscribe(
                    profile => {
                        localStorage.profile = JSON.stringify(profile);
                        resolve(JSON.parse(localStorage.profile));
                    },
                    error => {
                        resolve(null);
                    }
                );
                resolve(null);
            }
            else {
                resolve(JSON.parse(localStorage.profile));
            }

            if (sesionId) {
                resolve(sesionId);
            }
            resolve(null);
        });
    }
    private createCookie(name, value, days, path) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date['toGMTString']();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/" + path;
    }
    private deleteCookie(name) {
        var expires = ";expires=Thu, 18 Dec 1900 12:00:00 UTC";
        document.cookie = name + "=" + expires + "; path=/";
    }
    private getCookie(c_name) {
        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

    public encryptWithAES = (text) => {
        return AES.encrypt(text, this.passphrase).toString();
    };

    public decryptWithAES = (ciphertext) => {
        const bytes = AES.decrypt(ciphertext, this.passphrase);
        const originalText = bytes.toString(Utf8);
        return originalText;
    };*/


}
