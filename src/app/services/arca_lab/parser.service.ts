import { Injectable } from '@angular/core';
import { BaseService } from '../util/base.service';
import * as mathjs from 'mathjs';
@Injectable()
export class ParserService extends BaseService {
    public symbols = ["(", ")", "+", "/", "-", "*", "^"];
    public TYPE_TOKEN_VAR_FOR_CTE: string = "VAR_FOR_CTE";
    public TYPE_TOKEN_NUMBER: string = "NUMBER";
    public TYPE_TOKEN_OPERATOR: string = "OPERATOR";
    public TYPE_TOKEN_OTHER: string = "OTHER";

    public cleanString(text) {
        var temp = text;
        // Elimino saltos de linea
        temp = temp.replace(/(\r\n|\n|\r)/gm, " ");
        // En los operadores me aseguro añadiendo espacios pero quito - y / porque se usan en la codificacion de las variables
        var symbols = ["\\(", "\\)", "\\+", "\\*", "\\^"];
        var symbols2 = ["(", ")", "+", "*", "^"];
        for (var i=0;i<symbols.length;i++) {
            var pattern = symbols[i];
            var r = symbols2[i];
            var re = new RegExp(pattern, "g");  
            temp = temp.replace(re, ' ' + r + ' ');
            
        }
        // Elimino espacios redundantes 
        temp = temp.replace(/  +/g, ' ').trim();
        return temp; 
    }
    // Este método renombra las variables, constantes y fórmulas por nombres más sencillos para que mathjs no tenga problemas en el parser.
    public createDictionary(formulaOriginal, tokens) {
        var formulaTranslated = formulaOriginal;
        var translations = {};
        var translations2 = {};
        var tokens2 = [];
        var transID = 1;
        for (var token of tokens) {
            if (token.type === this.TYPE_TOKEN_VAR_FOR_CTE) {
                var id = "V_" + transID;
                translations[token.value] = id;
                translations2[id] = token.value;
                transID++;
                tokens2.push({
                    type: token.type,
                    value: id
                });
            }
            else {
                tokens2.push({
                    type: token.type,
                    value: token.value
                });
            }
        }
        //Obtengo la expresión formula con los términos traducidos
        for (var token of tokens) {
            if (token.type === this.TYPE_TOKEN_VAR_FOR_CTE) {
                formulaTranslated = formulaTranslated.replace(token.value, translations[token.value]);
            }
        }
        return {
            formulaTranslated: formulaTranslated,
            translations: translations,
            translations2: translations2,
            tokensTranslated: tokens2
        }
    }
    // Este método separa una cadena en tokens
    public simpleTokenizer(formula) {
        var temp = this.cleanString(formula);
        var tokens = this.tokenize(temp);
        
        var tokens2 = [];
        var strFixed = "";
        for (var token of tokens) {
            if (this.isNumeric(token)) {
                tokens2.push({
                    type: this.TYPE_TOKEN_NUMBER,
                    value: token
                });
                strFixed += token;
            }
            else if (token.startsWith('V') || token.startsWith('F') || token.startsWith('C')) {
                tokens2.push({
                    type: this.TYPE_TOKEN_VAR_FOR_CTE,
                    value: token
                });
                strFixed += token;
            }
            else if (this.symbols.includes(token)) {
                tokens2.push({
                    type: this.TYPE_TOKEN_OPERATOR,
                    value: token
                });
                strFixed += token;
            }
            else if (token.trim() != "") {
                tokens2.push({
                    type: this.TYPE_TOKEN_OTHER,
                    value: token
                });
                strFixed += token;
            }
            strFixed += " ";
        }
        return {
            tokens: tokens2,
            formulaFixed: strFixed
        };
    }
    // Este método para una fórmula de notación infija (normal) a postfija y devuelve un array de tokens
    public formula2PostFix(formula) {
        var result = {
            error: null,
            tokens: []
        };

        var res = this.simpleTokenizer(formula);
        var tokens = res.tokens;
        var formulaFixed = res.formulaFixed;
        var aux = this.createDictionary(formulaFixed, tokens);
        // Expresión en notación posfija
        var r = this.infixToPostfix(aux.formulaTranslated);
        if (r.error) {
            result.error = r.msgError;
            return result;
        }
        else {
            var postFiex = r.postfix;
            // Tokenizo la expresión posfija
            var res2 = this.simpleTokenizer(postFiex);
            
            // Tradución y tratamiento del operador unario
            var tokens2 = []; 
            for (var item of res2.tokens) {
                if (item.type === this.TYPE_TOKEN_VAR_FOR_CTE) {                    
                    tokens2.push(aux.translations2[item.value]);
                }
                else if (item.type === this.TYPE_TOKEN_NUMBER) {
                    if (parseFloat(item.value) < 0) {
                        tokens2.push(item.value * -1);
                        tokens2.push("U-");
                    }
                    else {
                        tokens2.push(item.value);
                    }
                }
                else {
                    tokens2.push(item.value);
                }
            }
            result.tokens = tokens2;
            return result;
        }

    }
    // Validación de la sintaxis de la fórmula
    public syntaxParse(formula) {
        var result = {
            error: null,
            tokens: []
        };
        // Separo en tokens la formula que llega en notación posfija (normal)
        var res = this.simpleTokenizer(formula);
        var tokens = res.tokens;
        var formulaFixed = res.formulaFixed;
        // Antes de pasar el parser de mathjs es necesario renombrar los nombres de variables para que funcione a nombres + simples
        var aux = this.createDictionary(formulaFixed, tokens);
        // Parseamos sintacticamente
        const parser = mathjs.parser();
        for (var token of aux.tokensTranslated) {
            if (token.type === this.TYPE_TOKEN_VAR_FOR_CTE) {
                parser.evaluate(token.value + ' = 1'); // Asigno un valor cualquiera para poder evaluar
            }
        }
        try {
            // Usamos una doble evaluación
            parser.evaluate(aux.formulaTranslated);
            mathjs.parse(aux.formulaTranslated) // Esta función no parsea del todo bien

        }
        catch (error) {
            // En caso de error sintáctico
            var temp = error.toString();
            // Para que los mensajes de error salgan con las nombres de variables originales
            for (var t in aux.translations2) {
                temp = temp.replace(t, aux.translations2[t]);
            }
            result.error = temp;
            return result;
        }
        // Comprobación adicional ya que el parser se traga dos variables seguidas sin operador
        result.error = this.checkVariablesSinOperador(tokens);
        return result;

    }
    // Chequea si un token es una variable
    isVariable(token) {
        if (token.startsWith('V_')) {
            return true;
        }
        return false;
    }
    // Chequea si un token es un valor numérico, sólo admitimos separador decimal el punto
    isNumeric(input) {
        var output = false;
        if (typeof input === "number") {
            output = true;
        }
        else {
            if (typeof input != "string") {
                output = false;
            }
            else {
                var pattern = /^-{0,1}\d*\.{0,1}\d+$/;
                output = pattern.test(input);
            }
        }
        return output;

    }
    // Comprobación adicional ya que el parser se traga dos variables seguidas sin operador
    checkVariablesSinOperador(tokens) {
        var token_ant;
        for (var token of tokens) {
            if (token.type == this.TYPE_TOKEN_VAR_FOR_CTE) {
                if (token_ant && token_ant.type == this.TYPE_TOKEN_VAR_FOR_CTE) {
                    return "Entre" + token_ant.value + " y " + token.value + " no hay operador ";
                }
            }
            token_ant = token;
        }
        return null;
    }
    // Separa una expresión en tokens (los numeros negativos son un único token)
    tokenize(exp) {
        var tokens = exp.split(" ").map((token, i) => /^\d$/.test(token) ? +token : token);
        var token_ant;
        var tokens2 = [];
        // Detección del operador unario negativo -> Para meterlo en un único token 
        for (var i = 0; i < tokens.length; i++) {
            var token = tokens[i];
            if (token == "-") {
                if (token_ant && this.symbols.includes(token_ant) && this.isNumeric(tokens[i + 1])) {
                    //console.log("HAY U-" + token + tokens[i + 1]);
                    tokens2.push(tokens[i + 1] * -1);
                    i++;
                }
                else {
                    tokens2.push(token);
                }
            }
            else {
                tokens2.push(token);
            }

            token_ant = token;
        }
        return tokens2
    }
    // Conversión de expresión a postfijo, algoritmo copiado de  https://www.mathblog.dk/tools/infix-postfix-converter/
    infixToPostfix(expr) {

        var result = {
            error: false,
            msgError: null,
            postfix: null
        };
        var i = 0;
        if (typeof expr !== 'string') {
            if (expr instanceof String) {
                expr = expr.toString();
            } else {
                result.error = true;
                result.msgError = 'Expresión inválida';
                return result;
            }
        }
        var tokens = expr.replace(/^-|([\^\(+\-*/])\s*-/g, "$1#")
            .match(/([\#?\.\w]+)|[\^()+\-*/]/gi)
            .map(e => e.replace("#", "-"));

        var S = [], O = [], tok;

        while (tok = tokens.shift()) {
            if (tok == '(') S.push(tok);
            else if (tok == ')') {
                while (S.length > 0 && S[S.length - 1] != '(') O.push(S.pop());
                if (S.length == 0) {
                    result.error = true;
                    result.msgError = 'Paréntesis no coincidente.';
                    return result;
                }
                S.pop();
            } else if (this.isOperator(tok)) {
                while (S.length > 0 && this.isOperator(S[S.length - 1]) && ((this.leftAssoc(tok) && this.priority(tok) <= this.priority(S[S.length - 1])) || (!this.leftAssoc(tok) && this.priority(tok) < this.priority(S[S.length - 1])))) O.push(S.pop());
                S.push(tok);
            } else {
                O.push(tok);
            }
        }

        while (S.length > 0) {
            if (!this.isOperator(S[S.length - 1])) {
                result.error = true;
                result.msgError = 'Paréntesis no coincidente.';
                return result;
            }
            O.push(S.pop());
        }

        if (O.length == 0) {
            result.error = true;
            result.msgError = 'Expresión inválida';
            return result;
        }

        var s = '';
        for (var i = 0; i < O.length; i++) {
            if (i != 0) s += ' ';
            s += O[i];
        }
        result.error = false;
        result.postfix = s;
        return result;


    }
    //Auxiliar de https://www.mathblog.dk/tools/infix-postfix-converter/
    isOperator(c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }
    //Auxiliar de https://www.mathblog.dk/tools/infix-postfix-converter/
    leftAssoc(c) {
        return c != '^';
    }
    //Auxiliar de https://www.mathblog.dk/tools/infix-postfix-converter/
    priority(c) {
        if (c == '^') return 3;
        if (c == '*') return 2;
        if (c == '/') return 2;
        if (c == '+') return 1;
        if (c == '-') return 1;
        return 0;
    }
    //Auxiliar de https://www.mathblog.dk/tools/infix-postfix-converter/
    rightPriority(c) {
        if (c == '+') return 1;
        if (c == '-') return 2;
        if (c == '*') return 3;
        if (c == '/') return 4;
        if (c == '^') return 5;
        return 0;
    }
}
