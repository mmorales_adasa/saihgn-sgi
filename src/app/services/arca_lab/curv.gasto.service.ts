import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BaseService } from '../util/base.service';
@Injectable()
export class CurvaGastoService extends BaseService {
    public nodos: any = {};
    public getInterpolacion(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/CurvaGasto/getInterpolacion';
        return this.callPost(url, params);
    }
    public calcula(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/CurvaGasto/calcula';
        return this.callPost(url, params);
    }
    public importXMLData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/CurvaGasto/importXMLData';
        return this.callPost(url, params);
    }
}
