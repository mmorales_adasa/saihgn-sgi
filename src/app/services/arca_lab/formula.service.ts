import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { BaseService } from '../util/base.service';
@Injectable()
export class FormulaService extends BaseService {
    public nodos: any = {};

    public testFormulas() {
        var variables = ['000301', '000302'];
        for (var vf of variables) {
            this.processFormulaTest(vf).then((resp) => {

            });
        }
    }
    processFormulaTest(cod_variable) {
        return new Promise((resolve, reject) => {
            this.getTokensFormula({ cod_variable: cod_variable }).subscribe((tokens) => {

            });
        });
    }

    public getNextId(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/getNextValueSequence';
        return this.callPost(url, params);
    }
    public findEstacionesVariablesYConstantes(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/getEstacionesVariablesYConstantes';
        return this.callPost(url, params);
    }
    public setRecalculo(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/setRecalculo';
        return this.callPost(url, params);
    }
    public checkTokensFormula(params): Observable<any> {

        var url = environment.RESTAPI_BASE_URL + '/Formula/checkTokensFormula';
        return this.callPost(url, params);
    }
    public getTokensFormula(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/getTokensFormula';
        return this.callPost(url, params);
    }
    public upgradeTokensFormula(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/upgradeTokensFormula';
        return this.callPost(url, params);
    }
    public tractarVarsAillades(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/Formula/tractarVarsAillades';
        return this.callPost(url, params);
    }
}
