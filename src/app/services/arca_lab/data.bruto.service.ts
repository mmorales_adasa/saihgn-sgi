import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { constants } from '../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { BaseService } from '../util/base.service';
@Injectable()
export class DatoBrutoService extends BaseService {
    public findVariables(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/findVariables';
        return this.callPost(url, params);
    }  
    public findVariablesPorRed(params): Observable<any> {
        console.log('findVariablesPorRed');
        console.log(params); 
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/findVariablesPorRed';
        return this.callPost(url, params);
    }  
    public findData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/findData';
        return this.callPost(url, params);
    } 
    public findDataPorRed(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/findDataPorRed';
        return this.callPost(url, params);
    } 
    public saveData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/saveData';
        return this.callPost(url, params);
    }  
    public saveDataPorRed(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/saveDataPorRed';
        return this.callPost(url, params);
    }      
    public createData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/createData';
        return this.callPost(url, params);
    }   
    public getDataSetVariables27Paginacion(params, self, entity, externalFilters=null) {
        return new Promise((resolve, reject) => {
            params.entity = entity//;constants.SGI.ENTITIES.VARIABLES_27;
            params.sort = "cod_variable";
            params.filters = [{ id: "cod_variable",   type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' },
                              { id: "descripcion",  type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' }
            ];
            if (externalFilters){
                params.filters = params.filters.concat(externalFilters);
                console.log(params.filters);
            }

            self._basicEntityService.getDataPagination(params).subscribe(
                (result: any[]) => {
                    var list2 = [];
                    for(var item of result){
                        list2.push({COD : item["cod_variable"] , DESCR : item["cod_variable"] + " (" + item["descripcion"] + ")"})
                    }   
                    resolve(list2);                 
                });
        });
    }               
    public getDataSetVariablesPaginacion(params, self, externalFilters=null) {
        return new Promise((resolve, reject) => {
            params.entity = constants.SGI.ENTITIES.VARIABLES;
            params.sort = "cod_variable";
            params.filters = [{ id: "cod_variable",   type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' },
                              { id: "desc_variable",  type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' }
            ];
            if (externalFilters){
                params.filters = params.filters.concat(externalFilters);
                console.log(params.filters);
            }

            self._basicEntityService.getDataPagination(params).subscribe(
                (result: any[]) => {
                    var list2 = [];
                    for(var item of result){
                        list2.push({COD : item["cod_variable"] , DESCR : item["cod_variable"] + " (" + item["desc_variable"] + ")"})
                    }   
                    resolve(list2);                 
                });
        });
    }   
    public getDataSetEstacionesPaginacion(params, self, externalFilters=null) {
        return new Promise((resolve, reject) => {
            params.entity = constants.SGI.ENTITIES.ESTACIONES;
            params.sort = "cod_estacion";
            params.filters = [
                { id: "cod_estacion",  type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' },
                { id: "desc_estacion", type_condition : 'OR', type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' }
            
            ];
            if (externalFilters){
                params.filters = params.filters.concat(externalFilters);
                console.log(params.filters);
            }
            self._basicEntityService.getDataPagination(params).subscribe(
                (result: any[]) => {
                    var list2 = [];
                    for(var item of result){
                        list2.push({COD : item["cod_estacion"] , DESCR : item["desc_estacion"] + " (" + item["cod_estacion"] + ")"})
                    }
                    resolve(list2);
                });
        });
    }            

    /******************************* */
    /***********AQUADAM************* */
    /******************************* */
    public findDataAquadam(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/findDataAquadam';
        return this.callPost(url, params);
    }        
    public saveDataAquadam(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/saveDataAquadam';
        return this.callPost(url, params);
    }   
    public saveDataAquadamMasivo(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/saveDataAquadamMasivo';
        console.log(params);
        return this.callPost(url, params);
    }        
    public getReferenceDays(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/getReferenceDays';
        return this.callPost(url, params);
    } 
    public getDatosNivel(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/getDatosNivel';
        return this.callPost(url, params);
    }      
    public getDatosCotas(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/getDatosCotas';
        return this.callPost(url, params);
    } 
    /******************************* */
    /***********AQUADAM************* */
    /******************************* */           
    public importData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/importData';
        return this.callPost(url, params);
    }            
    public fillHoles(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/fillHoles';
        return this.callPost(url, params);
    } 
    public forceSumarizacion(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/forceSumarizacion';
        return this.callPost(url, params);
    }     
    public showHistory(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/showHistory';
        return this.callPost(url, params);
    }  
    public fillGaps(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoBruto/fillGaps';
        return this.callPost(url, params);
    }               
} 
