import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { constants } from '../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { BaseService } from '../util/base.service';
@Injectable()
export class DatoDiarioService extends BaseService {
    public findVariables(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoDiario/findVariables';
        return this.callPost(url, params);
    }  
    public findData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoDiario/findData';
        return this.callPost(url, params);
    } 
    public saveData(params): Observable<any> {
        var url = environment.RESTAPI_BASE_URL + '/DatoDiario/saveData';
        return this.callPost(url, params);
    }          
    public getDataSetVariablesPaginacion(params, self) {
        return new Promise((resolve, reject) => {
            params.entity = constants.SGI.ENTITIES.VARIABLES;
            params.sort = "cod_variable";
            params.filters = [{ id: "cod_variable", type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' }];
            self._basicEntityService.getDataPagination(params).subscribe(
                (result: any[]) => {
                    var list = self._utilService.convertToListForArray(result, "cod_variable", "cod_variable");
                    resolve(list);
                });
        });
    }   
    public getDataSetEstacionesPaginacion(params, self) {
        return new Promise((resolve, reject) => {
            params.entity = constants.SGI.ENTITIES.ESTACIONES;
            params.sort = "cod_estacion";
            params.filters = [{ id: "cod_estacion", type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values: params.text ? params.text : '' }];
            self._basicEntityService.getDataPagination(params).subscribe(
                (result: any[]) => {
                    //var list = self._utilService.convertToListForArray(result, "cod_estacion", "cod_estacion");
                    var list2 = [];
                    for(var item of result){
                        list2.push({COD : item["cod_estacion"] , DESCR : item["desc_estacion"] + " (" + item["cod_estacion"] + ")"})
                    }
                    resolve(list2);
                });
        });
    }            
} 
