import { Injectable } from '@angular/core';
import { constants } from './../../config/constants';
import { constants as constantsCommon } from '@arca/common';

@Injectable()
export class UtilService  {
    convertToListForArray(list, fieldCod, fielDesc) {
        var result = [];
        for(var item of list){
            result.push({COD : item[fieldCod] , DESCR : item[fielDesc]})
        }
        return result;
    }
    paginationFunctionTemplate(params, fun , entity, sort ){
        return new Promise((resolve, reject) => {               
            params["entity"] = entity;
            params["sort"] = sort;
            params.filters  = [ {id : "cod_estacion" , type : constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, values :  params.text ? params.text : '' }];
            fun(params).subscribe(
                (result: any[]) => {
                  var list = this.convertToListForArray(result, "cod_estacion", "cod_estacion");
                  resolve(list);
                });
          });        
    }
}