import { Injectable } from '@angular/core';
@Injectable()
export class GraphService {
    defaultColors = [
    [255, 99, 132],
    [54, 162, 235],
    [255, 206, 86],
    [231, 233, 237],
    [75, 192, 192],
    [151, 187, 205],
    [220, 220, 220],
    [247, 70, 74],
    [70, 191, 189],
    [253, 180, 92],
    [148, 159, 177],
    [77, 83, 96]
    ];

    constructor() {        
    }

    public getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public getRandomColor() {
        return [this.getRandomInt(0, 255), this.getRandomInt(0, 255), this.getRandomInt(0, 255)];
    }    

    public generateColor(index) {
        var color = this.defaultColors[index] || this.getRandomColor();
        return {serie  : 'rgba(' + color.concat(0.4).join(',') + ')', normal : 'rgba(' + color.concat(1).join(',') + ')'};
    }
    public getColors(index, count) {
        return this.formatLineColor(this.generateColor(index).serie);
    }

    public rgba(colour, alpha) {
        return 'rgba(' + colour.concat(alpha).join(',') + ')';
    }

    public formatLineColor(colors) {
        return {
            backgroundColor: this.rgba(colors, 0.4),
            borderColor: this.rgba(colors, 1),
            pointBackgroundColor: this.rgba(colors, 1),
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: this.rgba(colors, 0.8)
        };
    }
}
