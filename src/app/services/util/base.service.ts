import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuditoriaService } from '@arca/common';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { HttpEvent, HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Injectable()
export class BaseService {
    nameToken = "authToken";
    constructor(public _router: Router, public _http: Http, public _httpClient: HttpClient, public _auditoriaService: AuditoriaService, public ngxXml2jsonService: NgxXml2jsonService) {
    }
    public handleError2(error: Response) {

        if (error.status === 403) {
            window.location.href = '/login';
        }
        else if (error.status == 500) {
            return Observable.throw(error.json());
        }
    }
    public getTokenFromProfile() {
        if (localStorage["profile"]) {
            var profile =  JSON.parse(localStorage["profile"]);
            return profile.sesionId;
        }
        return "";
    }
    public callPost(url, params): Observable<any> {        
        var token = this.getTokenFromProfile();
        if (!params) {
            params = {};
        }
        params[this.nameToken] = token;
        return this._http.post(url, {data : params}, this.getHeaders()).pipe(
            map(res => { return res.json() }),
            catchError(
                (error: any, caught: Observable<HttpEvent<any>>) => {
                    console.log(error);
                    if (error.status === 401) {
                        window.location.href = '/';
                    }
                    throw error;
                }
            )
        );
    }
    private getHeaders() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append("Accept", 'application/json');
        var token = this.getTokenFromProfile();
        if (token){
            headers.append('Authorization', token);
        }
        return new RequestOptions({ headers: headers });
    }
    public callGet(url): Observable<any> {
        /*var token = this.getTokenFromProfile();
        if (url.includes('?')) {
            url += "&" + this.nameToken + "=" + token;
        } else {
            url += "?" + this.nameToken + "=" + token;
        }*/
        return this._http.get(url).pipe(
            map(res => { return res.json() }),
            catchError(
                (error: any, caught: Observable<HttpEvent<any>>) => {
                    if (error.status === 401) {
                        window.location.href = '/';
                    }
                    throw error;
                }
            )
        )
    }
}