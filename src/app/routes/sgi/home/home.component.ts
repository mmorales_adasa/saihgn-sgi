import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { Router } from '@angular/router';
import { LayoutService } from './../../../layout/layout.service';
import { SecurityService } from './../../../services/common/security.service';
import { constants } from './../../../config/constants';
import { environment } from './../../../../environments/environment';
import jwt_decode from "jwt-decode";
import { Chart } from 'chart.js';
@Component({
    selector: 'home-laboratorio-component',
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html',
    providers: []    
})
export class HomeLaboratorioComponent implements OnInit {
    profile: any;

    visibleVal : boolean = true;
    visibleEditConf : boolean = true;
    visibleConfiguration : boolean = true;
    visibleModelos : boolean = true;
    visibleCurvas : boolean = true;
    visibleFormulas : boolean = true;
    visibleSeguridad : boolean = true;
    visibleMonitor : boolean = true;
    visibleSira  : boolean = true;
    visibleEsquemas : boolean = true;
    visibleInformes : boolean = true;

    constructor(public _auditoriaService: AuditoriaService, private _router: Router, public layoutService: LayoutService, public _securityService : SecurityService) {
        console.log('HomeLaboratorioComponent');
    }
    ngOnInit() {
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.HOME);
        //this.profile = this._auditoriaService.getUserProfile();

        /*this.visibleVal =  this._securityService.checkVisibleElement('MOD_VAL_MANUAL');
        this.visibleEditConf =  this._securityService.checkVisibleElement('MOD_EDIT_CONF');
        this.visibleConfiguration =  this._securityService.checkVisibleElement('MOD_CONFIGURACION');
        this.visibleModelos =  this._securityService.checkVisibleElement('MOD_MODELOS_REF');
        this.visibleCurvas =  this._securityService.checkVisibleElement('MOD_CURVAS_GASTO');
        this.visibleFormulas =  this._securityService.checkVisibleElement('MOD_FORMULAS');
        this.visibleMonitor =  this._securityService.checkVisibleElement('MOD_MONITOR');
        this.visibleSeguridad =  this._securityService.checkVisibleElement('MOD_SEGURIDAD');*/
        

    }


    clickOnMainMenuOption(option) {
        var disabled = ['po'];
        if (disabled.includes(option)) {
            alert("TODO");
        }
        else {
            this.layoutService.setMenuSelected(option);
            this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
        }
    }
    testProfile(){
        var self = this;
        this._securityService.startLocalSesion('adasa', 'db', null).subscribe(
            result => {
                var decoded = jwt_decode(result.sesionId);
                alert(JSON.stringify(decoded));

                //alert(result.sesionId);
                this._securityService.decodeJWT2(result.sesionId).subscribe(result => {
                    alert(JSON.stringify(result));
                });                           
            },
            error => {
                alert("Error startLocalSesion");
            });
    }
    decodeJWT(){
        this.decode().then((list) => {
            
        });
    }

    decode() {
        return new Promise((resolve, reject) => { 
        this._securityService.decodeJWT().subscribe(result => {
            resolve(result);    
            });
        });
    }      
    openSiraWeb(){
        window.open(environment.URL_SIRA, "_blank");
    }
}
