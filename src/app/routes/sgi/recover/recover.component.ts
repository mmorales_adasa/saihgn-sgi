import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from '../../../config/constants';
import { Router } from '@angular/router';
import { AuditoriaService } from '@arca/common';
import { SecurityService } from '../../../services/common/security.service';
import { environment } from './../../../../environments/environment';

@Component({
    selector: 'recover-component',
    templateUrl: './recover.component.html',
    styleUrls: ['./recover.component.scss'],
    providers: []
})
export class RecoverComponent implements OnInit {    
    codeApp  = constants.SGI.COD_APP;
    username;
    error;
    enviado = -1;
    env = environment;
    constructor(
        public _securityService: SecurityService,
        private _auditoriaService: AuditoriaService,
        private translate: TranslateService,
        private _router: Router,
        private _layoutService: LayoutService
    ) {
        /*this.translate.addLangs(['es', 'en']);
        this.translate.setDefaultLang('es');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/es|en/) ? browserLang : 'es');*/
    }
    ngOnInit() {     
        this._auditoriaService.removeProfile();           
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.RECOVER);
    }
    
    recover() {
        this.error = null;
        this.enviado = -1;
        if (!this.username || this.username.trim()==""){
            this.error = "Introduzca su nombre de usuario";
        }
        //return new Promise((resolve, reject) => {
            var params = { filters: [{id : "username", type: "TEXT", values :  this.username,}]}
            this._securityService.recover(params).subscribe(result => {
                if (!result){
                    this.error = "No se ha podido reenviar su contraseña. Revise su email o consulte con soporte@saihguadiana.com";
                }
                if (result){
                    this.enviado=1;
                }
                
         //       resolve(list);    
            });
         //   });
    }
    onKeydown(event) {
        if (event.key === "Enter") this.recover();
      }    
}
