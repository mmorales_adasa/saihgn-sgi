import { Component, OnInit } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel } from '@arca/common';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-monitor-ops',
    templateUrl: './monitor.ops.component.html',
    styleUrls: ['./monitor.ops.component.scss'],
    providers: []
})
export class MonitorOpsComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "timestamp_operacion DESC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listEstadoEstacion = [];
    listTiposEstacion = [];
    listComunidades = [];
    listProvincias = [];
    listMunicipios = [];
    listComarcas = [];
    ngOnInit() {
        this.configureTable();
        this.searchData();
    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.OPERACIONES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {

            var params = { entity: constants.SGI.ENTITIES.OPERACIONES, filters: this.filterSelection }
            this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                this.tablaConstantes.setTotalElements(total);
                this.tablaConstantes.setDataAndLoadTable(list);
            });


        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_est',
            title: null,
            columns: [
                { id: "cod_operacion", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "timestamp_operacion", filter: true, displayed: true, translation: "Fecha" },
                { id: "cod_usuario", filter: true, displayed: true, translation: "Usuario" },
                { id: "tipo_operacion", filter: true, displayed: true, translation: "Tipo operación" },
                { id: "desc_observaciones", filter: true, displayed: true, translation: "Observaciones" },
                
            ],
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }

    onSort(event) {
        //console.log(event);
        this.sort = event.active + " " + event.direction;
        this.searchData();
    }
    onPagination(event) {
        console.log("del server");
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData();
    }

} 