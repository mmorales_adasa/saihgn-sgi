import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-monitor-main',
    templateUrl: './monitor.main.component.html',
    styleUrls: ['./monitor.main.component.scss'],
    providers: []
})
export class MonitorMainComponent implements OnInit {
    profile: any;
    selectedOption: string;
    rootSection = {
        title: "Monitorización",
        class: "fa fa-eye "
    }
    sections = [{
        title: "Procesos",
        route: "mto", 
        public: true,
        selected: this._route.snapshot.url[0].path == "mto",
    },
    {
        title: "Operaciones",
        route: "mto_ops",
        public: true,
        selected: this._route.snapshot.url[0].path == "mto_ops",
    }
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.MONITORIZACION);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
