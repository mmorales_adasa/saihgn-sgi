import { Component, OnInit } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel } from '@arca/common';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-monitor-procesos',
    templateUrl: './monitor.proc.component.html',
    styleUrls: ['./monitor.proc.component.scss'],
    providers: []
})
export class MonitorProcComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "num_proceso DESC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listEstadoEstacion = [];
    listTiposEstacion = [];
    listComunidades = [];
    listProvincias = [];
    listMunicipios = [];
    listComarcas = [];
    ngOnInit() {
        this.configureTable();
        this.searchData();
    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.PROCESOS, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {

            var params = { entity: constants.SGI.ENTITIES.PROCESOS, filters: this.filterSelection }
            this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                this.tablaConstantes.setTotalElements(total);
                this.tablaConstantes.setDataAndLoadTable(list);
            });
        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_est',
            title: null,
            columns: [
                { id: "cod_proceso", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "timestamp_ini", filter: true, displayed: true, translation: "Inicio" },
                { id: "timestamp_fin", filter: true, displayed: true, translation: "Fin" },
                { id: "resultado", filter: true, displayed: true, translation: "Resultado" },
                { id: "num_proceso", filter: true, displayed: true, translation: "Procesados" },
                { id: "num_regs_error", filter: true, displayed: true, translation: "Erróneos" }
            ],
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    onSort(event) {
        this.sort = event.active + " " + event.direction;
        this.searchData();
    }
    onPagination(event) {
        console.log("del server");
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData();
    }

} 