import { Component, OnInit,ViewChild, ɵConsole } from '@angular/core';
import { constants } from '../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel,BasicFilter, BasicTableUtility} from '@arca/common';
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import moment from 'moment';


@Component({
    selector: 'app-val-aquadam-manager',
    templateUrl: './val.aquadam.component.html',
    styleUrls: ['./val.aquadam.component.scss'],
    providers: []
})
export class ValidacionManualAquadam extends SGIListBaseComponent implements OnInit {
    @ViewChild('modalValidacion') modalValidacion: any;
    @ViewChild('modalValidacionConfirm') modalValidacionConfirm: any;
    
    profile: any;
    filter;
    filterSelection = [];
    offset: number = 0;    
    tablaResultados;
    //tablaCotas;
    listSondas = [];
    motivosInvalidacion = [];
    datoMuestra;
    rowSelected;
    dateSelected;
    statusValidacion;
    sort = " timestamp_sondeo desc ";
    referenceDays;
    datosNivel;
    datosCotas;
    confSonda;
    chart;
    showCotas : boolean = false;
    charcotasRelProf ={};
    chartRange : boolean = false;
    tipoValidacion : number = 1; // 1 normal, 2 masiva
    statusValidacionDefecto = 
    [
        {cod : 'temperatura',  name : 'Temperatura', value : 1},
        {cod : 'ph',  name : 'pH', value : 1},
        {cod : 'oxigeno',  name : ' Oxigeno Disuelto: ', value : 1},
        {cod : 'conductividad',  name : 'Conductividad', value : 1},
        {cod : 'redox',  name : 'Redox', value : 1},
        {cod : 'turbidez',  name : 'Turbidez', value : 1},
        {cod : 'secchi',  name : 'SECCHI', value : 1},
        {cod : 'clorofila',  name : 'Clorofila', value : 1},
        {cod : 'ficocianina',  name : 'Ficocianina', value : 1},
    ];    

    validacionesUtility = new BasicTableUtility({
        id: "VALIDACION_RANGO_FECHAS",
        tooltip: "Validar rango de fechas",
        faIcon: "fa-check-square-o",
        innerHtml: ""
    });

    date_from_rango;
    date_to_rango;
    saving_masivo : boolean = false;
    msgErrorMasivo;
    
    ngOnInit() {
        this.profile = this._auditoriaService.getUserProfile();
        this.searchMasterData().then((resp) => {
            this.configureTable();            
            this.configureFilters();
            this.initFilters();
            
        });
    }
    initFilters(){
        this.filter.elementMap["date_from"].selectedValue=moment().subtract(5, "days").format('YYYY-MM-DD'); 
        this.filter.elementMap["date_from"].dirty = true;                
    }    
    searchMasterData() { 
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.SONDAS_AQUADAM_27, filters: [], sort: "cod_sonda ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.listSondas = list;
                
                resolve();
            });
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MOTIVOS_INVALIDACION_27, sort : "cod_validez ASC" }).subscribe(list => {
                this.motivosInvalidacion = list.filter(function (item) { return (item.es_sonda_mult)});;                
                //this.motivosInvalidacion.push({"desc_motivo_invalidacion" : "--", "cod_motivo_invalidacion" : 0})
            });

        });
    }    
    configureFilters() {     
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
            group: 1,
            width: 300,
            id: 'cod_sonda',
            label: 'Sonda',
            code: 'cod_sonda',
            desc: 'nombre',
            data: this.listSondas
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 1,
            width: 200,
            id: 'date_from',
            label: 'Fecha desde'
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 1,
            width: 200,
            id: 'date_to',
            label: 'Fecha Hasta'
        });    
        
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 1,
            width: 300,
            id: 'hay_invalidados',
            label: 'Sólo muestras con medidas inválidas',
            code: 'COD',
            desc: 'DESCR',
            data: this._staticListService.getYesNoList()
        });        
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 1,
            width: 300,
            id: 'solo_programados',
            label: 'Sólo sondeos programados',
            code: 'COD',
            desc: 'DESCR',
            data: this._staticListService.getYesNoList()
        });                
/*
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 1,
            width: 300,
            id: 'invalidas',
            label: 'Sólo variables con medidas inválidas',
            code: 'COD',
            desc: 'DESCR',
            data: this._staticListService.getYesNoList()
        }); */      

    }    
    configureTable() {
        
        var tableConfiguration = {
            id: 'mr_mol',
            title: null,
            utilities: [this.validacionesUtility], 
            columns: [
                { id: "Validate", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Validar", icon: "fa fa-check", color: "green" }},
                { id: "cod_sonda", filter: true, displayed: true, translation: "Sonda", primaryKey: true},
                { id: "nombre", filter: true, displayed: true, translation: "Nombre"},
                { id: "timestamp_sondeo", filter: true, displayed: true, translation: "Fecha sondeo" },
                { id: "hay_invalidados", filter: true, displayed: false, translation: "Hay inválidos" },
                { id: "hay_invalidados_desc", filter: true, displayed: true, translation: "Hay inválidos" }
            ],
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "timestamp_sondeo",
                direction: "desc"
            }
        };
        this.tablaResultados = new BasicTableModel(tableConfiguration);

    }

    adaptFilters(){
    for (var i=0;i<this.filterSelection.length;i++){
        if (this.filterSelection[i].id=='date_to'){
            this.filterSelection[i].type = constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_DATE_MINOR;
        }
    }
}

searchData(count) {
     var self = this;
     this.adaptFilters();        
     // En este caso que se vean todas, eliminamos el filtro
     var temp  = JSON.parse(JSON.stringify(this.filterSelection)); 
     var index = this.filterSelection.findIndex(o => o.id === 'hay_invalidados');      
     if (temp[index].values == 0){
         temp.splice(index, 1);
     }    
     index = temp.findIndex(o => o.id === 'solo_programados'); 
     if (temp[index].values == 0){
         temp.splice(index, 1);
    }    
     var params = { entity: constants.SGI.ENTITIES.DATOS_SONDAS_AQUADAM_27, offset: this.offset, 
                   limit: constants.DEFAULT_LIMIT_PAGINATION, filters: 
                   temp, sort: this.sort};
                   this.tablaResultados.startLoading();
    this._basicEntityService.getDataPagination(params).subscribe(list => {        
        var params = { entity: constants.SGI.ENTITIES.DATOS_SONDAS_AQUADAM_27, filters: temp }
        if (count){
            
            this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                self.tablaResultados.setTotalElements(total);
                self.tablaResultados.setDataAndLoadTable(list);                
                self.tablaResultados.stopLoading();
            });    
        }
        else{
            self.tablaResultados.stopLoading();
            self.tablaResultados.setDataAndLoadTable(list);
        }

    });
} 
    /*searchData() {
        var params = { entity: constants.SGI.ENTITIES.MODELOS, filters: this.filterSelection, sort: "desc_decreto ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    }*/
    afterExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData(true);
        }
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(true);
    }
    goDay(num){
        var date;
        if (num == 1){            
            date = this.referenceDays.next_date;
        }
        else if  (num == -1){            
            date = this.referenceDays.prev_date;
        }
        else if  (num == 2){            
            date = this.referenceDays.last_date;
        }
        if (date){
            this.datoMuestra = null;
            //this.tablaCotas.startLoading();
            this.dateSelected = moment(date,'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:mm:ss');
            this.searchDataFromDay(this.rowSelected.cod_sonda,date).then((list) => {             
                this.datoMuestra = list;                           
                //this.tablaCotas.setDataAndLoadTable(this.datoMuestra);
                this.createChart();
             }); 
             this.referenceDays = null;
             this.getReferenceDays(this.rowSelected.cod_sonda, date).then((days : any) => {
                this.referenceDays = days;
            });               

        }
    }

    
    getReferenceDays(cod_sonda, timestamp_sondeo){
        return new Promise((resolve, reject) => {  
            var params = {
                cod_sonda    : cod_sonda,
                timestamp_sondeo    : timestamp_sondeo,
            }              
            this._datoBrutoService.getReferenceDays(params).subscribe(days => {
                resolve(days);               
            });        
        });
    }
    getDatosNivel(cod_sonda){
        return new Promise((resolve, reject) => {  
            var params = {
                cod_sonda    : cod_sonda
            }              
            this._datoBrutoService.getDatosNivel(params).subscribe(days => {
                resolve(days);               
            });        
        });
    }

    getDatosCotas(cod_sonda){
        return new Promise((resolve, reject) => {  
            var params = {
                cod_sonda    : cod_sonda
            }              
            this._datoBrutoService.getDatosCotas(params).subscribe(days => {
                resolve(days);               
            });        
        });
    }    

    searchDataFromDay(cod_sonda, timestamp_sondeo){
        return new Promise((resolve, reject) => {  
            var filters = {
                variables: { id: "cod_sonda", values: [cod_sonda], type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                date_from: { id: "timestamp_sondeo", values: [timestamp_sondeo], type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_TIMESTAMP_EQUAL },               
            };
                this._datoBrutoService.findDataAquadam({ filters: filters }).subscribe(list => {            
                    resolve(list);
            });        
        });
    }

    onExecuteTableUtilityAction(event) {
        var self = this;    
        this.msgErrorMasivo = null;
        
        if (event.action == "VALIDACION_RANGO_FECHAS") {
            // Inicializamos a OK por defecto
            if (!this.statusValidacion){
                this.statusValidacion = this.statusValidacionDefecto;
            }else{            
                for (var i=0;i<this.statusValidacion.length;i++){
                    this.statusValidacion[i].value = 1;
                } 
            }
            this.date_to_rango = null;
            this.date_from_rango = null;   
            for (var i=0;i<this.filterSelection.length;i++){
                if (this.filterSelection[i].id=='date_to'){ this.date_to_rango = this.filterSelection[i].values;}
                if (this.filterSelection[i].id=='date_from'){ this.date_from_rango = this.filterSelection[i].values;}
            }                       
            this.tipoValidacion = 2;
            if (this.date_to_rango && this.date_from_rango){
                this.saving_masivo = false;
                this.modalService.open(this.modalValidacionConfirm, { size: 'sm', centered: true });
            }
            else{
                this.modalService.open(this.modalValidacionConfirm, { size: 'sm', centered: true });
                this.msgErrorMasivo = "Debe selecccionar al menos la fecha de inicio y la de final y darle al botón buscar para que los sondeos a modificar se muestren en la tabla";                
            }
        }
    }

    onExecuteTableRowUtilityAction(event) {        
        this.datoMuestra = null;
        this.datosNivel = null;
        this.datosCotas = null;
        this.referenceDays = null;
        this.showCotas = false;
        if (event.action == "Validate") {
         this.rowSelected = event.row;
         this.dateSelected = moment(event.row.timestamp_sondeo,'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:mm:ss');
            this.statusValidacion = null;
            this.tipoValidacion = 1;

            
            this.modalService.open(this.modalValidacion, { size: 'lg', centered: true });
            this.confSonda = this.listSondas.find(o => o.cod_sonda === event.row.cod_sonda);
            //this.configureTableCotas(confSonda);                
            
            this.searchDataFromDay(event.row.cod_sonda,event.row.timestamp_sondeo).then((list : any) => {
                this.datoMuestra = list;                
                //this.tablaCotas.setDataAndLoadTable(this.datoMuestra);                
                this.createChart();                    
            });      
            this.getReferenceDays(event.row.cod_sonda,event.row.timestamp_sondeo).then((days : any) => {
                this.referenceDays = days;
            });            
            this.getDatosNivel(event.row.cod_sonda).then((result : any) => {
                this.datosNivel = result;
            });              
            this.getDatosCotas(event.row.cod_sonda).then((result : any) => {
                this.datosCotas = result;
            });              
        }
    }    
    onSort(event) {
        this.sort = event.active + " " + event.direction;        
        this.searchData(false);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(false);
    }    
    onSave(modal){
        //console.log(this.statusValidacion);
        this.save().then((result : any) => {
            modal.dismiss('Cross click');
        });
    }
    save(){
        var self = this;
        if (this.tipoValidacion==2){ // Validación masiva
            return new Promise((resolve, reject) => {    
            var params = {
                filters : []
            };         
            for (var i=0;i<this.filterSelection.length;i++){
                if (this.filterSelection[i].id=='cod_sonda'){
                   params.filters.push({ id: "cod_sonda", type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, values: this.filterSelection[i].values });
                }
                if (this.filterSelection[i].id=='date_to'){
                    params.filters.push({ id: "timestamp_sondeo", type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_DATE_MINOR, values: this.filterSelection[i].values });
                 }                      
                if (this.filterSelection[i].id=='date_from'){
                    params.filters.push({ id: "timestamp_sondeo", type: "DATE", values: this.filterSelection[i].values });
                 }                
                 /*if (this.filterSelection[i].id=='hay_invalidados'){
                    params.filters.push({ id: "hay_invalidados", type: "INTEGER", values: this.filterSelection[i].values });
                 }*/                

            }
        
          
            for (var i=0;i<this.statusValidacion.length;i++){
                params[this.statusValidacion[i].cod] = this.statusValidacion[i].value; 
            }    
            this.saving_masivo = true;        
            this._datoBrutoService.saveDataAquadamMasivo(params).subscribe(list => {
                resolve();
                this.saving_masivo = false;
                this.searchData(true);
                
            });                 
            });
        }
        else{
        return new Promise((resolve, reject) => {    
            var params = {
                cod_sonda    : this.rowSelected.cod_sonda,
                timestamp_sondeo    : this.rowSelected.timestamp_sondeo,
            }    
            for (var i=0;i<this.statusValidacion.length;i++){
                params[this.statusValidacion[i].cod] = this.statusValidacion[i].value; 
            }
            this._datoBrutoService.saveDataAquadam(params).subscribe(list => {
                
                // Actualización en grid
                for (var i=0;i<this.statusValidacion.length;i++){            
                    var field_val = "val_" + self.statusValidacion[i].cod;
            
                     for (var j=0;j<this.datoMuestra.length;j++){
                        if (this.datoMuestra[j][field_val]){
                            this.datoMuestra[j][field_val] = this.statusValidacion[i].value;
                        }
                    }                     
                }
                           resolve();
            });        
        });
        }
    }
    onValidate(){
        var self = this;
        
        
        if (!this.statusValidacion){
            this.statusValidacion = this.statusValidacionDefecto;
        }   
        
        for (var i=0;i<this.statusValidacion.length;i++){
            
            var field_val = "val_" + self.statusValidacion[i].cod;
            var aux = self.datoMuestra.filter(function (item) {                                 
                return item[field_val] && item[field_val.trim()] != "1";
             });
            if (aux.length>0){
                this.statusValidacion[i].value= aux[0][field_val];
            }
            else{
                this.statusValidacion[i].value=1;
            }
        }
        
        this.tipoValidacion =1;
        this.saving_masivo = false;

        this.modalService.open(this.modalValidacionConfirm, { size: 'sm', centered: true });
    }
   /* configureTableCotas(confSonda) {
        var tableConfiguration = {
            id: 'mr_mol',
            title: null,
            columns: [
                { id: "num_lectura", filter: true, displayed: true, translation: "Nº muestra" },
                { id: "cota", filter: true, displayed: true, translation: "Cota [msnm]" },
                { id: "profundidad", filter: true, displayed: true, translation: "Prof [m]" },
                
                //{ id: "ph", filter: true, displayed: true, translation: "pH [ud pH]" },
                //{ id: "oxigeno", filter: true, displayed: true, translation: "O.D [mg/l]" },
                //{ id: "conductividad", filter: true, displayed: true, translation: "Conduct [µS/cm]" },
                //{ id: "redox", filter: true, displayed: true, translation: "REDOX [mV]" },
                //{ id: "turbidez", filter: true, displayed: true, translation: "Turbidez [NTU]" },
                //{ id: "secchi", filter: true, displayed: true, translation: "Secchi [%]" }                                
            ],
            serverPagination: false,
            pageSize: 40,//constants.DEFAULT_LIMIT_PAGINATION, 
            sort: {
                active: "num_lectura",
                direction: "asc"
            }
        };

        if (confSonda.temperatura){ tableConfiguration.columns.push({ id: "temperatura", filter: true, displayed: true, translation: "Temp [ºC]" });}
        if (confSonda.ph){ tableConfiguration.columns.push({ id: "ph", filter: true, displayed: true, translation: "pH [ud pH]" })}
        if (confSonda.oxigeno){ tableConfiguration.columns.push({ id: "oxigeno", filter: true, displayed: true, translation: "O.D [mg/l]" })};
        if (confSonda.conductividad){ tableConfiguration.columns.push({ id: "conductividad", filter: true, displayed: true, translation: "Conduct [µS/cm]" })};
        if (confSonda.redox){ tableConfiguration.columns.push({ id: "redox", filter: true, displayed: true, translation: "REDOX [mV]" }); }
        if (confSonda.turbidez){ tableConfiguration.columns.push({ id: "turbidez", filter: true, displayed: true, translation: "Turbidez [NTU]" }); }
        if (confSonda.secchi){ tableConfiguration.columns.push({ id: "secchi", filter: true, displayed: true, translation: "Secchi [%]" }); }
        if (confSonda.clorofila1){ tableConfiguration.columns.push({ id: "clorofila1", filter: true, displayed: true, translation: "Clorofila1 [ug/l]" }); }
        if (confSonda.ficocianina){tableConfiguration.columns.push({ id: "ficocianina", filter: true, displayed: true, translation: "Ficocianina [Cell/L]" }); }
        
        this.tablaCotas = new BasicTableModel(tableConfiguration);

    }*/
    generateChartDataFormat() {
        var chartData = [];
        
        this.charcotasRelProf = {};
                for (var i = this.datoMuestra.length-1; i >= 0; i--) {
                  this.charcotasRelProf[this.datoMuestra[i].cota] = this.datoMuestra[i].profundidad
                  var data = {
                    profundidad: Math.trunc(this.datoMuestra[i].profundidad), 
                    cota: Math.trunc(this.datoMuestra[i].cota),
                    TA_AQ: this.datoMuestra[i].temperatura,
                    PH_AQ: this.datoMuestra[i].ph,
                    OX_AQ: this.datoMuestra[i].oxigeno,
                    CD_AQ: this.datoMuestra[i].conductividad,
                    RD: this.datoMuestra[i].redox,
                    TB_AQ: this.datoMuestra[i].turbidez,
                    CL_AQ: this.datoMuestra[i].clorofila1,
                    FI_AQ: this.datoMuestra[i].ficocianina,
                    SC_AQ: this.datoMuestra[i].secchi,
                }                  
                chartData.push(data);
            }
        return chartData.reverse();
    }
    createAxisAndSeries(field, name, opposite, color, max, min) {
        var valueAxis = this.chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = min;
        valueAxis.max = max;
        valueAxis.renderer.minGridDistance = 30;
        var series = this.chart.series.push(new am4charts.LineSeries());
        series.dataFields.categoryY = "cota";
        series.dataFields.valueX = field;
        series.xAxis = valueAxis;
        series.name = name;
        series.tooltipText = "{name}: [bold]{valueX}[/]";
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color(color);
        series.tensionX = 1;
        series.strokeWidth = 2;
        series.stroke = am4core.color(color);
        valueAxis.renderer.labels.template.fill = am4core.color(color);
        valueAxis.renderer.labels.template.stroke = am4core.color(color);
        valueAxis.renderer.line.strokeOpacity = 2;
        valueAxis.renderer.line.strokeWidth = 2;
        valueAxis.renderer.line.stroke = series.stroke;
        valueAxis.renderer.labels.template.fill = series.stroke;
        valueAxis.renderer.opposite = opposite;
        valueAxis.renderer.grid.template.strokeOpacity = 2;
        valueAxis.renderer.grid.template.disabled = true;
        valueAxis.renderer.grid.template.strokeWidth = 2;
        this.chart.scrollbarX = new am4charts.XYChartScrollbar();
        this.chart.scrollbarY = new am4core.Scrollbar();
    }

    onShowCotas(){
        this.createChart();
    }
    
    createChart(){
        this.chart = am4core.create("chartdiv", am4charts.XYChart);
        this.chart.language.locale["_decimalSeparator"] = ",";
        this.chart.language.locale["_thousandSeparator"] = ".";
        this.chart.colors.step = 2;
        this.chart.data = this.generateChartDataFormat();
        var dateAxis = this.chart.yAxes.push(new am4charts.CategoryAxis());
        dateAxis.title.text = "Profundidad(m) / Cota (msnm) ";
        dateAxis.title.fontWeight = 600;
        dateAxis.dataFields.category = "cota";
        dateAxis.renderer.inversed = true;
        dateAxis.renderer.minGridDistance = 10;        
        
        var campos = [];



        if (this.confSonda.temperatura){campos.push({field : "TA_AQ", nombre : 'Temp.(ºC)', opossite : true, color : '#338DFF', max : 30, min : 0 })}
        if (this.confSonda.ph){campos.push({field : "PH_AQ", nombre : 'pH (ud pH)', opossite : false, color : '#FF5833',      max : 14, min : 0 });};
        if (this.confSonda.oxigeno){campos.push({field : "OX_AQ", nombre : 'O.D.(mg/l)', opossite : true, color : '#FFD733',       max : 20, min : 0 });};
        if (this.confSonda.conductividad){campos.push({field : "CD_AQ", nombre : 'Conduct.(µS/cm)', opossite : false, color : '#2CCD3B', max : 1000, min : 0 });};
        if (this.confSonda.redox){ campos.push({field : "RD",    nombre : 'REDOX(mV)', opossite : true, color : '#FF8BD8',        max : 600, min : -400 });};
        if (this.confSonda.turbidez){campos.push({field : "TB_AQ", nombre : 'Turbidez.(NTU)', opossite : false, color : '#7800FF',  max : 100, min : 0 });};
        if (this.confSonda.secchi){ campos.push({field : "SC_AQ", nombre : 'Secchi [%]', opossite : true, color : 'black',  max : 100, min : 0 });};
        if (this.confSonda.clorofila1){campos.push({field : "CL_AQ", nombre : 'Clorofila [ug/l]', opossite : false, color : '#5BFF5B',  max : 1000, min : 0 });};
        if (this.confSonda.ficocianina){campos.push({field : "FI_AQ", nombre : 'Ficocianina [Cell/L]', opossite : true, color : '#FCB209',  max : 1000000, min : 0 });};


        for (var i = 0; i < campos.length; i++) {
            this.createAxisAndSeries(campos[i].field, campos[i].nombre, campos[i].opossite, campos[i].color, campos[i].max, campos[i].min);
        }

        this.chart.legend = new am4charts.Legend();
        this.chart.legend.position = "top";
        this.chart.legend.scrollable = true;
        this.chart.scrollbarX = new am4core.Scrollbar();
        this.chart.scrollbarY = new am4core.Scrollbar();       
        this.chart.cursor = new am4charts.XYCursor();        
        
        
        //var cotas = {};
        if (this.showCotas) {
            
            for (var i = 0; i < this.datosCotas.length; i++) {                
                var cota = this.datosCotas[i];
                    let range = dateAxis.axisRanges.create();
                    var tipo = cota['cod_nivel'].split("/")[1];
                    var style = this.getStyleTipoCota(tipo);
                    if (tipo==='NMN' || tipo==='ALIV' || tipo.includes('ABA')) // De momento solo saco cotas de aliviero
                    {
                        range.category = Math.trunc(parseFloat(cota['nivel'])); //;parseFloat(cota['ABA2']['nivel'].split("/")[0]);; //Math.trunc(cota['nivel']); //;cota['cod_nivel'].split("/")[1];                                     
                        range.grid.stroke = am4core.color(style.color);
                        range.grid.strokeWidth = 2;
                        range.grid.strokeOpacity = 1;
                        range.label.inside = true;
                        range.label.text = cota['descripcion'];
                        range.label.fill = range.grid.stroke;
                        range.label.dy = -7; 
                        range.label.align = style.align;
                        
                    }
            }
        }
    }
    getStyleTipoCota(tipo){
        var style = { color : 'black', align : 'center' };
        if (tipo.includes('NMN')){
            style.color = 'black';
            style.align = 'right';
        }
        else if (tipo.includes('NMN')){
            style.color = 'yellow';
            style.align = 'left';
        }
        else if (tipo.includes('ABA')){
            style.color = '#BA4A00';
        }
        return style;
    }
    getAlignTipoCota(){
        
    }    

    getMotivoInvalidacionDesc(id){        
        var index = this.motivosInvalidacion.findIndex(o => o.cod_validez === id);
        if (index>=0){
        return this.motivosInvalidacion[index].desc_motivo_invalidacion;
        }else{return null;}
    }
    
}
