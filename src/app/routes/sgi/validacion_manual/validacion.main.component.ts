import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-validacion-main', 
    templateUrl: './validacion.main.component.html',
    styleUrls: ['./validacion.main.component.scss'],
    providers: []
})
export class ValidacionMainComponent implements OnInit {
    profile: any;
    started : boolean = false;
    selectedOption: string;
    sections = [];
    rootSection;
    rootSection_ec = {
        title: "Edición y confirmación",
        class: "fa fa-pencil "
    }
    rootSection_vm = {
        title: "Validación",
        class: "fa fa-check "
    }

    
    sections_ec = [{
        title: "Normal",
        route: "ec",
        public: true,
        selected: this._route.snapshot.url[0].path == "ec"
    }];
    sections_vm = [{
        title: "Normal",
        route: "vm",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm",
    },
    {
        title: "Sondas Multiparam",
        route: "vm_aquadam",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_aquadam",
    },
    {
        title: "Auscultación",
        route: "vm_auscul",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_auscul",
    },
    {
        title: "SAIHG",
        route: "vm_saihg",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_saihg",
    },
    {
        title: "Piezometría",
        route: "vm_piezo",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_piezo",
    }    
    ,
    {
        title: "REACAR",
        route: "vm_reacar",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_reacar",
    }         
    ,
    {
        title: "SAICA",
        route: "vm_saica",
        public: true,
        selected: this._route.snapshot.url[0].path == "vm_saica",
    }                 
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        
        if (this.selectedOption==='ec'){
            this.layoutService.setMenuSelected(constants.SGI.SECTIONS.EDICION_CONFIRMACION);
            this.sections = this.sections_ec;
            this.rootSection = this.rootSection_ec;
        }
        else{
            this.layoutService.setMenuSelected(constants.SGI.SECTIONS.VALIDACION_MANUAL);
            this.sections = this.sections_vm;
            this.rootSection = this.rootSection_vm; 
        }        
        this.profile = this._auditoriaService.getUserProfile();
        this.started = true;
    }
    selectOption(option) {
        this._router.onSameUrlNavigation = 'reload';
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
