
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { constants } from './../../../../config/constants';

import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel, BasicTableUtility } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
import { MatMenuTrigger } from "@angular/material";
import { Chart } from 'chart.js';
import 'chartjs-plugin-zoom';
import moment from 'moment';
import * as fs from 'file-saver';



@Component({
    selector: 'app-val_manual',
    templateUrl: './val.manual.component.html',
    styleUrls: ['./val.manual.component.scss'],
    providers: []
})
export class ValidacionManualComponent extends SGIListBaseComponent implements OnInit {
    @ViewChild('triggerMenuOperacionesMasivo') triggerMenuOperacionesMasivo: MatMenuTrigger;
    @ViewChild('modalInvalidar') modalInvalidar: any;
    @ViewChild('modalConfirmSave') modalConfirmSave: any;
    @ViewChild('modalEditarDiario') modalEditarDiario: any;
    @ViewChild('modalHistory') modalHistory: any;
    @ViewChild('modalConfirmDeselect') modalConfirmDeselect: any;
    @ViewChild('modalAddDatoDiario') modalAddDatoDiario: any;
    @ViewChild('modalFillGaps') modalFillGaps: any;
    @ViewChild('modalOtrasAcciones') modalOtrasAcciones: any;
    @ViewChild('modalHelp') modalHelp: any;
    @ViewChild('modalAdvertencia') modalAdvertencia: any;





    @ViewChild('modalExportData') modalExportData: any;
    @ViewChild('modalImportData') modalImportData: any;
    @ViewChild('modalCompletarHuecos') modalCompletarHuecos: any;
    @ViewChild('modalForzarSumarizacion') modalForzarSumarizacion: any;

    // Para el manejo de la gráfica de validación
    public chart: Chart = null;
    public ctxChart: CanvasRenderingContext2D;
    public ctxOverlay: CanvasRenderingContext2D;
    public selectedAreas = [];
    public canvas;
    public canvasOverlay;
    public dataSetsChart = [];
    public toolCharSelected: number = 1;
    public toolZoomEnabled: boolean = false;
    public hideInvalidosEnabled: boolean = false;
    public fillGapsEnabled: boolean = false;
    public TOOL_CHART_NONE_SELECTION = 1;
    public TOOL_CHART_SQUARE_SELECTION = 2;
    public TOOL_CHART_SINGLE_SELECTION = 3;

    // Para la exportación
    fechaIniExport;
    fechaFinExport;
    horaIniExport = "00:00";
    horaFinExport = "00:00";
    frecuenciaExport = 10;
    separadorExport = "P";
    codVariableExport;
    constantValueExport;
    variablesExport = [];
    msgConfirmImport;
    msgErrorImport;
    fileToUpload: File = null;
    modalRefImportar;
    msgFileOK;
    linesFile = [];
    variablesFile = [];
    msgErrorExport;
    importing = false;
    estrategiaImport = 'v';
    separadorImport = "P";
    validacionImport = "S";
    showValidarImport = true;
    msgErrorHoles;
    msgOKHoles;
    showConfirmarImport = false;
    listDataHistory = [];
    horas = [];
    // End Para la exportación
    rootSection = {}
    selectedTabIndex;
    sections = [];
    dependencyFilters = {};
    profile: any;
    codVariableHuecos;
    metodoInterpolacion = 3;
    fillingGaps;
    msgErrorGaps;
    msgOKGaps;
    areasToFill = [];
    frecuenciaGAP = 10;
    modeFillGaps = 1;
    tipoDatosGaps = 1;
    msgAdvertencia;
    public calculatingDataGaps: boolean = false;
    columnsTablaVariablesDatoBruto: any = [
        { id: "cod_estacion", filter: true, displayed: true, translation: "Cod. Estación" },
        { id: "desc_estacion", filter: true, displayed: true, translation: "Desc. Estación" },
        { id: "cod_variable", filter: true, displayed: true, translation: "Cod. Variable", primaryKey: true },
        { id: "desc_variable", filter: true, displayed: true, translation: "Desc. Variable" },

    ];
    columnsTablaVariablesDatoDiario: any = [
        { id: "cod_estacion", filter: true, displayed: true, translation: "Cod. Estación" },
        { id: "desc_estacion", filter: true, displayed: true, translation: "Desc. Estación" },
        { id: "cod_variable", filter: true, displayed: true, translation: "Cod. Variable", primaryKey: true },
        { id: "desc_variable", filter: true, displayed: true, translation: "Desc. Variable" },
        { id: "is_acum", filter: true, displayed: true, translation: "Acumulado" },
        //{ id: "add_data_diario", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Añadir dato manualmente", icon: "fa fa-plus", color: "blue" } },
    ];
    tableConfigurationVariables = {
        parentId: 'variables',
        id: 'table_variables',
        title: null,
        columns: [],
        editable: false,
        deletable: false,
        insertable: false,
        selectable: false,
        selectableAll: false,
        selectableUnique: false,
        serverPagination: false,
        showHeaderFilters: false,
        showMovementOptions: false,
        showExportExcel: false,
        msgNoResultados: "No se han seleccionado variables.",
        msgNumResultados: "variables",
        pageSize: constants.DEFAULT_LIMIT_PAGINATION
    };
    confTree: any = { isSelectable: true };

    invalidacionesUtility = new BasicTableUtility({
        id: "INVALIDACION_MASIVA",
        tooltip: "Invalidaciones masivas",
        faIcon: "fa-check-square-o",
        innerHtml: null
    });
    saveUtility = new BasicTableUtility({
        id: "SAVE",
        tooltip: "Guardar en base de datos",
        faIcon: " fa-floppy-o",
        innerHtml: null
    });
    deleteUtility = new BasicTableUtility({
        id: "DELETE",
        tooltip: "Eliminar todo",
        faIcon: " fa-trash-o",
        innerHtml: null
    });
    filter;
    filterSelection: any = {};
    tablaVariables = new BasicTableModel(this.tableConfigurationVariables);
    tablaMedidas;
    offset: number = 0;
    readyFilters = false;
    dataTree: any = [];
    variablesSelected: any = [];
    motivosInvalidacion: any = [];
    measures: any = [];
    minValueMeasuresScaleY;
    maxValueMeasuresScaleY;
    measuresHashByDateIndexes: any = {};
    measuresCache: any = {};
    showPanelTree: boolean = false;
    showPanelMeasures: boolean = false;
    showSelectedVariables: boolean = false;
    changeSelection: boolean = false;
    loadingTree: boolean = false;
    loadingMeasures: boolean = false;
    hasChanges: boolean = false;
    selectedMotivo: any;
    selectedData: any = [];
    tipoDato: string = constants.SGI.PERIODICIDAD_DATO.HORARIO;
    tipoRed: any = constants.SGI.TIPO_RED_VALIDACION.NORMAL;
    listVariables = [];
    valueEdition;
    measuresDeselected = 0;
    serieSelected = "";
    periodoSelected;
    esCalculadaSelected;
    multipleSerieSelected = "";
    varSelected;
    msgErrorOnCreate;
    pendientesSave = 0;
    pendientesSaveNumeroVar = 0;
    algunaInvalidacionMarcada: boolean = false;
    saving: boolean = false;
    fillingHoles: boolean = false;

    ngOnInit() {
        this.profile = this._securityService.getProfileFromLocalSesion();


        this.tipoDato = this._activatedRoute.snapshot.data['PERIODICIDAD'];
        this.tipoRed = this._activatedRoute.snapshot.data['RED'];

        if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
            this.tableConfigurationVariables.columns = this.columnsTablaVariablesDatoBruto;
            if (this.isValidacionNormal()) {
                this.columnsTablaVariablesDatoBruto.push({ id: "add_data_horario", filter: true, displayed: true, translation: "", 'type': 'ROWUTILITY', attributes: { tooltip: "Añadir dato manualmente", icon: "fa fa-plus", color: "blue" } });
            }
            this.tablaVariables = new BasicTableModel(this.tableConfigurationVariables);
        }
        else {
            this.tableConfigurationVariables.columns = this.columnsTablaVariablesDatoDiario;
            if (this.isValidacionNormal()) {
                this.columnsTablaVariablesDatoDiario.push({ id: "add_data_diario", filter: true, displayed: true, translation: "", "type": "ROWUTILITY", attributes: { tooltip: "Añadir dato manualmente", icon: "fa fa-plus", color: "blue" } });
            }
            this.tablaVariables = new BasicTableModel(this.tableConfigurationVariables);
        }
        this.configurateHeader();
        if (this.isValidacionNormal()) {
            this.initValidationNormal();
        }
        else {
            this.initValidationOtrasRedes();
        }
        this.horas = [];
        for (var i = 0; i <= 23; i++) {
            this.horas.push(String(i).padStart(2, '0') + ":00");
        }

    }
    isValidacionNormal() {
        return (this.tipoRed == constants.SGI.TIPO_RED_VALIDACION.NORMAL);
    }
    initValidationNormal() {
        this.searchMasterData().then((resp) => {
            this.configureFiltersNormal();
            this.initFilters();
            this.configureTable();
        });
    }
    initValidationOtrasRedes() {
        this.searchMasterData().then((resp) => {
            this.configureFiltersOtras();
            this.initFilters();
            this.configureTable();
        });
    }
    initFilters() {
        this.filter.elementMap["date_from"].selectedValue = moment().subtract(5, "days").format('YYYY-MM-DD');
        this.filter.elementMap["date_from"].dirty = true;
    }
    configurateHeader() {

        if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
            this.rootSection = {
                title: "Validación manual",
                class: "fa fa-table "
            }
        }
        else {
            this.rootSection = {
                title: "Edición y confirmación",
                class: "fa fa-check "
            }
        }
    }

    searchMasterData() {
        var self = this;
        var entity = constants.SGI.ENTITIES.MOTIVOS_INVALIDACION;
        var sort = " cod_motivo_invalidacion asc ";
        if (!this.isValidacionNormal()) {
            entity = constants.SGI.ENTITIES.MOTIVOS_INVALIDACION_27;
            sort = " cod_validez asc ";
        }

        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: entity, sort: sort }).subscribe(list => {
                if (this.isValidacionNormal()) {
                    this.motivosInvalidacion = list;
                }
                else {
                    this.motivosInvalidacion = list.filter(function (item) {
                        if (self.tipoRed === constants.SGI.TIPO_RED_VALIDACION.AUSCULTACION) {
                            return (item.es_auscultacion);
                        }
                        else if (self.tipoRed === constants.SGI.TIPO_RED_VALIDACION.REACAR || self.tipoRed === constants.SGI.TIPO_RED_VALIDACION.SAICA) {
                            return (item.es_calidad);
                        }
                        else if (self.tipoRed === constants.SGI.TIPO_RED_VALIDACION.PIEZO) {
                            return (item.es_piezo);
                        }
                        else if (self.tipoRed === constants.SGI.TIPO_RED_VALIDACION.SAIHG) {
                            return (item.es_saih);
                        }

                    });
                }
                resolve(1);
            });
        });
    }
    configureTable() {
        var columnsDatoBruto = [
            { id: "cod_variable", filter: true, displayed: true, translation: "Cod. Variable", primaryKey: true },
            { id: "date", filter: true, displayed: true, translation: "Fecha", primaryKey: true },
            { id: "value", filter: true, displayed: true, translation: "Valor" },
            { id: "valido", filter: true, displayed: true, translation: "Válido" },
            { id: "motivoInvalidacion.cod", filter: true, displayed: false, translation: "Cod. Motivo" },
            { id: "motivoInvalidacion.desc", filter: true, displayed: true, translation: "Desc Motivo" },
            { id: "SHOW_HISTORY", filter: true, displayed: true, translation: "", 'type': 'ROWUTILITY', attributes: { tooltip: "Ver historial", icon: "fa fa-history", color: "blue" } }
        ];
        var columnsDatoDiario = [
            { id: "cod_variable", filter: true, displayed: true, translation: "Cod. Variable", primaryKey: true },
            { id: "date", filter: true, displayed: true, translation: "Fecha", primaryKey: true, type: constantsCommon.BASIC_TABLE_TYPE_COLUMNS.DATE, attributes: { dateMask: 'dd/MM/yyyy HH:mm:ss' } },
            { id: "value", filter: true, displayed: true, translation: "Valor" },
            { id: "indConfirmacion", filter: true, displayed: true, translation: "Confirmado" },
            { id: "fiabilidad", filter: true, displayed: true, translation: "Fiabilidad (%)" },
            { id: "is_acum", filter: true, displayed: true, translation: "Acumulado" },
            { id: "EDIT_ROW_DIARIO", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Editar valor", icon: "fa fa-pencil", color: "blue" } },

        ];


        var tableConfigurationMedidas = {
            id: 'table_medidas',
            title: null,
            columns: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? columnsDatoBruto : columnsDatoDiario,
            utilities: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? [this.saveUtility, this.invalidacionesUtility] : [this.saveUtility],
            selectable: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO,
            selectableAll: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO,
            serverPagination: false,
            markDirtyRows: true,
            showHeaderFilters: false,
            showMovementOptions: false,
            msgNoResultados: "No se han encontrado medidas.",
            msgNumResultados: "medidas",
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "date",
                direction: "asc"
            }
        };
        this.tablaMedidas = new BasicTableModel(tableConfigurationMedidas);

    }
    searchTotalElements(filter) {
        var params = { entity: constants.SGI.ENTITIES.CONSTANTES, filters: filter }
        this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
            this.tablaVariables.setTotalElements(total);
        });
    }
    searchData(filter) {
        var params = { entity: constants.SGI.ENTITIES.CONSTANTES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: filter, sort: "desc_constante ASC" }
        this.tablaVariables.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {
            this.tablaVariables.setDataAndLoadTable(list);
        });
    }
    configureFiltersOtras() {
        this.filter = new BasicFilter({
            autoClose: true,
            initClosed: false,
            initSearch: false,
            sections: [
                { id: 1, name: "Localización", open: true, gridWidth: 4 },
                { id: 2, name: "Otros", open: true, gridWidth: 4 },
                { id: 3, name: "Tiempo", open: true, gridWidth: 4 }
            ]
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 3,
            width: 400,
            id: 'date_from',
            label: 'Fecha desde'
        });
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 3,
            width: 400,
            id: 'date_to',
            label: 'Fecha hasta'
        });
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 3,
            width: 400,
            id: 'invalidas',
            label: 'Sólo variables con medidas inválidas',
            code: 'COD',
            desc: 'DESCR',
            data: this._staticListService.getYesNoList()
        });



        if (this.tipoRed == constants.SGI.TIPO_RED_VALIDACION.PIEZO) {
            this.addFilterEstacionesPiezo27().then((resp) => {
                this.addFilterVariables27().then((resp) => { });
            });
        }
        else {
            this.addFilterEstaciones27().then((resp) => { this.addFilterVariables27().then((resp) => { }); });
        }


    }
    configureFiltersNormal() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: true,
            initClosed: false,
            initSearch: false,
            sections: [
                { id: 1, name: "Localización", open: true, gridWidth: 4 },
                { id: 2, name: "Otros", open: true, gridWidth: 4 },
                { id: 3, name: "Tiempo", open: true, gridWidth: 4 }
            ]
        });
        this.filter.considerEmptyError = false;

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 3,
            width: 400,
            id: 'date_from',
            label: 'Fecha desde'
        });
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.DATE_SELECT,
            group: 3,
            width: 400,
            id: 'date_to',
            label: 'Fecha hasta'
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 3,
            width: 400,
            id: 'conditions',
            label: 'Aplicar condiciones',
            code: 'COD',
            desc: 'DESCR',
            data: [{ COD: 1, DESCR: 'Todas' }, { COD: 2, DESCR: 'Alguna' }]
        });
        if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
            this.filter.extraActions = true;
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
                group: 3,
                width: 400,
                id: 'invalidas',
                label: 'Sólo variables con medidas inválidas',
                code: 'COD',
                desc: 'DESCR',
                data: this._staticListService.getYesNoList()
            });
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
                group: 3,
                width: 400,
                id: 'gaps',
                label: 'Sólo variables con huecos',
                code: 'COD',
                desc: 'DESCR',
                data: this._staticListService.getYesNoList()
            });
        }
        else {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
                group: 3,
                width: 400,
                id: 'no_confirmadas',
                label: 'Sólo variables con medidas no confirmadas',
                code: 'COD',
                desc: 'DESCR',
                data: this._staticListService.getYesNoList()
            });
        }

        this.addFilterOrigenDatos().then((resp) => {
            this.addFilterTipoEstacion().then((resp) => {
                this.addFilterTipoVariable().then((resp) => {
                    this.addFilterEstacion().then((resp) => {
                        this.addFilterVariables().then((resp) => {
                            this.addFilterZonas().then((resp) => {
                                this.addFilterAreas().then((resp) => {
                                    this.addFilterSubcuencas().then((resp) => {

                                    });
                                });
                            });
                        });
                    });
                });
            });
        });


        if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.DIARIO) {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.NUMBER_FIELD,
                group: 2,
                width: 200,
                id: 'fiabilidad_menor',
                label: 'Fiabilidad menor que',
                defaultValue: null,
            });
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.NUMBER_FIELD,
                group: 2,
                width: 200,
                id: 'fiabilidad_mayor',
                label: 'Fiabilidad mayor que',
                defaultValue: null,
            });
        }
    }

    loadVariables(event) {
        // CLEAR
        this.measures = null;
        this.measuresCache = {};
        this.serieSelected = null;
        //this.listVariables = [];  
        //console.log(event.filterSelection);

        if (this.isValidacionNormal()) {
            // Advertencia de la búsqueda de huecos
            var positionGaps = event.filterSelection.findIndex(item => item.id === "gaps");
            var positionDateTo = event.filterSelection.findIndex(item => item.id === "date_to");
            var aux = moment().format("YYYY-MM-DD");

            if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
                if (event.filterSelection[positionGaps].values[0] == 1 && (positionDateTo == -1 || event.filterSelection[positionDateTo].values >= aux)) {
                    this.msgAdvertencia = "Ha activado el filtro de búsqueda de series con huecos para un periodo de fechas que incluye el día de hoy (incompleto). La búsqueda de variables y datos se limitará <b>hasta el último día completo (ayer)</b>.";
                    this.modalService.open(this.modalAdvertencia, { size: 'sm', centered: true });

                    //alert(moment().add(-1, 'days').format("YYYY-MM-DD"));
                    var ayer = moment().subtract(1, "days").format('YYYY-MM-DD');
                    this.filter.elementMap["date_to"].selectedValue = ayer;
                    if (positionDateTo == -1) {
                        event.filterSelection.push({ id: "date_to", type: "DATE", values: ayer })
                    } else {
                        this.filter.elementMap["date_to"].selectedValue = ayer;
                        event.filterSelection[positionDateTo].values = ayer;
                    }
                }

            }
            this.loadVariablesNormal(event);
        }
        else {
            this.loadVariablesOtras(event);
        }
    }
    loadVariablesOtras(event) {
        this.showPanelTree = false;
        this.loadingTree = true;
        this.filterSelection = event.filterSelection;
        this.listVariables = [];
        var params = { filters: this.filterSelection, red: this.tipoRed }
        this._datoBrutoService.findVariablesPorRed(params).subscribe(list => {
            this.listVariables = list;
            this.measures = [];
            this.measuresCache = {};
            this.variablesSelected = [];

            this.tablaMedidas.setDataAndLoadTable(this.measures);
            this.tablaVariables.setDataAndLoadTable(this.variablesSelected);
            this.dataTree = this.setDataTree(list);
            this.loadingTree = true;
            this.showPanelTree = true;
            this.showSelectedVariables = true;
        });
    }

    loadVariablesNormal(event) {
        this.showPanelTree = false;
        this.loadingTree = true;
        this.filterSelection = event.filterSelection;
        this.listVariables = [];

        var params = { filters: this.filterSelection, sort: "desc_tipo_constante ASC" }
        
        if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
            this._datoBrutoService.findVariables(params).subscribe(list => {
                this.listVariables = list;

                this.measures = [];
                this.measuresCache = {};
                this.variablesSelected = [];

                this.tablaMedidas.setDataAndLoadTable(this.measures);
                this.tablaVariables.setDataAndLoadTable(this.variablesSelected);
                this.dataTree = this.setDataTree(list);
                this.loadingTree = false;
                this.showPanelTree = true;
                this.showSelectedVariables = true;
            });
        }
        else {
            
            this._datoDiarioService.findVariables(params).subscribe(list => {
                this.listVariables = list;
                this.measures = [];
                this.measuresCache = {};
                this.variablesSelected = [];
                this.tablaMedidas.setDataAndLoadTable(this.measures);
                this.tablaVariables.setDataAndLoadTable(this.variablesSelected);
                this.dataTree = this.setDataTree(list);
                this.loadingTree = false;
                this.showPanelTree = true;
                this.showSelectedVariables = true;
            });
        }
    }
    setDataTree(list) {
        var estaciones = [];
        var cod_estacion_ant;
        var estacion: any;
        for (var item of list) {
            if (item.estacion.cod === cod_estacion_ant) {
                estacion.attributes.invalids = estacion.attributes.invalids || item.hasDataInvalids;

                estacion.children.push({
                    cod: item.cod,
                    name: item.cod + " - " + item.desc,

                    attributes: {
                        cod_variable: item.cod,
                        desc_variable: item.desc,
                        cod_estacion: item.estacion.cod,
                        desc_estacion: item.estacion.desc,
                        invalids: item.hasDataInvalids, // Solo para minutales
                        gaps: item.hasGaps,
                        periodo: item.periodo,
                        es_calculada: item.es_calculada,
                        info: item.info,
                        is_acum: item.is_acum // Solo para diarios
                    }
                });

            }
            else {
                estacion = {
                    cod: item.estacion.cod,
                    name: item.estacion.cod + " - " + item.estacion.desc,
                    attributes: {
                        invalids: item.hasDataInvalids,
                        gaps: item.hasGaps,
                    },
                    children: [{
                        cod: item.cod,
                        name: item.cod + " - " + item.desc,
                        attributes: {
                            cod_variable: item.cod,
                            desc_variable: item.desc,
                            cod_estacion: item.estacion.cod,
                            desc_estacion: item.estacion.desc,
                            invalids: item.hasDataInvalids, // Solo para minutales
                            gaps: item.hasGaps,
                            info: item.info,
                            es_calculada: item.es_calculada,
                            periodo: item.periodo,
                            is_acum: item.is_acum // Solo para diarios
                        }
                    }]
                }
                cod_estacion_ant = estacion.cod;
                estaciones.push(estacion);
            }
        }
        return estaciones;
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(this.filterSelection);
    }
    onExecuteTableUtilityAction(event) {

        this.selectedMotivo = null;
        var self = this;
        if (event.action == "INVALIDACION_MASIVA") {
            this.selectedData = [];
            for (var measure of this.tablaMedidas.getSelection()) {
                self.selectedData.push({
                    valido: measure.valido,
                    motivoInvalidacion: measure.motivoInvalidacion,
                    cod_variable: measure.cod_variable,
                    date: moment(measure.date).format("DD/MM/YYYY HH:mm:ss"),
                    value: measure.value
                });

            }
            self.selectedMotivo = null;
            this.modalService.open(this.modalInvalidar, { size: 'lg', centered: true, keyboard: false, backdrop: false });

        }
        else if (event.action == "SAVE") {
            this.openModalSave();
        }
        else if (event.action == "DELETE") {
            alert("todo");
        }
    }

    openModalSave() {
        this.setNumeroPendientesPendientesSave();
        this.saving = false;
        this.modalService.open(this.modalConfirmSave, { size: 'lg', centered: true, keyboard: false, backdrop: false });

    }
    onChangeSelectionTree(node) {

        const position = this.variablesSelected.findIndex(({ cod_variable }) => cod_variable === node.attributes.cod_variable);

        if (node.selected) {
            if (position == -1) {


                var variable = {
                    cod_estacion: node.attributes.cod_estacion,
                    desc_estacion: node.attributes.desc_estacion,
                    cod_variable: node.attributes.cod_variable,
                    desc_variable: node.attributes.desc_variable,
                    is_acum: node.attributes.is_acum,
                    periodo: node.attributes.periodo,
                    es_calculada: node.attributes.es_calculada,
                    selected: true
                };

                this.variablesSelected.push(variable);

            }
        } else {
            if (position >= 0) {

                var tmp = this.measures.filter(function (measure) { return measure.cod_variable == node.attributes.cod_variable });
                this.measuresDeselected = tmp.length;
                if (this.measuresDeselected > 0) {
                    this.modalService.open(this.modalConfirmDeselect, { size: 'sm', centered: true });
                }

                this.variablesSelected.splice(position, 1);
                this.measures = this.measures.filter(function (measure) { return measure.cod_variable != node.attributes.cod_variable });

                this.tablaMedidas.setDataAndLoadTable(this.measures);

            }
        }
        this.showPanelMeasures = (this.variablesSelected.length > 0);
        this.changeSelection = true;
        this.tablaVariables.setDataAndLoadTable(this.variablesSelected);
    }



    loadDataSerie(selected, variable) {
        return new Promise((resolve, reject) => {
            if (selected) {
                if (this.measuresCache[variable.cod_variable] && this.measuresCache[variable.cod_variable].datos) {
                    this.measures = this.measuresCache[variable.cod_variable].datos;
                    this.measuresHashByDateIndexes = this.measuresCache[variable.cod_variable].indices;
                    resolve(1);
                    return;
                }
                this.loadingMeasures = true;
                var date_from;
                var date_to;
                if (this.filter.elementMap['date_from']) {
                    var date_from = this.filter.elementMap['date_from'].getValue();
                }
                if (this.filter.elementMap['date_to']) {
                    var date_to = this.filter.elementMap['date_to'].getValue();
                }
                var filters = {};
                if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
                    filters = {
                        variables: { id: "cod_variable", values: [variable.cod_variable], type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING },
                        date_from: { id: "date_from", values: [date_from], type: constantsCommon.TYPE_DATA_FILTER.DATE },
                        date_to: { id: "date_to", values: [date_to], type: constantsCommon.TYPE_DATA_FILTER.DATE }
                    };

                    if (this.isValidacionNormal()) {
                        //alert(filters);
                        this._datoBrutoService.findData({ filters: filters }).subscribe(list => {


                            // Prueba de visualización de relleno de huecos
                            /*for (var i=100;i<=300;i++){
                                 list[i].value=-9999;
                                 list[i].valido='N';
                             }*/

                            // >> this.measures = this.measures.concat(list);                            
                            this.measures = list;
                            this.measuresHashByDateIndexes = {};
                            for (var i = 0; i < this.measures.length; i++) {
                                this.measuresHashByDateIndexes[moment(this.measures[i].date, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss").trim()] = i;
                            }
                            this.measuresCache[variable.cod_variable] = { datos: this.measures, indices: this.measuresHashByDateIndexes }
                            this.loadingMeasures = false;
                            resolve(1);
                        });
                    }
                    else {
                        this._datoBrutoService.findDataPorRed({ filters: filters, red: this.tipoRed }).subscribe(list => {
                            this.measures = list;
                            this.measuresHashByDateIndexes = {};
                            for (var i = 0; i < this.measures.length; i++) {
                                this.measuresHashByDateIndexes[moment(this.measures[i].date, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss").trim()] = i;
                            }
                            this.measuresCache[variable.cod_variable] = { datos: this.measures, indices: this.measuresHashByDateIndexes }
                            this.loadingMeasures = false;
                            resolve(1);
                        });
                    }
                }
                else {
                    filters = {
                        variables: { id: "cod_variable", values: [variable.cod_variable], type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING },
                        date_from: { id: "date_from", values: [date_from], type: constantsCommon.TYPE_DATA_FILTER.DATE },
                        date_to: { id: "date_to", values: [date_to], type: constantsCommon.TYPE_DATA_FILTER.DATE },
                        is_acum: { id: "is_acum", values: variable.is_acum ? [1] : [0], type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                    };
                    this._datoDiarioService.findData({ filters: filters }).subscribe(list => {
                        this.measures = list;
                        this.measuresHashByDateIndexes = {};
                        for (var i = 0; i < this.measures.length; i++) {
                            this.measuresHashByDateIndexes[moment(this.measures[i].date, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss").trim()] = i;
                        }
                        this.measuresCache[variable.cod_variable] = { datos: this.measures, indices: this.measuresHashByDateIndexes }
                        this.loadingMeasures = false;
                        resolve(1);
                    });
                }
            }
            else {
                // Mostramos el aviso de des-selección
                var tmp = this.measures.filter(function (measure) { return measure.cod_variable == variable.cod_variable });
                this.measuresDeselected = tmp.length;
                if (this.measuresDeselected > 0) {
                    this.modalService.open(this.modalConfirmDeselect, { size: 'sm', centered: true });
                    this.measures = this.measures.filter(function (measure) { return measure.cod_variable != variable.cod_variable });
                    this.tablaMedidas.setDataAndLoadTable(this.measures);
                    this.loadingMeasures = false;
                    resolve(1);
                }
                else {
                    resolve(1);
                }
            }


        });
    }

    cerrarDeselect() {
        this.modalService.dismissAll();
    }
    setNumeroPendientesPendientesSave() {
        var counter = 0;
        var counter2 = 0;
        for (var variable of this.variablesSelected) {
            if (this.measuresCache[variable.cod_variable] && this.measuresCache[variable.cod_variable].datos) {
                var temp = this.measuresCache[variable.cod_variable].datos.filter(x => x.dirty === true);

                if (temp) {
                    counter += temp.length;
                    if (counter > 0) {
                        counter2++;
                    }
                }
            }
        }
        this.pendientesSave = counter;
        this.pendientesSaveNumeroVar = counter2;
    }

    saveToDatabase(measures) {


        return new Promise((resolve, reject) => {
            var params = {};
            if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
                if (this.isValidacionNormal()) {
                    params = {
                        measures: measures,
                        cod_usuario: this.profile.cod_usuario,
                        operation: 'EM'
                    };
                    console.log(this.profile);
                    console.log(params);
                    this._datoBrutoService.saveData(params).subscribe(list => {
                        resolve(1);
                    });
                }
                else {
                    params = {
                        measures: measures,
                        cod_usuario: this.profile.cod_usuario,
                        operation: 'EM',
                        red: this.tipoRed,
                    };
                    this._datoBrutoService.saveDataPorRed(params).subscribe(list => {
                        resolve(1);
                    });
                }
            }
            else {
                params = {
                    measures: measures,
                    cod_usuario: this.profile.cod_usuario,
                    operation: 'EM'
                };
                if (this.isValidacionNormal()) {
                    this._datoDiarioService.saveData(this.measures).subscribe(list => {
                        resolve(1);
                    });
                }
                else {
                    // NOT IMPLEMENTED
                }
            }
        });
    }
    confirmSave() {
        var self = this;
        var promises = [];
        this.saving = true;
        for (var variable of this.variablesSelected) {
            if (this.measuresCache[variable.cod_variable] && this.measuresCache[variable.cod_variable].datos) {
                var dirties = this.measuresCache[variable.cod_variable].datos.filter(function (measure) { return measure.dirty });

                var p = this.saveToDatabase(dirties).then((result) => {
                });
                promises.push(p);

            }
        }

        Promise.all(promises).then(result => {
            self.modalService.dismissAll();
            this.saving = false;
            for (var variable of this.variablesSelected) {

                if (this.measuresCache[variable.cod_variable] && this.measuresCache[variable.cod_variable].datos) {
                    for (var value of this.measuresCache[variable.cod_variable].datos) {
                        value.dirty = false;
                    }
                    // Clean cache (excepto la actual)
                    if (variable.cod_variable != this.serieSelected) {
                        this.measuresCache[variable.cod_variable] = null;
                    }
                }
            }
            // Actualizo
            this.setNumeroPendientesPendientesSave();
            this.showChart();
            this.chart.update();
        });

        /*
                if (this.tipoDato == constants.SGI.PERIODICIDAD_DATO.HORARIO) {
                    if (this.isValidacionNormal()){
                        var dirties = this.measures.filter(function (measure) { return measure.dirty});
                        this._datoBrutoService.saveData(dirties).subscribe(list => {
                            for (var item of self.measures) {
                                item.dirty = false;
                            }
                            self.modalService.dismissAll();
                            this.tablaMedidas.setDataAndLoadTable(this.measures);
                            this.showChart();
                            this.chart.update();
                        });
                    } 
                    else{
                        var dirties = this.measures.filter(function (measure) { return measure.dirty});
                        var params = { measures: dirties, red : this.tipoRed }
                        this._datoBrutoService.saveDataPorRed(params).subscribe(list => {
                            for (var item of self.measures) {
                                item.dirty = false;
                            }
                            self.modalService.dismissAll();
                            this.tablaMedidas.setDataAndLoadTable(this.measures);
                            this.showChart();
                        });
                    }
                }
                else {
                    if (this.isValidacionNormal()){
                        this._datoDiarioService.saveData(this.measures).subscribe(list => {
                            for (var item of self.measures) {
                                item.dirty = false;
                            }
                            self.modalService.dismissAll();
                            this.tablaMedidas.setDataAndLoadTable(this.measures);
                        });
                    }
                    else{
                        alert('todo');
                    }
                }                            
        
        */
    }

    onChangeModalAllSelection(e) {

        for (var item of this.selectedData) {
            item.selected = e.target.checked;
        }
    }
    closeInvalidar() {
        this.modalService.dismissAll();
    }
    marcarFilasInvalidas() {
        if (this.selectedMotivo) {
            var motivo = this.motivosInvalidacion.find(({ cod_motivo_invalidacion }) => cod_motivo_invalidacion.trim() === this.selectedMotivo.trim());
            if (this.selectedMotivo != -1) {
                var valido = 'N';
            } else {
                var valido = 'S';
            }

            for (var item of this.selectedData) {
                if (item.selected) {
                    item.valido = valido;
                    item.dirty = true;
                    item.motivoInvalidacion.cod = this.selectedMotivo;
                    item.motivoInvalidacion.desc = motivo.desc_motivo_invalidacion;
                    this.algunaInvalidacionMarcada = true;
                }
            }
        }
        else {
            alert('Debe selecionar algún  motivo de la lista');
        }
    }
    onTabChanged() {
        if (this.selectedTabIndex == 1) {
            this.tablaMedidas.startLoading();
            this.tablaMedidas.setDataAndLoadTable(this.measures);
        }
    }
    invalidarMasivo() {
        for (var item of this.selectedData) {
            var position = this.measuresHashByDateIndexes[item.date.trim()];

            //var position = this.measures.findIndex(x => moment(x.date, "YYYY-MM-DD HH:mm:ss").format("DD/MM/YYYY HH:mm:ss").trim() === item.date.trim());
            if (position >= 0) {
                this.measures[position].motivoInvalidacion.cod = item.motivoInvalidacion.cod;//this.selectedMotivo;
                this.measures[position].motivoInvalidacion.desc = item.motivoInvalidacion.desc//; =motivo.desc_motivo_invalidacion;;
                this.measures[position].valido = item.valido;//; =motivo.desc_motivo_invalidacion;;
                this.measures[position].dirty = item.dirty;

            }
        }

        //this.tablaMedidas.setDataAndLoadTable(this.measures);   // Esto se puede omitir aqui         
        this.generateDatasetsForChart();
        this.chart.data.datasets[0] = this.dataSetsChart[0];
        this.chart.update();
        this.modalService.dismissAll();
        this.hasChanges = true;
    }
    changeSerieChart() {
        this.clearCanvas();
        this.selectedAreas = [];
        this.areasToFill = [];
        this.periodoSelected = null;
        this.esCalculadaSelected = null;
        if (this.serieSelected != "") {
            var index = this.variablesSelected.findIndex(x => x.cod_variable === this.serieSelected);
            this.periodoSelected = this.variablesSelected[index].periodo;
            this.esCalculadaSelected = this.variablesSelected[index].es_calculada;
            this.loadDataSerie(true, this.variablesSelected[index]).then((resp) => {
                this.tablaMedidas.setDataAndLoadTable(this.measures);
                this.toolCharSelected = this.TOOL_CHART_NONE_SELECTION;
                this.showChart();
            });
        }
    }

    changeSerieDiaria() {
        if (this.serieSelected != "") {
            var index = this.variablesSelected.findIndex(x => x.cod_variable === this.serieSelected);
            this.loadDataSerie(true, this.variablesSelected[index]).then((resp) => {
                this.tablaMedidas.setDataAndLoadTable(this.measures);
            });
        }
    }

    changeChartTool() {

        var allowZoomAndPan = (this.toolCharSelected == this.TOOL_CHART_NONE_SELECTION);
        this.chart.options.plugins.zoom.pan.enabled = allowZoomAndPan;
        this.chart.options.plugins.zoom.zoom.enabled = allowZoomAndPan;
        if (this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
            this.chart.ctx.canvas.removeEventListener('wheel', this.chart._wheelHandler);
        }
        else {
            this.chart.ctx.canvas.addEventListener('wheel', this.chart._wheelHandler);
        }

        this.chart.update();
        if (this.toolCharSelected == this.TOOL_CHART_NONE_SELECTION) {
            this.clearCanvas();
            this.selectedAreas = [];
            this.areasToFill = [];
            //this.canvas.style.cursor = "move";
        }
        else {
            //this.canvas.style.cursor = "default";
        }

    }

    resetZoom() {
        this.chart.resetZoom();
        this.clearCanvas();
        this.selectedAreas = [];
        this.areasToFill = [];
    }
    openHelp() {
        this.modalService.open(this.modalHelp, { size: 'sm', centered: true });
    }

    generateDatasetsForChart() {
        var self = this;
        var sf;
        if (this.hideInvalidosEnabled) {
            sf = this.measures.filter(function (measure) { return measure.cod_variable == self.serieSelected && measure.valido != "N" });
        } else {
            sf = this.measures.filter(function (measure) { return measure.cod_variable == self.serieSelected });
        }

        var series = {};
        var pointBackgroundColors = [];

        this.minValueMeasuresScaleY = 99999999;
        this.maxValueMeasuresScaleY = -9999999;

        for (var measure of sf) {
            if (!series[measure.cod_variable]) {
                series[measure.cod_variable] = {
                    values: []
                };
            }
            series[measure.cod_variable].values.push({
                x: moment(measure.date).toDate(),
                y: measure.value,
                valido: measure.valido,
                dirty: measure.dirty,
                origen: measure.origen,
                motivoInvalidacion: measure.motivoInvalidacion
            });

            if (parseFloat(measure.value) > parseFloat(this.maxValueMeasuresScaleY)) {
                this.maxValueMeasuresScaleY = measure.value;
            }
            if (parseFloat(measure.value) < parseFloat(this.minValueMeasuresScaleY)) {
                this.minValueMeasuresScaleY = measure.value;
            }

            if (measure.valido === 'S') {

                if (measure.dirty) {
                    pointBackgroundColors.push("orange");
                }
                else {
                    if (measure.origen == 'M1') {
                        pointBackgroundColors.push("black");
                    }
                    else {
                        pointBackgroundColors.push("green");
                    }
                }
            }
            else if (measure.valido === 'N') {
                if (measure.dirty) {
                    pointBackgroundColors.push("orange");
                } else {
                    pointBackgroundColors.push("red");
                }
            }
            else {
                pointBackgroundColors.push("black");
            }
        }
        this.dataSetsChart = [];
        var i = 0;

        for (var serie in series) {
            var color = this._graphService.generateColor(i);
            this.dataSetsChart.push({
                //borderColor: color.serie,
                label: serie,

                data: series[serie].values,
                pointBackgroundColor: pointBackgroundColors,
                //pointStyle: pointStyle,
            });
            i++;
        }

    }
    showChart() {
        var self = this;
        if (this.chart) {
            this.chart.destroy();
        }

        this.clearCanvas();
        this.selectedAreas = [];
        this.areasToFill = [];


        var mode = 'x';
        if (this.toolZoomEnabled) {
            mode = 'xy';

        }

        /*if (this.toolZoomEnabled){
            mode = 'xy';
            minScaleY =   99999999;
            maxScaleY =  -9999999;
        }*/

        this.generateDatasetsForChart();
        var minScaleY = Math.floor(this.minValueMeasuresScaleY);
        var maxScaleY = Math.ceil(this.maxValueMeasuresScaleY);
        if (this.toolZoomEnabled) {
            mode = 'xy';
            minScaleY = this.minValueMeasuresScaleY;
            maxScaleY = this.maxValueMeasuresScaleY;

        }

        //console.log("min " + minScaleY);
        //console.log("max " + maxScaleY);

        var timeFormat = 'DD/MM/YYYY hh:mm:ss';
        var options: any = {
            type: 'line',
            spanGaps: true,
            data: { datasets: this.dataSetsChart },
            options: {
                plugins: {
                    zoom: {
                        pan: {
                            enabled: true,
                            mode: mode
                        },
                        zoom: {
                            enabled: true,
                            sensitivity: 3,
                            threshold: 2,
                            speed: 0.2,
                            mode: mode,
                            rangeMax: {

                            },
                            drag: false
                        }
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            /*var label = data.datasets[tooltipItem.datasetIndex].label || '';
                            if (label) {
                                label += ': ';
                            }                   */
                            //multiLabel.push(label);
                            var label = "Valor: " + Math.round(tooltipItem.yLabel * 100) / 100;
                            var multiLabel = [label];
                            var motivo_desc = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].motivoInvalidacion.desc;
                            //var motivo_cod = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].motivoInvalidacion.cod;
                            var valido = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].valido;
                            var origen = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].origen;
                            //console.log(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                            if (valido == 'S') {
                                //label += " (Válido)";
                                multiLabel.push("Estado: Válido");
                            }
                            if (motivo_desc && valido && valido == 'N') {
                                multiLabel.push("Estado: Inválido");
                                multiLabel.push("Mótivo invalidación: " + motivo_desc);
                                //label += " (Inválido - " + motivo_desc + ")";
                            }
                            if (origen) {
                                multiLabel.push("Origen: " + origen);
                            }

                            return multiLabel;
                            //return label;
                        },
                        labelColor: function (tooltipItem, chart) {
                            return {
                                //borderColor: 'rgb(255, 0, 0)',
                                //backgroundColor: 'rgb(255, 0, 0)'
                            };
                        },
                        labelTextColor: function (tooltipItem, chart) {
                            return 'white';
                        }
                    }
                },
                scales: {
                    yAxes: [],
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'hour',
                            stepSize: "1",
                            tooltipFormat: 'DD/MM/YYYY HH:mm',
                            displayFormats: {
                                millisecond: 'HH:mm',
                                second: 'HH:mm',
                                minute: 'HH:mm',
                                hour: 'DD/MM  HH:mm'
                            }
                        },
                        ticks: {
                            display: true,

                        }
                    }]
                },
                title: {
                    display: false,
                    text: 'Validación de datos brutos'
                }
            }
        };

        if (!this.toolZoomEnabled) {
            options.options.scales.yAxes.push({
                ticks: { min: minScaleY, max: maxScaleY }
            });
        }
        //console.log(options);

        this.chart = new Chart('graphContainer', options);
        this.canvas = document.getElementById('graphContainer');
        this.ctxChart = this.canvas.getContext('2d');
        this.canvasOverlay = document.getElementById('overlay');
        this.ctxOverlay = this.canvasOverlay.getContext('2d');
        this.canvasOverlay.width = this.canvas.width;
        this.canvasOverlay.height = this.canvas.height;
        this.ctxOverlay.fillStyle = "#FF0000";
        this.ctxOverlay.globalAlpha = 0.2;
        var currentRect: any = {};
        var drawing = false;




        // Canvas events 

        this.canvas.addEventListener('pointerdown', evt => {
            if (this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
                currentRect.startX = evt.clientX;
                currentRect.startY = evt.clientY;
                currentRect.startIndex = this.getIndexOnCanvas(evt);
                let axisYValue = this.chart.scales['y-axis-0'].getValueForPixel(evt.offsetY);
                currentRect.startYAxisValue = axisYValue;
                drawing = true;
            }
        });
        this.canvas.addEventListener('pointermove', evt => {
            if (drawing && this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
                this.clearCanvas();
                this.drawAddedAreas();
                this.drawArea(evt, currentRect.startX, currentRect.startY, currentRect.startIndex, this.getIndexOnCanvas(evt), -1, -1, false);
            }
        });

        this.canvas.addEventListener('pointerup', evt => {
            if (this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
                var axis = this.chart.scales["y-axis-0"];
                let endYAxisValue = this.chart.scales['y-axis-0'].getValueForPixel(evt.offsetY);
                this.drawArea(evt, currentRect.startX, currentRect.startY, currentRect.startIndex, this.getIndexOnCanvas(evt), currentRect.startYAxisValue, endYAxisValue, true);
                drawing = false;
            }
        });
        /*
                this.canvas.addEventListener('contextmenu', function(evt) {
        
                    if (this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
                        if (!drawing) {
                            var indexArea = this.checkOverArea(evt);
                            //console.log(indexArea);
                            if (indexArea >= 0) {
        
                                this.modalService.open(this.modalFillGaps, { size: 'lg', centered: true });
                            }
                        }
                    }            
                    
                });*/

        this.canvas.addEventListener('click', evt => {

            if (this.toolCharSelected == this.TOOL_CHART_SINGLE_SELECTION) {
                var points = this.chart.getElementsAtEvent(evt);
                if (points && points[0]) {
                    this.openSingleValidation(points[0]._index);
                }

            }
            if (this.toolCharSelected == this.TOOL_CHART_SQUARE_SELECTION) {
                if (!drawing) {
                    var indexArea = this.checkOverArea(evt);
                    if (indexArea >= 0) {
                        this.openValidation(indexArea);
                    }
                }
            }
        });
        this.chart.update();
    }
    /*changeToolChart(idTool){
        this.toolCharSelected = idTool;
    }*/
    activateZoom() {
        this.showChart();
    }
    ocultarInvalidos() {
        this.showChart();
    }
    getIndexOnCanvas(evt) {

        const points = this.chart.getElementsAtEventForMode(evt, 'index', {
            intersect: false
        });

        // Index position
        if (points && points.length > 0) {
            return points[0]._index
        }
        else {
            //console.log('getIndexOnCanvas -1');
            return -1;
        }
    }

    openSingleValidation(indexData) {
        var self = this;
        this.selectedData = [];
        var tmp = this.dataSetsChart[0].data[indexData];
        if (tmp) {
            this.selectedData.push({
                selected: true,
                valido: tmp.valido,
                motivoInvalidacion: tmp.motivoInvalidacion,
                cod_variable: this.dataSetsChart[0].label,
                date: moment(tmp.x).format("DD/MM/YYYY HH:mm:ss"),
                value: tmp.y
            });

        }
        this.algunaInvalidacionMarcada = false;
        self.modalService.open(self.modalInvalidar, { size: 'lg', centered: true, keyboard: false, backdrop: false });
    }
    openValidation(indexArea) {
        var self = this;
        this.selectedData = [];
        this.selectedMotivo = null;
        for (var j = 0; j < this.selectedAreas.length; j++) {
            if (indexArea == -1 || indexArea == j) {
                var area = this.selectedAreas[j];
                for (var i = area[4]; i <= area[5]; i++) {
                    var tmp = this.dataSetsChart[0].data[i];
                    if (tmp) {
                        // Check height of reactangle                  
                        var yMax = Math.max(area[6], area[7]);
                        var yMin = Math.min(area[6], area[7]);
                        if (tmp.y >= yMin && tmp.y <= yMax) {
                            this.selectedData.push({
                                selected: true,
                                valido: tmp.valido,
                                motivoInvalidacion: tmp.motivoInvalidacion,
                                cod_variable: this.dataSetsChart[0].label,
                                date: moment(tmp.x).format("DD/MM/YYYY HH:mm:ss"),
                                value: tmp.y
                            });
                        }
                    }
                }
            }
        }
        this.algunaInvalidacionMarcada = false;
        self.modalService.open(self.modalInvalidar, { size: 'lg', centered: true, keyboard: false, backdrop: false });
    }
    btnRefresh() {
        this.selectedAreas = [];
        this.areasToFill = [];
        var index = this.variablesSelected.findIndex(x => x.cod_variable === this.serieSelected);
        if (index >= 0) {
            if (this.measuresCache[this.serieSelected]) {
                this.measuresCache[this.serieSelected] = null;
                this.measuresCache[this.serieSelected] = [];
            }
        }
        this.changeSerieChart()
    }
    btnClearCanvas() {
        this.clearCanvas();
        this.selectedAreas = [];
        this.areasToFill = [];
    }

    btnUndo() {
        this.selectedAreas.splice(-1);
        this.clearCanvas();
        this.drawAddedAreas();
    }
    clearCanvas() {
        if (this.ctxOverlay) {
            this.ctxOverlay.clearRect(0, 0, this.canvasOverlay.width, this.canvasOverlay.height);
        }
    }

    checkOverArea(evt) {
        const rect = this.canvas.getBoundingClientRect();
        var x = evt.clientX - rect.left;
        var y = evt.clientY - rect.top;
        var selected = -1;
        for (var i = 0; i < this.selectedAreas.length; i++) {
            var area = this.selectedAreas[i];

            if ((x > area[0]) && (x < (area[0] + area[2])) && (y > area[1]) && (y < (area[1] + area[3]))) {
                selected = i;
            }
        }
        return selected;

    }
    drawAddedAreas() {
        for (var i = 0; i < this.selectedAreas.length; i++) {
            var area = this.selectedAreas[i];
            this.ctxOverlay.fillRect(area[0], area[1], area[2], area[3]);
        }

    }

    drawArea(evt, startX, startY, startIndex, endIndex, startYAxisValue, endYAxisValue, addToList) {
        const rect = this.canvas.getBoundingClientRect();
        var x = startX - rect.left;
        var y = startY - rect.top;
        var width = evt.clientX - startX;
        var height = evt.clientY - startY;
        if (Math.abs(width) > 3 && Math.abs(height) >= 3) { // Un mínimo de selección
            if (width < 0) {
                x = x - Math.abs(width);
                width = width * -1;
            }
            if (height < 0) {
                y = y - Math.abs(height);
                height = height * -1;
            }

            if (addToList) {

                var start = startIndex;
                var end = endIndex;

                // Ponemos bien inicios y finales en función de la dirección en la que el rectángulo haya sido dibujado
                if (startIndex > endIndex) {
                    startIndex = end;
                    endIndex = start;
                }
                var startRect = startX - rect.left;
                var endRect = evt.clientX - rect.left;
                if (startRect > endRect) {
                    var temp = endRect;
                    endRect = startRect;
                    startRect = temp;
                }

                //console.log('Rectangulo [' + startRect + ',' + endRect + ']');

                // Obtenemos posiciones en cambas de los puntos elegidos por defecto
                var meta = this.chart.getDatasetMeta(0);
                var x_start = meta.data[startIndex]._model.x;
                var x_end = meta.data[endIndex]._model.x;

                // Ajustamos índices
                if (x_start < startRect && (startIndex < this.dataSetsChart[0].data.length)) {
                    startIndex = startIndex + 1;
                }
                if (x_end > endRect && (endIndex > 0)) {
                    endIndex = endIndex - 1;
                }

                // Evito crear  áreas duplicads
                var add = true;
                for (var area of this.selectedAreas) {
                    if (area[0] == x && area[1] == y) {
                        add = false;
                    }
                }
                if (add) {
                    this.selectedAreas.push([x, y, width, height, startIndex, endIndex, startYAxisValue, endYAxisValue]);
                }

                //this.selectedAreas


            }
            this.ctxOverlay.fillRect(x, y, width, height);
        }
    }
    cleanGraphRangeSelection() {

    }
    onExtraAction() {
        this.modalService.open(this.modalOtrasAcciones, { size: 'sm', centered: true, keyboard: false, backdrop: false });
    }
    onClearFilter() {
        this.selectedData = [];
        this.dataTree = [];
        this.showPanelTree = false;
        this.showPanelMeasures = false;
        this.loadingMeasures = false;
        this.loadingTree = false;
    }
    onExecuteTableRowUtilityAction(event) {
        var self = this;
        this.selectedData = event.row;
        this.valueEdition = event.row.value;
        if (event.action == "EDIT_ROW_DIARIO") {
            this.modalService.open(this.modalEditarDiario, { size: 'sm', centered: true, keyboard: false, backdrop: false });
        }
        else if (event.action == "SHOW_HISTORY") {
            self.listDataHistory = [];
            var data = {
                cod_variable: event.row.cod_variable,
                timestamp_medicion: event.row.date
            };

            self._datoBrutoService.showHistory(data).subscribe(data => {
                self.listDataHistory = data;
                this.modalService.open(this.modalHistory, { size: 'lg', centered: true });
            });
        }
    }
    confirmEditionValue() {
        this.selectedData.value = this.valueEdition;
        this.selectedData.dirty = true;
        this.modalService.dismissAll();

    }

    onChangeFilterComponent(data) {
        var value;
        var filtersStacion = [];
        var filtersVariable = [];
        if (this.isValidacionNormal()) {

            if (data.element === 'cod_zona' || data.element === "t1.cod_fuente_origen" ||
                data.element == "t4.cod_subcuenca" || data.element == "t1.cod_tipo_variable") {
                value = data.info.msModel.selectedElements;
            } else {
                value = data.value;
            }

            if (data.element == 't1.cod_fuente_origen') {   // Si cambia fuente origen actualizamos estaciones
                var values = [];
                for (var item of data.info.msModel.selectedElements) {
                    values.push(item.COD);
                }
                if (values.length > 0) {
                    filtersStacion.push({ id: "cod_fuente_origen", type_condition: 'AND', type: "ARRAY_STRING", values: values });
                    filtersVariable.push({ id: "t1.cod_fuente_origen", type_condition: 'AND', type: "ARRAY_STRING", values: values });
                }
                var aux = "t1.cod_estacion";
                this.dependencyFilters[aux] = filtersStacion;
                this.filter.elementMap[aux].clean();
                this.filter.elementMap[aux].refresh();

                aux = this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 't1.cod_variable' : 't4.cod_variable';
                this.dependencyFilters[aux] = filtersVariable;
                this.filter.elementMap[aux].clean();
                this.filter.elementMap[aux].refresh();

            }
            if (data.element == 't1.cod_estacion') {

                var values = [];
                for (var item of value) {
                    values.push(item.COD);
                }
                if (values.length > 0) {
                    filtersVariable.push({ id: "t1.cod_estacion", type_condition: 'AND', type: "ARRAY_STRING", values: values });
                }
                aux = this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 't1.cod_variable' : 't4.cod_variable';
                this.dependencyFilters[aux] = filtersVariable;
                this.filter.elementMap[aux].clean();
                this.filter.elementMap[aux].refresh();

            }
        }
        else {

            if (data.element == 'cod_estacion') {
                var values = [];
                value = data.info.msModel.selectedElements;

                for (var item of value) {
                    values.push(item.COD);
                }
                if (values.length > 0) {
                    filtersVariable.push({ id: "cod_estacion", type_condition: 'AND', type: "ARRAY_STRING", values: values });
                }
                aux = 't1.cod_variable';
                this.dependencyFilters[aux] = filtersVariable;
                this.filter.elementMap[aux].clean();
                this.filter.elementMap[aux].refresh();
            }
            if (data.element == 't2.cod_estacion') {
                var values = [];
                value = data.info.msModel.selectedElements;

                for (var item of value) {
                    values.push(item.COD);
                }
                if (values.length > 0) {
                    filtersVariable.push({ id: "t2.cod_estacion", type_condition: 'AND', type: "ARRAY_STRING", values: values });
                }
                aux = 't1.cod_variable';
                this.dependencyFilters[aux] = filtersVariable;
                this.filter.elementMap[aux].clean();
                this.filter.elementMap[aux].refresh();
            }

        }
    }
    addFilterEstacion() {
        return new Promise((resolve, reject) => {
            var self = this;
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
                group: 2, id: 't1.cod_estacion',
                label: 'Estación', searchOption: false, width: 300, disabled: false, fun: function (params) {
                    return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self, self.dependencyFilters["t1.cod_estacion"]);
                }
            });
            resolve(1);

        });
    }


    addFilterVariables() {
        var self = this;
        return new Promise((resolve, reject) => {
            var aux = this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 't1.cod_variable' : 't4.cod_variable';
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
                group: 2,
                id: aux,
                label: 'Variables', searchOption: false, width: 300, disabled: false, fun: function (params) {
                    return self._datoBrutoService.getDataSetVariablesPaginacion(params, self, self.dependencyFilters[aux]);
                }
            });
            resolve(1);
        });
    }

    addFilterVariables27() {
        var self = this;

        return new Promise((resolve, reject) => {
            var aux = 't1.cod_variable';
            var values = [];
            var entity = constants.SGI.ENTITIES.VARIABLES_27;
            var field = 'tipo';
            if (this.tipoRed == constants.SGI.TIPO_RED_VALIDACION.PIEZO) {
                values = ['PZ', 'PZM']
                entity = constants.SGI.ENTITIES.VARIABLES_PIEZO_27;
                field = 'tipo';
            } else {
                values = [this.tipoRed];
            }
            var filters = [{ id: field, type_condition: 'AND', type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, values: values }];
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
                group: 2,
                id: aux,
                label: 'Variables', searchOption: false, width: 300, disabled: false, fun: function (params) {
                    var filters2;
                    if (self.dependencyFilters[aux] && self.dependencyFilters[aux].length > 0) {
                        filters2 = filters.concat(self.dependencyFilters[aux]);
                    }
                    else {
                        filters2 = filters;
                    }


                    return self._datoBrutoService.getDataSetVariables27Paginacion(params, self, entity, filters2);
                }
            });
            resolve(1);
        });
    }

    addFilterOrigenDatos() {
        var self = this;
        return new Promise((resolve, reject) => {
            self._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ORIGENES, sort: 'desc_fuente_origen' }).subscribe(list => {
                self.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 2,
                    width: 200,
                    id: 't1.cod_fuente_origen',
                    label: 'Origen de datos',
                    data: list,
                    code: 'cod_fuente_origen',
                    desc: 'desc_fuente_origen',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }

    addFilterZonas() {
        var self = this;
        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ZONAS, sort: 'desc_zona' }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 'cod_zona' : 't4.cod_zona',
                    label: 'Zona',
                    data: list,
                    code: 'cod_zona',
                    desc: 'desc_zona',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }

    addFilterEstaciones27() {
        var self = this;
        return new Promise((resolve, reject) => {
            var filters = [{ id: 'tipo', type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, values: [this.tipoRed] }];
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ESTACIONES_27, sort: 'nombre', filters: filters }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: 'cod_estacion',
                    label: 'Estación',
                    data: list,
                    code: 'cod_estacion',
                    desc: 'nombre',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }

    addFilterEstacionesPiezo27() {
        var self = this;
        return new Promise((resolve, reject) => {
            var filters = []; // [{ id: 'tipo', type: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, values: [this.tipoRed]}];         
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ESTACIONES_PIEZO_27, sort: 'nombre', filters: filters }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: 't2.cod_estacion',
                    label: 'Estación',
                    data: list,
                    code: 'cod_estacion',
                    desc: 'nombre',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }

    addFilterAreas() {
        var self = this;
        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.AREAS }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 'cod_area' : 't4.cod_area',
                    label: 'Área',
                    data: list,
                    code: 'cod_area',
                    desc: 'desc_area',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }
    addFilterSubcuencas() {
        var self = this;
        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.SUBCUENCAS, sort: 'desc_subcuenca' }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: 't4.cod_subcuenca',
                    label: 'Sub cuenca',
                    data: list,
                    code: 'cod_subcuenca',
                    desc: 'desc_subcuenca',
                    searchOption: true
                });
                resolve(1);
            });
        });
    }

    addFilterTipoEstacion() {
        var self = this;
        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPO_ESTACIONES, sort: 'desc_tipo_estacion' }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: this.tipoDato === constants.SGI.PERIODICIDAD_DATO.HORARIO ? 'cod_tipo_estacion' : 't4.cod_tipo_estacion',
                    label: 'Tipo Estación',
                    data: list,
                    code: 'cod_tipo_estacion',
                    desc: 'desc_tipo_estacion',
                    searchOption: true
                });
                resolve(1);
            });
        });

    }

    addFilterTipoVariable() {
        return new Promise((resolve, reject) => {
            this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, sort: 'desc_tipo_variable' }).subscribe(list => {
                this.filter.addFilter({
                    dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                    group: 1,
                    width: 400,
                    id: 't1.cod_tipo_variable',
                    label: 'Tipo Variable',
                    data: list,
                    code: 'cod_tipo_variable',
                    desc: 'desc_tipo_variable',
                    searchOption: true
                });
                resolve(1);
            });
        });

    }

    onExecuteTableRowUtilityActionVariable(event) {
        this.varSelected = event.row;
        this.varSelected.date = null;
        this.varSelected.hour = null;
        this.varSelected.value = null;
        this.varSelected.cod_motivo_invalidacion = null;


        //console.log(this.varSelected);
        if (event.action == "add_data_horario") {
            this.modalService.open(this.modalAddDatoDiario, { size: 'lg', centered: true, keyboard: false, backdrop: false });
        }
        else if (event.action == "add_data_diario") {
            //alert('add');
            //this.modalService.open(this.modalDuplicar, { size: 'sm', centered: true });
        }
    }
    onCreateDatoHorario() {



        this.msgErrorOnCreate = null;
        if (this.validateOnCreate()) {
            var dt = moment(this.varSelected.date);
            this.varSelected.fullDate = dt.format("YYYY-MM-DD") + ' ' + this.varSelected.hour;
            this.varSelected.anyo = dt.year();
            this.varSelected.mes = dt.month() + 1;
            this.varSelected.dia = dt.date();
            this.varSelected.ind_validacion = this.varSelected.cod_motivo_invalidacion === '-1' ? 'S' : 'N';
            var aux = moment(this.varSelected.fullDate, 'YYYY-MM-DD HH:mm');

            if (moment().diff(aux, 'minutes') < 0) {
                alert('No está permitido introducir valores en fechas futuras');

            } else {
                this._datoBrutoService.createData(this.varSelected).subscribe(list => {
                    alert('ATENCIÓN: Es posible que necesite forzar algún recálculo para la correcta propagación de los datos.')
                    //this.changeSerieChart()
                    this.modalService.dismissAll();
                });

            }

        }
    }
    validateOnCreate() {
        if (!this.varSelected.date || !this.varSelected.hour || !this.varSelected.value || !this.varSelected.cod_motivo_invalidacion || this.varSelected.cod_motivo_invalidacion == 0) {
            this.msgErrorOnCreate = 'Complete todos los campos del formulario';
            return false;
        }
        return true;
    }

    openExportar() {
        this.fechaFinExport = null;
        this.fechaIniExport = null;
        this.horaFinExport = "00:00";
        this.horaIniExport = "00:00";
        this.frecuenciaExport = 10;
        this.separadorExport = 'P';
        this.variablesExport = [];
        this.codVariableExport = null;
        this.constantValueExport = null;
        this.msgErrorExport = null;
        this.modalService.open(this.modalExportData, { size: 'lg', centered: true });

    }

    openImportar() {
        this.msgErrorImport = null;
        this.msgFileOK = null;
        this.msgConfirmImport = null;
        this.importing = false;
        this.fileToUpload = null;
        this.showValidarImport = true;
        this.showConfirmarImport = false;
        this.estrategiaImport = 'V';
        this.separadorImport = 'P';
        this.validacionImport = 'S';
        this.modalRefImportar = this.modalService.open(this.modalImportData, { size: 'lg', centered: true });
    }

    openCompletarHuecos() {
        this.fechaFinExport = null;
        this.fechaIniExport = null;
        this.horaFinExport = "0";
        this.horaIniExport = "0";
        this.frecuenciaExport = 10;
        this.msgErrorHoles = null;
        this.msgOKHoles = null;
        this.codVariableHuecos = null;
        this.fillingHoles = false;
        this.modalService.open(this.modalCompletarHuecos, { size: 'lg', centered: true });
    }

    openForzarSumarizacion() {
        this.fechaFinExport = null;
        this.fechaIniExport = null;
        this.msgErrorHoles = null;
        this.msgOKHoles = null;
        this.codVariableHuecos = null;
        this.fillingHoles = false;
        this.modalService.open(this.modalForzarSumarizacion, { size: 'lg', centered: true });
    }

    rellenarHuecos() {
        this.msgErrorHoles = null;
        this.msgOKHoles = null;

        if (this.fechaIniExport == null || this.horaIniExport == null || this.fechaFinExport == null || this.horaFinExport == null || this.codVariableHuecos == null) {
            this.msgErrorHoles = "Complete todos los campos obligatorios";
            return;
        }


        var dtIni = moment(this.fechaIniExport).format('YYYY-MM-DD') + ' ' + this.horaIniExport;
        var dtFin = moment(this.fechaFinExport).format('YYYY-MM-DD') + ' ' + this.horaFinExport;
        //var ini = moment(dtIni, 'DD/MM/YYYY HH:mm');
        //var fin = moment(dtFin, 'DD/MM/YYYY HH:mm');

        var self = this;
        var data = {
            fechaIni: dtIni,
            fechaFin: dtFin,
            codVariable: this.codVariableHuecos,
            frecuencia: this.frecuenciaExport
        };

        this.fillingHoles = true;
        self._datoBrutoService.fillHoles(data).subscribe(data => {
            this.msgOKHoles = 'Proceso finalizado correctamente (' + data['filled'] + ' registros rellenados)';
            this.fillingHoles = false;
        });
    }

    generarPlantilla() {
        this.msgErrorExport = null;
        // Validamos que todos los campos estén completos
        if (this.fechaIniExport == null || this.horaIniExport == null || this.fechaFinExport == null || this.horaFinExport == null
            || this.variablesExport.length == 0) {
            this.msgErrorExport = "Complete todos los campos obligatorios para la generación de la plantilla";
            return;
        }



        //
        var content = "Timestamp";
        //console.log(this.fechaFinExport);
        var dtIni = moment(this.fechaIniExport).format('DD/MM/YYYY') + ' ' + this.horaIniExport;
        var dtFin = moment(this.fechaFinExport).format('DD/MM/YYYY') + ' ' + this.horaFinExport;
        var ini = moment(dtIni, 'DD/MM/YYYY HH:mm');
        var fin = moment(dtFin, 'DD/MM/YYYY HH:mm');
        var separator = ';';
        if (this.separadorExport == "T") {
            separator = "\t";
        }
        // Header
        for (var variable of this.variablesExport) {
            content += separator + variable.codigo;
        }
        content += '\n';
        while (ini.diff(fin, 'minutes') <= 0) {
            content += ini.format('DD/MM/YYYY HH:mm');
            for (var variable of this.variablesExport) {
                content += separator + variable.valor;
            }
            content += '\n';
            ini.add(this.frecuenciaExport, 'minutes');
        }

        var data = new Blob([content], { type: 'text/plain;charset=utf-8' });
        fs.saveAs(data, 'plantilla_minutal_saihg.csv')
    }
    anadirVariablePlantilla() {
        this.msgErrorExport = null;
        if (this.codVariableExport == null) {
            this.msgErrorExport = "Complete la variable";
            return;
        }

        this.variablesExport.push({
            codigo: this.codVariableExport,
            valor: this.constantValueExport
        }
        );
        this.codVariableExport = '';
        this.constantValueExport = '';
    }
    eliminarVariablePlantilla(index) {
        this.variablesExport.splice(index, 1);
    }
    validateLine(index, line) {

        var separator = ';';
        if (this.separadorImport == "T") {
            separator = '\t';
        }
        var tokens = line.split(separator);
        var errors = [];


        if (tokens.length != this.variablesFile.length + 1) {
            errors.push('Línea <b>' + index + '</b>: <i>' + line + '</i> tiene formato incorrecto o faltan tokens ');
        }
        else {
            if (!moment(tokens[0], 'DD/MM/YYYY HH:mm', true).isValid()) {
                errors.push('Línea <b>' + index + '</b>. Formato de fecha inválido ' + tokens[0] + ', debería ser DD/MM/YYYY HH:mm.');
            }
            else {
                var aux = moment(tokens[0], 'DD/MM/YYYY HH:mm', true);
                if (moment().diff(aux, 'minutes') < 0) {
                    errors.push('Línea <b>' + index + '</b>. Contiene una fecha futura no permitida ' + aux.format('DD/MM/YYYY HH:mm'));
                }
            }


        }
        //console.log(errors);
        return errors;
    }
    validateFile() {
        var errorsFile = [];
        for (var i = 1; i < this.linesFile.length; i++) {
            var errors = this.validateLine(i, this.linesFile[i]);
            if (errors.length > 0) {
                errorsFile = errorsFile.concat(errors);
            }
        }
        return errorsFile;
    }
    importarPlantilla(validar, ejecutar) {
        var self = this;
        this.msgErrorImport = null;
        this.msgFileOK = null;
        this.msgConfirmImport = null;
        this.importing = false;
        self.linesFile = [];
        self.variablesFile = [];

        if (this.fileToUpload) {
            var name = this.fileToUpload.name;
            var ext = name.substring(name.lastIndexOf(".") + 1);
            if (ext.toUpperCase() != "CSV") {
                self.msgErrorImport = "La extensión del fichero es incorrecta. Sólo ficheros con extensión .csv son permitidos.";
            }
            else {
                self.importing = true;
                var separator = ';';
                if (this.separadorImport == "T") {
                    separator = '\t';
                }
                let fileReader = new FileReader();
                var dataVariables = [];
                fileReader.onload = (function (f) {
                    return function (e) {
                        var content = fileReader.result;
                        var lines = content.toString().split('\n');
                        for (var line = 0; line < lines.length; line++) {
                            // Excluimos lineas vacias
                            if (lines[line].trim() != "") {
                                self.linesFile.push(lines[line]);
                                var tokens = lines[line].split(separator);
                                // Obtengo las variables de la primera línea
                                if (line == 0) {
                                    for (var token = 0; token < tokens.length; token++) {
                                        if (token > 0) {
                                            self.variablesFile.push(tokens[token]);
                                        }
                                    }
                                }
                                else {
                                    dataVariables.push(tokens);
                                }
                            }
                        }
                        var errors = self.validateFile();

                        if (errors.length == 0) {

                            var data = {
                                conf: {
                                    estrategia: self.estrategiaImport,
                                    includeValidacion: validar,
                                    includeExecution: ejecutar
                                },
                                vars: self.variablesFile,
                                datos: dataVariables,
                                cod_usuario: self.profile.cod_usuario                                
        
                            };

                            self._datoBrutoService.importData(data).subscribe(data => {
                                if (data.errors.length > 0) {
                                    self.importing = false;
                                    self.msgErrorImport = "";
                                    for (var error of data.errors) {
                                        self.msgErrorImport += error;
                                    }
                                }
                                else {
                                    if (validar) {
                                        self.importing = false;
                                        if ((data['inserts'] + data['updates']) > 0) {
                                            self.msgConfirmImport = "Se insertarán <b>" + data['inserts'] + " registros</b> y se actualizaran <b>" + data['updates'] + "</b> registros de <b>" + self.variablesFile.length + " variables: </b> (<i>" + self.variablesFile + "</i>)  <br> <b>Por favor, confirme para continuar.</b>   ";
                                            self.showConfirmarImport = true;
                                            self.showValidarImport = false;
                                        }
                                        else {
                                            if (self.estrategiaImport == 'SC') {
                                                self.msgErrorImport = "El fichero no contiene registros que insertar o crear";
                                            } else {
                                                self.msgErrorImport = "El fichero no contiene registros que crear";
                                            }
                                            self.showValidarImport = true;
                                        }
                                    }
                                    else {
                                        self.importing = false;
                                        self.showConfirmarImport = false;
                                        self.msgFileOK = "Datos importados satisfactoriamente!";
                                    }
                                }
                            },
                                error => {
                                    self.importing = false;
                                    self.msgErrorImport = "Error inesperado. ";
                                    self.showValidarImport = false;
                                    self.showConfirmarImport = false;

                                });
                        }
                        else {
                            self.msgErrorImport = "";
                            for (var j = 0; j < errors.length; j++) {
                                //console.log(errors[j]);
                                if (errors[j]) {
                                    self.msgErrorImport += errors[j] + "<br>";
                                }
                            }
                            self.importing = false;
                        }
                    };
                })(this.fileToUpload);

                fileReader.readAsText(this.fileToUpload)
            }
        }
        else {
            this.msgErrorImport = "No ha seleccionado ningún fichero";
        }
    }
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }
    onChangeActivarValidacionImport(evt) {
        if (this.validacionImport == 'S') {
            this.showValidarImport = true;
            this.showConfirmarImport = false;
        }
        else {
            this.showValidarImport = false;
            this.showConfirmarImport = true;
        }
    }
    forzarSumarizacion() {
        this.msgErrorHoles = null;
        this.msgOKHoles = null;

        if (this.fechaIniExport == null || this.fechaFinExport == null || this.codVariableHuecos == null) {
            this.msgErrorHoles = "Complete todos los campos obligatorios";
            return;
        }

        var dtIni = moment(this.fechaIniExport).format('YYYY-MM-DD');
        var dtFin = moment(this.fechaFinExport).format('YYYY-MM-DD');
        var self = this;
        var data = {
            fechaIni: dtIni,
            fechaFin: dtFin,
            codVariable: this.codVariableHuecos
        };

        this.fillingHoles = true;
        self._datoBrutoService.forceSumarizacion(data).subscribe(data => {
            this.msgOKHoles = 'Variables marcadas para sumarizar en la siguiente ejecución';
            this.fillingHoles = false;
        });
    }
    openDetectGaps() {
        var self = this;
        this.modeFillGaps = 2;
        this.fillingGaps = false;
        this.msgErrorGaps = null;
        this.msgOKGaps = null;
        this.metodoInterpolacion = 3;
        this.frecuenciaGAP = 10;
        this.tipoDatosGaps = 1;

        this.modalService.open(this.modalFillGaps, { size: 'lg', centered: true, keyboard: false, backdrop: false });
        setTimeout(function () {
            self.calculateDataGaps();
        }, 1000);

    }
    openFillGaps() {
        var self = this;
        this.modeFillGaps = 1;
        this.fillingGaps = false;
        this.msgErrorGaps = null;
        this.msgOKGaps = null;
        this.metodoInterpolacion = 3;
        this.frecuenciaGAP = 10;
        this.tipoDatosGaps = 1;
        this.calculatingDataGaps = true;
        this.modalService.open(this.modalFillGaps, { size: 'lg', centered: true });
        //setTimeout(function () {
        this.calculateDataGaps();
        //}, 1000);
    }
    calculateDataGaps() {
        var self = this;
        this.calculatingDataGaps = true;
        setTimeout(function () {
            if (self.modeFillGaps == 1) {
                self.calculateDataGapsSelected();
                self.calculatingDataGaps = false;
            }
            else {
                self.calculateDataGapsFull();
                self.calculatingDataGaps = false;
            }

        }, 1000);

    }


    calculateDataGapsFull() {

        var dateMask = "DD/MM/YYYY HH:mm";
        var areaData = [];
        this.areasToFill = [];
        var totalHuecos = 0;
        var totalInvalidos = 0;
        var totalAprocesar = 0;
        var contiene5minutales = false;
        if (this.measures.length > 0) {
            var first = this.measures[0];
            var last = this.measures[this.measures.length - 1];

            var ini = moment(first.date, 'YYYY-MM-DD HH:mm:ss');
            var fin = moment(last.date, 'YYYY-MM-DD HH:mm:ss');

            // Primera pasa para ver si tiene datos cincominutales
            for (var i in this.measures) {
                var minute = moment(this.measures[i].date).format("mm")
                contiene5minutales = contiene5minutales || this.esMinimoMultiplo5(parseInt(minute));
            }

            while (ini.diff(fin, 'minutes') <= 0) {
                var aux = this.measures.find(x => x.date === ini.format('YYYY-MM-DD HH:mm:ss'));
                if (!aux) {
                    areaData.push({
                        date: ini.format(dateMask),
                        ts: ini.unix(),
                        value: 'NaN'
                    });
                    totalHuecos++;

                }
                else {

                    areaData.push({
                        date: ini.format(dateMask),
                        ts: ini.unix(),
                        value: (aux.valido != 'S' && (this.tipoDatosGaps == 2 || this.tipoDatosGaps == 3)) ? 'NaN' : aux.value,
                    });
                    if (aux.valido != 'S') {
                        totalInvalidos++;
                    }
                }
                if (contiene5minutales) {
                    ini.add(5, 'minutes');
                }
                else {
                    ini.add(this.frecuenciaGAP, 'minutes');
                }
            }
            this.areasToFill.push({
                cod_variable: this.dataSetsChart[0].label,
                data: areaData,
                start: first.date,
                end: last.date,
                periodicity: this.frecuenciaGAP,
                totalHuecos: totalHuecos,
                totalInvalidos: totalInvalidos,
                contiene5minutales: contiene5minutales

            });
        }

    }
    calculateDataGapsSelected() {
        this.areasToFill = [];
        var dateMask = "DD/MM/YYYY HH:mm";
        var areaData = [];
        this.areasToFill = [];
        var firstDateArea = null;
        var endDateArea = null;

        for (var j = 0; j < this.selectedAreas.length; j++) {
            var area = this.selectedAreas[j];
            areaData = [];
            firstDateArea = null;
            endDateArea = null;
            var totalInvalidos = 0;
            var totalHuecos = 0;
            var contiene5minutales = false;
            // Pongo los datos del área
            for (var i = area[4]; i <= area[5]; i++) {
                var tmp = this.dataSetsChart[0].data[i];
                if (tmp) {
                    // Check height of reactangle                  
                    var yMax = Math.max(area[6], area[7]);
                    var yMin = Math.min(area[6], area[7]);
                    if (tmp.y >= yMin && tmp.y <= yMax) {
                        var fecha = moment(tmp.x).format(dateMask);
                        var minute = parseInt(moment(tmp.x).format('mm'));
                        contiene5minutales = contiene5minutales || this.esMinimoMultiplo5(minute);

                        var ts = moment(tmp.x).unix();
                        if (!firstDateArea) {
                            firstDateArea = fecha;
                        }
                        areaData.push({
                            date: fecha,
                            ts: ts,
                            valido: tmp.valido,
                            value: (tmp.valido != 'S' && (this.tipoDatosGaps == 2 || this.tipoDatosGaps == 3)) ? 'NaN' : tmp.y,
                        });
                        if (tmp.valido != 'S') {
                            totalInvalidos++;
                        }
                    }
                }
                if (areaData.length > 0) {
                    endDateArea = areaData[areaData.length - 1].date;
                }
            }


            // Terminada de procesar el area seleccionada
            // Recorrido de valores teóricos
            // Ajuste de la hora de inicio con la frecuencia, para que el sampling vaya bine
            //var minuteIntial = moment(firstDateArea, dateMask).minute();
            /*if ((this.frecuenciaGAP == 10 && (minuteIntial != 0 && minuteIntial != 10 &&
                minuteIntial != 20 && minuteIntial != 30 && minuteIntial != 40 && minuteIntial != 50))
                ||
                (this.frecuenciaGAP == 15 && (minuteIntial != 0 && minuteIntial != 15 &&
                    minuteIntial != 30 && minuteIntial != 45))) {
                var dateMaskRound = "DD/MM/YYYY HH:00";
                var t = moment(firstDateArea, dateMask).format(dateMaskRound);
                var t2 = moment(t, dateMaskRound);
                console.log("Ajusto " + firstDateArea + " a " + t);
                firstDateArea = t;
            }*/


            var ini = moment(firstDateArea, dateMask);
            var fin = moment(endDateArea, dateMask);

            if (!contiene5minutales) {
                while (ini.diff(fin, 'minutes') <= 0) {
                    if (!areaData.find(x => x.date === ini.format(dateMask))) {
                        areaData.push({
                            date: ini.format(dateMask),
                            ts: ini.unix(),
                            value: 'NaN'
                        });
                        totalHuecos++;
                    }
                    ini.add(this.frecuenciaGAP, 'minutes');
                }
            }
            else {
                // El sampleo será cada 5 minutos
                while (ini.diff(fin, 'minutes') <= 0) {
                    if (!areaData.find(x => x.date === ini.format(dateMask))) {
                        areaData.push({
                            date: ini.format(dateMask),
                            ts: ini.unix(),
                            value: 'NaN'
                        });
                        totalHuecos++;
                    }

                    var minute = parseInt(moment(tmp.x).format('mm'));
                    ini.add(5, 'minutes');
                }
            }
            areaData.sort((a, b) => (a.ts > b.ts) ? 1 : ((b.ts > a.ts) ? -1 : 0))


            this.areasToFill.push({
                cod_variable: this.dataSetsChart[0].label,
                data: areaData,
                // method: this.metodoInterpolacion,
                start: firstDateArea,
                end: endDateArea,
                totalHuecos: totalHuecos,
                totalInvalidos: totalInvalidos,
                periodicity: this.frecuenciaGAP,
                contiene5minutales: contiene5minutales
            });

        }
    }

    confirmFillGaps() {
        this.fillGaps();
    }
    fillGaps() {
        //console.log(this.measures);
        var self = this;
        self.fillingGaps = true;
        //this.tablaMedidas.setDataAndLoadTable([]);
        for (var i = 0; i < this.areasToFill.length; i++) {
            this.areasToFill[i].method = this.metodoInterpolacion;
            if (this.areasToFill[i].contiene5minutales) {
                this.areasToFill[i].periodicity = 5;
            }
        }

        self.fillGapsArea(this.areasToFill).then(areasFilled => {
            for (var i = 0; i < areasFilled.length; i++) {
                var area = areasFilled[i];

                for (var j = 0; j < area.data.length; j++) {
                    if (parseFloat(area.data[j].value) != parseFloat(area.data[j].value_filled)) {

                        var date = moment(area.data[j].date, "DD/MM/YYYY HH:mm").format('YYYY-MM-DD HH:mm:ss');
                        var minute = parseInt(moment(area.data[j].date, "DD/MM/YYYY HH:mm").format('mm'));

                        if (!area.contiene5minutales ||
                            (area.contiene5minutales && !self.esMinimoMultiplo5(minute))) {

                            var index = this.measures.findIndex(x => x.date === date && x.cod_variable == area.cod_variable);

                            if (area.data[j].value_filled) {
                                if (index < 0) {
                                    this.measures.push({
                                        value: area.data[j].value_filled,
                                        date: moment(area.data[j].date, "DD/MM/YYYY HH:mm").format('YYYY-MM-DD HH:mm:ss'),
                                        valido: 'S',
                                        origen: 'M1',
                                        cod_variable: area.cod_variable,
                                        motivoInvalidacion: { cod: null, desc: null },
                                        dirty: true,
                                        recalculadoValor: true
                                    });
                                    //console.log("Rellenado valor para " + area.data[j].date + " -> " + area.data[j].value_filled);
                                }
                                else {
                                    this.measures[index].value = area.data[j].value_filled;
                                    this.measures[index].origen = 'M1';
                                    this.measures[index].valido = 'S';
                                    this.measures[index].dirty = true;
                                    this.measures[index].recalculadoValor = true;
                                    this.measures[index].motivoInvalidacion = { cod: null, desc: null };

                                    //console.log("Modificado valor para " + area.data[j].date + " -> " + area.data[j].value_filled);
                                }
                            }
                        }
                    }
                }
            }
            this.measures.sort((a, b) => (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0))
            this.generateDatasetsForChart();
            this.chart.data.datasets[0] = this.dataSetsChart[0];
            this.chart.update();
            this.fillingGaps = false;
            //console.log(this.measures);
            //this.tablaMedidas.setDataAndLoadTable(this.measures);
            this.msgOKGaps = "Proceso finalizado. No olvide guardar los cambios propuestos";

        });



    }
    esMinimoMultiplo5(minute): boolean {
        return (minute == 5 || minute == 15 || minute == 25 || minute == 35 || minute == 45 || minute == 55);
    }

    fillGapsArea(areas): any {
        var self = this;
        return new Promise((resolve, reject) => {
            self._datoBrutoService.fillGaps(areas).subscribe(result => {
                resolve(result);
            });
        });
    }
}
