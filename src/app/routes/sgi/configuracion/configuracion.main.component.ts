import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-configuracion-main',
    templateUrl: './configuracion.main.component.html',
    styleUrls: ['./configuracion.main.component.scss'],
    providers: []
})
export class ConfiguracionMainComponent implements OnInit {
    profile: any;
    selectedOption: string;
    rootSection = {
        title: "Configuracion",
        class: "fa fa-calculator "
    }
    sections = [/*{
        title: "Estaciones",
        route: "conf_est",
        public: true,
        selected: this._route.snapshot.url[0].path == "conf_est",
    }
    {
        title: "Compresion",
        route: "conf_comp",
        public: true,
        selected: this._route.snapshot.url[0].path == "conf_comp",
    },*/ {
        title: "Validacion",
        route: "conf_val",
        public: true, 
        selected: this._route.snapshot.url[0].path == "conf_val",
    },
    {
        title: "Sumarizacion",
        route: "conf_sum",
        public: true,
        selected: this._route.snapshot.url[0].path == "conf_sum",
    }/*,
    {
        title: "Permanencia de los datos",
        route: "conf_per",
        public: true,
        selected: this._route.snapshot.url[0].path == "conf_per",
    }*/
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.CONFIGURACION);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
