import { Component, OnInit } from '@angular/core';
import { constants } from '../../../../config/constants';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
@Component({
    selector: 'app-compresion-manager',
    templateUrl: './compresion.component.html',
    styleUrls: ['./compresion.component.scss'],
    providers: []
})
export class CompresionComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    ngOnInit() {
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.CONFIGURACION);
        this.profile = this._auditoriaService.getUserProfile();
    }
}
