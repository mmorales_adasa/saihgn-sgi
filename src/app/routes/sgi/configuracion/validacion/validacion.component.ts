import { Component, OnInit, ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel, BasicTableUtility  } from '@arca/common';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-validacion-manager',
    templateUrl: './validacion.component.html',
    styleUrls: ['./validacion.component.scss'],
    providers: []
})
export class ValidacionComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "order_variable ASC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listTiposVariable = [];
    rowSelected;
    selectedVariable;
    @ViewChild('modalEdicion') modalEdicion: any;
    @ViewChild('modalSelectVariable') modalSelectVariable: any;
    @ViewChild('tableValidacion') tableValidacion: any;
    addRowUtility = new BasicTableUtility({
        id: "ADD_ROW",
        tooltip: "Añadir regla de validación",
        faIcon: "fa-plus",
        innerHtml: ""
    });    
    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
            this.searchData(true);;
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var obs1 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, filters: [], sort: "desc_tipo_variable ASC" });
            return forkJoin([obs1]).subscribe(([res1 ]) => {
                this.listTiposVariable = res1;

                resolve();
            });
        });
    }

    searchData(count) {
        var params = { entity: constants.SGI.ENTITIES.GEST_VALIDACION, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        // Si descomento esto no me funciona la paginacion
        
        this._basicEntityService.getDataPagination(params).subscribe(list => {
                var params = { entity: constants.SGI.ENTITIES.GEST_VALIDACION, filters: this.filterSelection }
                if (count)
                {
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.totalElements = total;
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
                else{
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_est',
            title: null,
            //utilities: [this.addRowUtility],
            columns: [                          
                { id: "cod_regla_val", filter: true, primaryKey: true, displayed: true, translation: "Id", typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_tipo_variable", filter: true, primaryKey: false, displayed: true, translation: "Tipos de variable"},
                { id: "cod_tipo_variable", filter: true, displayed: true, translation: "Cod.Tipo Variable"},
                { id: "cod_variable", filter: true, displayed: true, translation: "Cod.Variable"},
                { id: "desc_aplicacion_validacion_aut", filter: true, displayed: true, translation: "Aplica" },
                { id: "desc_tipo_validacion_aut", filter: true, displayed: true, translation: "Tipo de validación" }
            ],    
            crud: {
                title: "Alta/Edición de Gestión de la validacion",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",                
                serviceParams: { entity: constants.SGI.ENTITIES.GEST_VALIDACION },
                fields: [
                    { row: 1, id: "cod_regla_val",  label: "Código", pk: true, sequence : true, mandatory: false, bsWidth: 2, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "cod_tipo_variable", label: "Tipo de variable", pk: false, mandatory: true, bsWidth: 5, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listTiposVariable, code: 'cod_tipo_variable', desc: 'desc_tipo_variable', field_in_table: 'desc_tipo_variable' },
                    //{ row: 1, id: "ind_todas_variables", label: "¿Aplica todas las variables?", pk: false, mandatory: true, bsWidth: 5, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR' },
                    { row: 1, id: "cod_variable", label: "Variables  (**** = todas)", pk: false, mandatory: false, bsWidth: 5, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT, code: 'cod_variable', desc: 'desc_variable', field_in_table: 'desc_variable' ,defaultValue : '****' },
                    { row: 2, id: "cod_tipo_validacion_aut", label: "Tipo de validación", pk: false, mandatory: true, bsWidth: 4, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getTipoValidacionAuto(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_tipo_validacion_aut', defaultValue : 'RANG', showEmpty : true },
                    { row: 2, id: "ind_aplicacion_validacion_aut", label: "Activo", pk: false, mandatory: true, bsWidth: 4, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 'N', DESCR: 'No' }, { COD: 'S', DESCR: 'Sí' }], code: 'COD', desc: 'DESCR', field_in_table: 'desc_aplicacion_validacion_aut', defaultValue : 'RANG', showEmpty : true },
                    { row: 3, id: "valor_min_validacion_aut", label: "[Por rango] Valor mínimo", pk: false, mandatory: false, bsWidth: 4, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength:200, defaultValue: null, disabled: false },
                    { row: 3, id: "valor_max_validacion_aut", label: "[Por rango] Valor máximo", pk: false, mandatory: false, bsWidth: 4, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 200, defaultValue: null, disabled: false },                    
                    { row: 3, id: "valor_pend_validacion_aut", label: "[Por pendiente] Valor pendiente", pk: false, mandatory: false, bsWidth: 4, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 200, defaultValue: null, disabled: false }                    
                ]
            },                  
            selectable: false,            
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    beforeExecuteCrudAction(event) {    
    }

    afterExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData(false);
        }
        if (event.action === "openAssistant") {
            this.openVariableSelector();
        }
    }    
    openVariableSelector() {
        this.modalService.open(this.modalSelectVariable, { size: 'lg', centered: true });
    }
    configureFilters() {
        var self;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.considerEmptyError = false;
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
            group: 1,
            width: 400,
            id: 'cod_tipo_variable',
            label: 'Tipo Variable',
            data: this.listTiposVariable,
            code: 'cod_tipo_variable',
            desc: 'desc_tipo_variable',
            searchOption: true
        });
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            group: 1,
            width: 200,
            id: 'cod_variable',
            label: 'Cod.Variable',                        
        });        
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        if (this.tableValidacion.paginator){
            this.tableValidacion.paginator.pageIndex = 0;
        }
        this.searchData(true);
    }
    onSort(event) {
        this.sort = event.active + " " + event.direction;        
        this.searchData(false);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(false);
    }
    onSelectVariable(event) {
            this.selectedVariable = event.cod_variable;
    }
    selectVariableTodas(modal) {
        this.tablaConstantes.setValueCrud("cod_variable", "****");
        modal.dismiss('Cross click')
    }            
    selectVariable(modal) {
        this.tablaConstantes.setValueCrud("cod_variable", this.selectedVariable);
        modal.dismiss('Cross click')
    }       
} 