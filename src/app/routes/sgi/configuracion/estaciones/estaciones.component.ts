import { Component, OnInit } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel } from '@arca/common';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-estaciones-manager',
    templateUrl: './estaciones.component.html',
    styleUrls: ['./estaciones.component.scss'],
    providers: []
})
export class EstacionesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "desc_estacion ASC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listEstadoEstacion = [];
    listTiposEstacion = [];
    listComunidades = [];
    listProvincias = [];
    listMunicipios = [];
    listComarcas = [];
    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
            this.searchData(true);
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var obs1 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPO_ESTACIONES, filters: [], sort: "desc_tipo_estacion ASC" });
            var obs2 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ESTADO_ESTACIONES, filters: [], sort: "desc_estado_estacion ASC" });
            var obs3 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.COMUNIDAD, filters: [], sort: "desc_comunidad ASC" });
            var obs4 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.PROVINCIA, filters: [], sort: "desc_provincia ASC" });
            var obs5 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MUNICIPIO, filters: [], sort: "desc_termino_municipal ASC" });
            var obs6 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.COMARCA, filters: [], sort: "desc_comarca ASC" });
            return forkJoin([obs1, obs2, obs3, obs4, obs5, obs6]).subscribe(([res1, res2, res3, res4, res5, res6]) => {
                this.listTiposEstacion = res1;
                this.listEstadoEstacion = res2;
                this.listComunidades = res3;
                this.listProvincias = res4;
                this.listMunicipios = res5;
                this.listComarcas = res6;
                resolve();
            });
        });
    }
    searchData(count) {
        var params = { entity: constants.SGI.ENTITIES.ESTACIONES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        // Si descomento esto no me funciona la paginacion
        
        this._basicEntityService.getDataPagination(params).subscribe(list => {
                var params = { entity: constants.SGI.ENTITIES.ESTACIONES, filters: this.filterSelection }
                if (count)
                {
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.totalElements = total;
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
                else{
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_est',
            title: null,
            columns: [
                { id: "cod_estacion", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Descripción" },
                { id: "desc_tipo_estacion", filter: true, displayed: true, translation: "Tipo" },
                { id: "desc_estado_estacion", filter: true, displayed: true, translation: "Estado" }
            ],
            crud: {
                title: "Alta/Edición de estaciones",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.ESTACIONES },
                fields: [
                    { row: 1, id: "cod_estacion", label: "Código", pk: true, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 16, defaultValue: null },
                    { row: 1, id: "desc_estacion", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
                    { row: 2, id: "desc_estacion_ext", label: "Descripcion Ext", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 4000, defaultValue: null },
                    { row: 2, id: "desc_observaciones", label: "Observaciones", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_AREA, maxLength: 4000, defaultValue: null },
                    { row: 3, id: "desc_notas_mantenimiento", label: "Notas Mto", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_AREA, maxLength: 4000, defaultValue: null },
                    { row: 3, id: "cod_estado_estacion", label: "Estado", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listEstadoEstacion, code: 'cod_estado_estacion', desc: 'desc_estado_estacion', field_in_table: 'desc_estado_estacion' },
                    { row: 4, id: "cod_tipo_estacion", label: "Tipo", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listTiposEstacion, code: 'cod_tipo_estacion', desc: 'desc_tipo_estacion', field_in_table: 'desc_tipo_estacion' },
                    { row: 4, id: "cod_comunidad", label: "Comunidad", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listComunidades, code: 'cod_comunidad', desc: 'desc_comunidad', field_in_table: 'desc_comunidad' },
                    { row: 4, id: "cod_provincia", label: "Provincia", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listProvincias, code: 'cod_provincia', desc: 'desc_provincia', field_in_table: 'desc_provincia' },
                    { row: 4, id: "cod_termino_municipal", label: "Municipio", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listMunicipios, code: 'cod_termino_municipal', desc: 'desc_termino_municipal', field_in_table: 'desc_termino_municipal' },
                    { row: 4, id: "cod_comarca", label: "Comarca", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listComarcas, code: 'cod_comarca', desc: 'desc_comarca', field_in_table: 'desc_comarca' },
                ]
            },
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    configureFilters() {
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.considerEmptyError = false;
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            group: 1,
            width: 200,
            id: 'cod_estacion',
            label: 'Código',
            defaultValue: null,
        });
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            group: 1,
            width: 200,
            id: 'desc_estacion',
            label: 'Descripción',
            defaultValue: null,
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
            group: 1,
            width: 400,
            id: 't1.cod_tipo_estacion',
            label: 'Tipo',
            data: this.listTiposEstacion,
            code: 'cod_tipo_estacion',
            desc: 'desc_tipo_estacion',
            searchOption: true
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
            group: 1,
            width: 400,
            id: 't1.cod_estado_estacion',
            label: 'Estado',
            data: this.listEstadoEstacion,
            code: 'cod_estado_estacion',
            desc: 'desc_estado_estacion',
            searchOption: true
        });        

        /* this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPO_CONSTANTES }).subscribe(list => {
             this.filter.addFilter({
                 dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                 group: 1,
                 width: 400,
                 id: 'cod_tipo_constante',
                 label: 'Tipo de constante',
                 data: list,
                 code: 'cod_tipo_constante',
                 desc: 'desc_tipo_constante',
                 searchOption: true
             });
         });*/
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(true);
    }
    onSort(event) {
        this.sort = event.active + " " + event.direction;        
        this.searchData(false);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(false);
    }
    afterExecuteCrudAction(event) {
        console.log(event);
    }
} 