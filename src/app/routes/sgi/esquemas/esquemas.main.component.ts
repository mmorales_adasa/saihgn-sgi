import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-esquemas-component-main',
    templateUrl: './esquemas.main.component.html',
    styleUrls: ['./esquemas.main.component.scss'],
    providers: []
})
export class EsquemasMainComponent implements OnInit {
    profile: any;
    selectedOption: string = 'mesq';
    rootSection = {
        title: "Esquemas",
        class: "fa fa-sitemap "
    }
    sections = [{
        title: "Gestión de esquemas",
        route: "esquemas",
        public: true,
        selected: this._route.snapshot.url[0].path == "esquemas",
    }
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.ESQUEMAS);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {  
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
