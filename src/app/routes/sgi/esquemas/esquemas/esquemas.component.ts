import { Component, OnInit, ViewChild } from '@angular/core';
import { constants } from '../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
import { EsquenaService } from '../../../../services/common/esquema.service';
import { BasicTableUtility } from '@arca/common';

@Component({
    selector: 'app-esquemas-manager',
    templateUrl: './esquemas.component.html',
    styleUrls: ['./esquemas.component.scss'],
    providers: [EsquenaService]
})
export class EsquemasComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    esquemaService;
    msgErrorImport;
    tablaTiposConstantes;
    selectedRow;
    fileToUpload: File = null;
    @ViewChild('confirmarBorrar') modalConfirmarBorrar: any;
    @ViewChild('nuevoEsquema') modalUpload: any;
    importUtility = new BasicTableUtility({
        id: "UPLOAD",
        tooltip: "Alta de nuevo esquema en fichero",
        faIcon: " fa-upload",
        innerHtml: null
    });
    ngOnInit() {
        this.esquemaService = this._injector.get(EsquenaService);
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.ESQUEMAS);
        this.profile = this._auditoriaService.getUserProfile();
        this.configureTable();
        this.searchData();
    }
    configureTable() {
        var tableConfiguration = {
            id: 'mesq',
            title: null,
            columns: [
                { id: "Borrar", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Borrar fichero de esquema", icon: "fa fa-trash-o" } },
                { id: "Descargar", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Descargar fichero de esquema", icon: "fa fa-download" } },
                { id: "filename", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },

            ],
            crud: {
                title: "Edición de esquemas",
                editable: false,
                deletable: false,
                insertable: false,
            },
            selectable: false,
            serverPagination: false,
            utilities: [this.importUtility],
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "filename",
                direction: "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        this.esquemaService.getData({}).subscribe((esquemas) => {
            this.tablaTiposConstantes.setDataAndLoadTable(esquemas);
        });
    }
    afterExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData();
        }
    }
    beforeExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData();
        }
    }
    confirmBorrar() {
        this.esquemaService.removeFile({ filename: this.selectedRow.filename }).subscribe((values) => {
            this.searchData();
            this.modalService.dismissAll();
        });
    }
    importHTML() {
        if (this.fileToUpload) {
            var name = this.fileToUpload.name;
            var ext = name.substring(name.lastIndexOf(".") + 1);
            if (ext.toUpperCase() != "HTM" && ext.toUpperCase() != "HTML") {
                this.msgErrorImport = "La extensión del fichero es incorrecta. Sólo ficheros HTML o HTM son permitidos.";
            }
            else {
                let fileReader = new FileReader();
                fileReader.onload = (e) => {
                    this.esquemaService.uploadFile({ filename: name, content: fileReader.result }).subscribe((content) => {
                        this.searchData();
                        this.modalService.dismissAll();

                    });
                }
                fileReader.readAsText(this.fileToUpload);
            }
        }
    }
    onExecuteTableRowUtilityAction(event) {
        this.selectedRow = event.row;
        if (event.action == "Borrar") {
            this.modalService.open(this.modalConfirmarBorrar, { size: 'sm', centered: true });;
        }
        if (event.action == "Descargar") {
            this.esquemaService.downloadFile({ filename: this.selectedRow.filename }).subscribe((content) => {
                this.export(content, this.selectedRow.filename);
            });
        }
    }
    onExecuteTableUtilityAction(event) {
        var self = this;
        if (event.action == "UPLOAD") {
            this.fileToUpload = null;
            this.modalService.open(this.modalUpload, { size: 'sm', centered: true });;
        }
    };
    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }
    public export(data, nameFile: string) {
        var contentType = 'application/html';
        var nameFile = nameFile;
        var blob = new Blob([data], { type: contentType });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, nameFile);
        }
        else {
            var a = document.createElement('a');
            a.href = URL.createObjectURL(blob);
            a.download = nameFile;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }
    }
}
