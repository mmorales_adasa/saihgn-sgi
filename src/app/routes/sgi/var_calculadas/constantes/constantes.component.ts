import { Component, OnInit } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicFilter, BasicTableModel } from '@arca/common';
@Component({
    selector: 'app-constantes-manager',
    templateUrl: './constantes.component.html',
    styleUrls: ['./constantes.component.scss'],
    providers: []
})
export class ConstantesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "desc_constante ASC";
    filterSelection = {};
    tablaConstantes;
    offset: number = 0;
    listTiposConstantes = [];
    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
            this.searchData(true);
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.TIPO_CONSTANTES, filters: [], sort: "desc_tipo_constante ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.listTiposConstantes = list;
                resolve();
            });
        });
    }
    searchData(count) {
        var params = { entity: constants.SGI.ENTITIES.CONSTANTES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        // Si descomento esto no me funciona la paginacion
        
        this._basicEntityService.getDataPagination(params).subscribe(list => {
                var params = { entity: constants.SGI.ENTITIES.CONSTANTES, filters: this.filterSelection }
                if (count)
                {
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.totalElements = total;
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
                else{
                    this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                        this.tablaConstantes.setDataAndLoadTable(list);
                });
                }
        });
    }
    configureTable() {
        var self = this;
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "cod_constante", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_tipo_constante", filter: true, displayed: true, translation: "Tipo" },
                { id: "desc_constante", filter: true, displayed: true, translation: "Descripción" },
                { id: "cod_estacion", filter: true, displayed: true, translation: "Cod Estación" },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Estación" },
            ],
            crud: {
                title: "Alta/Edición de constantes", 
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.CONSTANTES },
                fields: [
                    { row: 1, id: "cod_constante", label: "Código", pk: true, mandatory: false, sequence: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "cod_tipo_constante", label: "Tipo de constante", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listTiposConstantes, code: 'cod_tipo_constante', desc: 'desc_tipo_constante', field_in_table: 'desc_tipo_constante' },
                    { row: 2, id: "desc_constante", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 2, id: "valor", label: "Valor", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    {
                        row: 3, id: "cod_estacion", label: "Estación", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'cod_estacion', desc: 'desc_estacion', field_in_table: 'desc_estacion',
                        fun: function (params) {
                            return new Promise((resolve, reject) => {
                                params.sort = "desc_estacion desc ";
                                params.searchField = "desc_estacion";
                                params.entity = constants.SGI.ENTITIES.ESTACIONES;
                                params.filters = [];
                                if (params.text) { params.filters = [{ id: "desc_estacion", type: "TEXT", values: params.text }]; }
                                self._basicEntityService.getDataPagination(params).subscribe(
                                    (list: any[]) => {
                                        var temp = [];
                                        for (var item of list) {
                                            temp.push({ "COD": item.cod_estacion, "DESCR": item.desc_estacion + " (" + item.cod_estacion +  ")" });
                                        }
                                        resolve(temp);
                                    });
                            });
                        },
                    },

                ]
            },
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    configureFilters() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: false,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.considerEmptyError = false;
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            group: 1,
            width: 200,
            id: 'desc_constante',
            label: 'Descripción',
            defaultValue: null,
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            group: 1, id: 't1.cod_estacion', label: 'Estación', searchOption: false, width: 300, disabled: false, fun: function (params) {
                return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self);
            }
        });

        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPO_CONSTANTES }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 400,
                id: 't1.cod_tipo_constante',
                label: 'Tipo de constante',
                data: list,
                code: 'cod_tipo_constante',
                desc: 'desc_tipo_constante',
                searchOption: true
            });
        });
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(true);
    }
    onSort(event) {
        this.sort = event.active + " " + event.direction;        
        this.searchData(false);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(false);
    }
    afterExecuteCrudAction(event) {
        console.log(event);
    }
}
