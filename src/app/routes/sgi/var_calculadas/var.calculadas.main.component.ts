import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-var-calculadas-main',
    templateUrl: './var.calculadas.main.component.html',
    styleUrls: ['./var.calculadas.main.component.scss'],
    providers: []
})
export class VariablesCalculadasMainComponent implements OnInit {
    profile: any;
    selectedOption: string;
    rootSection = {
        title: "Variables y Fórmulas",
        class: "fa fa-calculator "
    }
    
    sections = [        
        {
        title: "Gestión de variables fórmula",
        route: "vc_vc",
        public: true,
        selected: this._route.snapshot.url[0].path == "vc_vc",
    },
    {
        title: "Gestión de variables simples",
        route: "vc_vs",
        public: true,
        selected: this._route.snapshot.url[0].path == "vc_vs",
    },    
    {
        title: "Gestión de  tipos de constantes",
        route: "vc_tc",
        public: true,
        selected: this._route.snapshot.url[0].path == "vc_tc",
    }, {
        title: "Gestion de constantes",
        route: "vc_c",
        public: true,
        selected: this._route.snapshot.url[0].path == "vc_c",
    }];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.VARIABLES_CALCULADAS);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
