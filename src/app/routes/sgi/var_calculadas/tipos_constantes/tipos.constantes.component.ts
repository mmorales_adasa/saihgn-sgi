import { Component, OnInit, AfterViewInit } from '@angular/core';
import { constants } from './../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
@Component({
    selector: 'app-tipos-constantes-manager',
    templateUrl: './tipos.constantes.component.html',
    styleUrls: ['./tipos.constantes.component.scss'],
    providers: []
})
export class TiposConstantesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    baseURLServiceCrud = environment.RESTAPI_BASE_URL + '/lab/admin';
    tablaTiposConstantes;
    listUnidades = [];    
    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.searchData();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.UNIDADES, filters: [], sort: "desc_unidad_medida ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.listUnidades = list;
                resolve();
            });
        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "cod_tipo_constante",  filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort : constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_tipo_constante", filter: true, displayed: true, translation: "Descripción" },
                { id: "desc_unidad_medida",  filter: true, displayed: true, translation: "Unidad" }
            ],
            crud: {
                title: "Alta/Edición de Tipos de constantes",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.TIPO_CONSTANTES },
                fields: [
                    { row: 1, id: "cod_tipo_constante", label: "Código", pk: true, mandatory: false, sequence : true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "desc_tipo_constante", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
                    { row: 2, id: "cod_unidad_medida", label: "Unidad", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listUnidades, code: 'cod_unidad_medida', desc: 'desc_unidad_medida', field_in_table: 'desc_unidad_medida' }

                ]
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort :{
                active : "cod_tipo_constante",
                direction : "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.TIPO_CONSTANTES, filters: this.filterSelection, sort: "desc_tipo_constante ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    }
    afterExecuteCrudAction(event) {
        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
            this.searchData();
        }
    }
}
