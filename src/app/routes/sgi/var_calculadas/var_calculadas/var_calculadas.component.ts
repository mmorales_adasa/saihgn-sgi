import { Component, OnInit, ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { FormulaService } from '../../../../services/arca_lab/formula.service';
import { BasicFilter, BasicTableModel } from '@arca/common';
import moment from 'moment';
import { ParserService } from 'src/app/services/arca_lab/parser.service';

@Component({
    selector: 'app-var-calculadas-manager',
    templateUrl: './var_calculadas.component.html',
    styleUrls: ['./var_calculadas.component.scss'],
    providers: [FormulaService, ParserService]
})
export class VarCalculadasComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "desc_variable ASC";
    filterSelection = [];
    tablaConstantes;
    offset: number = 0;
    lisTipoVariable = [];
    formulaEdition = "";
    variableSelected;
    // typeVariableSelected = "";
    visibleSelectorVariable: boolean = false;
    formulaService: any;
    parserService: any;
    @ViewChild('modalFormulaEditor') modalFormulaEditor: any;
    @ViewChild('modalRecalculo') modalRecalculo: any;
    dataTree: any = [];
    txtSearchTree;
    txtErroresFormula = "";
    txtFormulaTranslation = "";
    variableRecalculo;
    fechaIniRecalculo;
    errorFechaIniRecalculo;
    fechaFinRecalculo;
    errorFechaFinRecalculo;
    confTree: any = { isSelectable: false };
    loadingTree: boolean = false;
    symbols = ["(", ")", "+", "/", "-", "x", "^", "*"];
    cod_variable;
    tokensFormula;
    nodos: any = {};

    ngOnInit() {
        this.formulaService = this._injector.get(FormulaService);
        this.parserService = this._injector.get(ParserService);

        /*this.obtenerNodeBD("000985").then((node) => {
            alert(JSON.stringify(node)); 
        });*/

        /*var formulaTest = " V_EX2-17/IVE1 + V_EX2-17/AAT + V_EX2-17/ATT + F_000408 + V_EX2-17/ACT + V_EX2-17/AFT";
        var result = this.parserService.parse(formulaTest);
        console.log(result);  
        */
        //this.formulaService.testFormulas();
        /*
            console.log(this.parserService.isNumeric(1.2));
            console.log(this.parserService.isNumeric(1,2));
            console.log(this.parserService.isNumeric("1.2"));
            console.log(this.parserService.isNumeric("1,2"));
            */
        /*var pattern = "\\+"; 
        var s  = '+';
        var re = new RegExp(pattern, "g");
        var temp = "V_EX2-17/IVE1 + V_EX2-17/AAT + V_EX2-17/ATT+F_000408 +V_EX2-17/ACT +  V_EX2-17/AFT";
        const result = temp.replace(re, ' ' + s + ' ');
        console.log(result);*/

        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, filters: [], sort: "desc_tipo_variable ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.lisTipoVariable = list;
                resolve();
            });
        });
    }
    getNextId() {
        var self = this;
        return new Promise((resolve, reject) => {
            this.formulaService.getNextId({}).subscribe(id => {
                self.cod_variable = id;
                resolve(id);
            });
        });

    }
    searchData(pagination) {
        this.filterSelection.push({ id: "cod_origen", type: "TEXT", values: "CALCULADA" });
        var params = { entity: constants.SGI.ENTITIES.VARIABLES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {
            if (pagination) {
                this.tablaConstantes.setDataAndLoadTable(list);

            }
            else {
                var params = { entity: constants.SGI.ENTITIES.VARIABLES, filters: this.filterSelection }
                this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                    this.tablaConstantes.setTotalElements(total);
                    this.tablaConstantes.setDataAndLoadTable(list);
                    this.testParserFormulas(list);
                });
            }

        });
    }
    configureTable() {
        var self = this;
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "Recalcular", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Especificar recálculo", icon: "fa fa-calendar" } },
                { id: "cod_variable", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                { id: "desc_variable", filter: true, displayed: true, translation: "Descripción" },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Desc Estación" },
                { id: "cod_estacion", filter: true, displayed: true, translation: "Cod Estación" },
                { id: "desc_tipo_variable", filter: true, displayed: true, translation: "Tipo Variable" },

            ],
            crud: {
                title: "Alta/Edición de Variables calculadas",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.VARIABLES },
                fields: [
                    { row: 1, id: "cod_variable", label: "Código", pk: true, mandatory: false, sequence: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "cod_tipo_variable", label: "Tipo de Variable", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.lisTipoVariable, code: 'cod_tipo_variable', desc: 'desc_tipo_variable', field_in_table: 'desc_tipo_variable' },
                    { row: 2, id: "desc_variable", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null },
                    { row: 2, id: "cod_punto_medida", label: "Código Punto medida", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    {
                        row: 3, id: "cod_estacion", label: "Estación", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'cod_estacion', desc: 'desc_estacion', field_in_table: 'desc_estacion',
                        fun: function (params) {
                            return new Promise((resolve, reject) => {
                                params.sort = "desc_estacion desc ";
                                params.searchField = "desc_estacion";
                                params.entity = constants.SGI.ENTITIES.ESTACIONES;
                                params.filters = [];
                                if (params.text) { params.filters = [{ id: "desc_estacion", type: "TEXT", values: params.text }]; }
                                self._basicEntityService.getDataPagination(params).subscribe(
                                    (list: any[]) => {
                                        var temp = [];
                                        for (var item of list) {
                                            temp.push({ "COD": item.cod_estacion, "DESCR": item.desc_estacion + " (" + item.cod_estacion + ")" });
                                        }
                                        resolve(temp);
                                    });
                            });
                        },
                    },
                    { row: 3, id: "formula", label: "Fórmula", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 4000, defaultValue: null, assistant: true, disabled: true },
                    { row: 4, id: "cod_origen", label: "Origen", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.HIDDEN, maxLength: 4000, defaultValue: 'CALCULADA', assistant: true, disabled: true },
                    { row: 4, id: "cod_fuente_origen", label: "Origen", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.HIDDEN, maxLength: 4000, defaultValue: null, assistant: true, disabled: true },

                ]
            },
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "cod_variable",
                direction: "asc"
            }
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    configureFilters() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            group: 1, id: 't1.cod_estacion', label: 'Estación', searchOption: false, width: 400, disabled: false, fun: function (params) {
                return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self);
            }
        });

        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPOS_VARIABLE }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 400,
                id: 't1.cod_tipo_variable',
                label: 'Tipo de variable',
                data: list,
                code: 'cod_tipo_variable',
                desc: 'desc_tipo_variable',
                searchOption: true
            });
        });
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(false);
    }
    onSort(event) {
        ////console.log(event);
        this.sort = event.active + " " + event.direction;
        this.searchData(true);
    }
    onPagination(event) {
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(true);
    }
    afterExecuteCrudAction(event) {
        var self = this;
        if (event.action === "openAssistant") {
            this.openFormulaEditor();
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {
            this.formulaService.upgradeTokensFormula({ tokens: this.tokensFormula, cod_variable: this.tablaConstantes.crudModel.rowSelected.cod_variable }).subscribe((tokens) => {
                console.log('actualizado!');
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.formulaService.upgradeTokensFormula({ tokens: this.tokensFormula, cod_variable: this.cod_variable }).subscribe((tokens) => {
                console.log('actualizado!');
                this.searchData(true);
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.DELETE) {
            var cod_variable = event.data.model.cod_variable.value;
            this.formulaService.tractarVarsAillades({ cod_variable: cod_variable }).subscribe((tokens) => {
                console.log('actualizado!');
            });

        }
    }
    beforeExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.tokensFormula = null;
            this.cod_variable = null;
            this.getNextId().then((id) => {
                this.tablaConstantes.crudModel.elementMap.cod_variable.value = id;
                this.cod_variable = id;
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {
            this.tokensFormula = null;
            this.cod_variable = this.tablaConstantes.crudModel.rowSelected.cod_variable;
            this.formulaService.getTokensFormula({ cod_variable: this.tablaConstantes.crudModel.rowSelected.cod_variable }).subscribe((tokens) => {
                this.tokensFormula = tokens;
            });

            /*this.testFormula(this.tablaConstantes.crudModel.rowSelected).then((result) => {

            });*/

        }
    }
    openFormulaEditor() {
        this.formulaEdition = this.tablaConstantes.crudModel.elementMap.formula.value;
        this.txtSearchTree = null;
        this.txtErroresFormula = "";
        this.txtFormulaTranslation = "";
        this.modalService.open(this.modalFormulaEditor, { size: 'lg', centered: true });

        this.loadEstacionesVariablesYConstantes().then((id) => {
            this.loadingTree = false;
        });
    }
    loadEstacionesVariablesYConstantes() {
        return new Promise((resolve, reject) => {
            this.loadingTree = true;
            this.formulaService.findEstacionesVariablesYConstantes({ text: this.txtSearchTree }).subscribe(list => {
                this.dataTree = this.setDataTree(list);
                resolve(true);
            });
        });
    }
    searchTree() {
        this.loadingTree = true;
        this.loadEstacionesVariablesYConstantes().then((id) => {
            this.loadingTree = false;
            this.modalService.open(this.modalFormulaEditor, { size: 'lg', centered: true });
        });
    }
    getPrefixTree(type) {
        if (type === "VARIABLE") {
            return "[V] ";
        }
        else if (type === "CALCULADA") {
            return "[F] ";
        }
        else if (type === "CONSTANTE") {
            return "[C] ";
        }

    }

    setDataTree(list) {
        var estaciones = [];
        var cod_estacion_ant;
        var estacion: any;
        for (var item of list) {
            if (item.estacion.cod === cod_estacion_ant) {
                estacion.attributes.invalids = estacion.attributes.invalids || item.hasDataInvalids;
                estacion.children.push({
                    cod: item.cod,
                    //name: item.cod + " - " + item.desc,
                    //name: this.getTypePrefix(item.type) + item.cod,
                    name: this.getPrefixTree(item.type) + item.desc + " (" + item.cod + ")",
                    attributes: {
                        cod_variable: item.cod,
                        desc_variable: item.desc,
                        cod_estacion: item.estacion.cod,
                        desc_estacion: item.estacion.desc,
                        type: item.type,
                        tooltip: item.cod + " - " + item.desc
                        //invalids: Fs
                    }
                });
            }
            else {
                estacion = {
                    cod: item.estacion.cod,
                    name: item.estacion.cod + " - " + item.estacion.desc,
                    attributes: {
                        invalids: item.hasDataInvalids,
                    },
                    children: [{
                        cod: item.cod,
                        //name: item.cod + " - " + item.desc,
                        name: this.getPrefixTree(item.type) + item.desc + " (" + item.cod + ")",
                        //name: item.cod,
                        attributes: {
                            cod_variable: item.cod,
                            desc_variable: item.desc,
                            cod_estacion: item.estacion.cod,
                            desc_estacion: item.estacion.desc,
                            //invalids: item.hasDataInvalids
                            tooltip: item.cod + " - " + item.desc,
                            type: item.type
                        }
                    }]
                }
                cod_estacion_ant = estacion.cod;
                estaciones.push(estacion);
            }
        }
        return estaciones;
    }
    clickValidateFormula() {
        this.validateFormula().then((tokens) => {

        }).catch(error => {

        })
    }
    validateFormula() {

        return new Promise((resolve, reject) => {

            var test = { cod_variable: this.tablaConstantes.crudModel.rowSelected.cod_variable, formula: this.formulaEdition };
            this.testFormula(test).then((result) => {

            });

            this.txtErroresFormula = "Validando, espere por favor...";
            var result = this.parserService.syntaxParse(this.formulaEdition);
            if (!result.error) {
                this.txtFormulaTranslation = "";
                var resultFormula2PostFix = this.parserService.formula2PostFix(this.formulaEdition);
                if (resultFormula2PostFix.error) {
                    this.txtErroresFormula = "Error convertiendo a notación infija: " + resultFormula2PostFix.error;
                    reject("Error tokenizando la cadena introducida");
                }
                else {
                    var tokens2 = [];
                    var errors = [];
                    if (result.tokens) {
                        this.formulaService.checkTokensFormula(resultFormula2PostFix.tokens).subscribe(result => {
                            this.txtErroresFormula = "";
                            var temp = this.formulaEdition;
                            var num_termino = 1;
                            for (var item of result) {
                                // console.log(item);
                                if (isNaN(item.code) && (item.code.startsWith("V_") || item.code.startsWith("F_") || item.code.startsWith("C_"))) {
                                    if (item.translation) {
                                        if (item.code.startsWith("C_")) {

                                            tokens2.push({
                                                num_termino: num_termino,
                                                cod_tipo_termino: 'CTE',
                                                tipo_valor: null,
                                                constante: item.code.replace('C_', ''),
                                                constante_num: null,
                                                variable: null,
                                                cod_variable: this.cod_variable,
                                                operador: null,
                                                calculada: null
                                            });
                                        }
                                        if (item.code.startsWith("F_")) {
                                            tokens2.push({
                                                num_termino: num_termino,
                                                cod_tipo_termino: 'VARC',
                                                tipo_valor: null,
                                                constante: null,
                                                constante_num: null,
                                                variable: null,
                                                cod_variable: this.cod_variable,
                                                operador: null,
                                                calculada: item.code.replace('F_', '')
                                            });
                                        }
                                        if (item.code.startsWith("V_")) {
                                            tokens2.push({
                                                num_termino: num_termino,
                                                cod_tipo_termino: 'VAR',
                                                tipo_valor: null,
                                                constante: null,
                                                constante_num: null,
                                                variable: item.code.replace('V_', ''),
                                                cod_variable: this.cod_variable,
                                                operador: null,
                                                calculada: null
                                            });
                                        }
                                        temp = temp.replace(item.code, item.translation);
                                    }
                                    else {
                                        errors.push(" " + item.code + " no definido.")
                                    }
                                }
                                else {
                                    if (isNaN(item.code) && (item.code.length > 1 || !this.symbols.includes(item.code))) {
                                        errors.push(" " + item.code + " no definido.")
                                    }
                                    if (this.parserService.isNumeric(item.code)) {
                                        tokens2.push({
                                            num_termino: num_termino,
                                            cod_tipo_termino: 'CTEN',
                                            tipo_valor: null,
                                            constante: null,
                                            constante_num: item.code,
                                            variable: null,
                                            cod_variable: this.cod_variable,
                                            operador: null,
                                            calculada: null
                                        });
                                    }
                                    if (this.symbols.includes(item.code)) {
                                        tokens2.push({
                                            num_termino: num_termino,
                                            cod_tipo_termino: 'OPER',
                                            tipo_valor: null,
                                            constante: null,
                                            constante_num: null,
                                            variable: null,
                                            cod_variable: this.cod_variable,
                                            operador: item.code,
                                            calculada: null
                                        });
                                    }
                                }
                                num_termino++;
                            }
                            if (errors.length > 0) {
                                this.txtErroresFormula = JSON.stringify(errors);
                                reject("Inválida");
                            }
                            else {

                                this.txtErroresFormula = "Fórmula correcta!\n ";
                                this.txtErroresFormula = "------------\n ";
                                this.txtErroresFormula += "Tokens detectados en notación infija:\n";
                                for (var token of resultFormula2PostFix.tokens) {
                                    this.txtErroresFormula += token + "\n";
                                }
                                this.txtFormulaTranslation = temp;

                                resolve(tokens2);
                            }
                        });
                    }
                    else {
                        this.txtErroresFormula = "Error tokenizando la cadena introducida";
                        reject("Error tokenizando la cadena introducida");
                    }
                }
            }
            else {
                this.txtErroresFormula = result.error;
                reject(result.error);
            }
        });
    }

    especificarRecalculo(event) {
        this.variableRecalculo = event.row;
        this.fechaIniRecalculo = null;
        this.fechaFinRecalculo = null;
        this.errorFechaFinRecalculo = null;
        this.errorFechaFinRecalculo = null;
        this.modalService.open(this.modalRecalculo, { size: 'lg', centered: true });
    }
    confirmFormulaEdition(modal) {
        this.validateFormula().then((tokens) => {
            this.tablaConstantes.crudModel.elementMap.formula.value = this.parserService.cleanString(this.formulaEdition);
            this.tokensFormula = tokens;
            console.log('confirmFormulaEdition');
            console.log(this.tokensFormula);

            modal.dismiss('Cross click');
        }).catch(error => {
            alert('Fórmula inválida, revise los errores.');
        })
    }
    validateRecalculo() {
        this.errorFechaFinRecalculo = null;
        this.errorFechaFinRecalculo = null;
        if (!this.fechaIniRecalculo || this.fechaIniRecalculo == "") {
            this.errorFechaIniRecalculo = "Campo obligatorio";
        }
        if (!this.fechaFinRecalculo || this.fechaFinRecalculo == "") {
            this.errorFechaFinRecalculo = "Campo obligatorio";
        }
        if (this.errorFechaIniRecalculo || this.errorFechaFinRecalculo) {
            return false;
        }
        return true;
    }
    confirmRecalculo(modal) {
        if (this.validateRecalculo()) {
            this.formulaService.setRecalculo({
                cod_variable: this.variableRecalculo.cod_variable,
                fecha_ini: moment(this.fechaIniRecalculo).format("YYYY-MM-DD"),
                fecha_fin: moment(this.fechaFinRecalculo).format("YYYY-MM-DD"),
            }).subscribe(list => {
                modal.dismiss('Cross click');
            });
        }
    }
    onChangeSelection(node) {
        if (node.attributes.type === "CONSTANTE") {
            this.addToFormula("C_" + node.code)
        }
        if (node.attributes.type === "VARIABLE") {
            this.addToFormula("V_" + node.code)
        }
        if (node.attributes.type === "CALCULADA") {
            this.addToFormula("F_" + node.code)
        }
    }
    addToFormula(text) {
        if (!this.formulaEdition)
            this.formulaEdition = "";
        this.formulaEdition += " " + text + " ";
    }
    testParserFormulas(list) {
        //list = [{cod_variable : '000409', formula : 'V_EX2-17/IVE1 + V_EX2-17/AAT + V_EX2-17/ATT+F_000408 + V_EX2-17/ACT + V_EX2-17/AFT'}];

        var max = list.length;
        var i = 0;
        var exito = 0;
        var error = 0;
        for (var i = 0; i < max; i++) {
            var item = list[i];
           // if (item.cod_variable == '000035') {
                var promises = [];
                var p = this.testFormula(item).then((result) => {
                    exito++;
                }).catch(cod_variable => {
                    error++;
                })
                promises.push(p);
            //}
        }
        Promise.all(promises).then(values => {
            console.log("Exito " + exito);
            console.log("Error " + error);
        });

    }
    getValue(term) {
        if (term.cod_tipo_termino === "VAR") { return term.variable }
        if (term.cod_tipo_termino === "VARC") { return term.calculada }
        if (term.cod_tipo_termino === "OPER") { return term.operador }
        if (term.cod_tipo_termino === "CTE") { return term.constante }
        if (term.cod_tipo_termino === "CTEN") { return term.constante_num }
        return "not found!";
    }
    testFormula(item) {
        //console.clear();        
        return new Promise((resolve, reject) => {
            var resultSyntaxParse = this.parserService.syntaxParse(item.formula);
            if (!resultSyntaxParse.error) {
                this.formulaService.getTokensFormula({ cod_variable: item.cod_variable }).subscribe((tokens) => {

                    var result = this.parserService.formula2PostFix(item.formula);
                    if (result.error) {
                        console.log(item.cod_variable + ' NOK!: Error convertido a notación postfija');
                        reject();
                    }
                    else {
                        var tokensApp = result.tokens;
                        if (tokens.length != tokensApp.length) {
                            console.log(item.cod_variable + ' NOK!: Distinto número de terminos');
                        }


                        var strBBD = "";
                        var strApp = "";
                        for (var i = 0; i < tokens.length; i++) {
                            strBBD += "[" + this.getValue(tokens[i]) + "] ";

                            var temp = tokensApp[i];
                            if (typeof temp === 'string') {
                                temp = tokensApp[i].replace("V_", "");
                                temp = temp.replace("F_", "");
                                temp = temp.replace("C_", "");
                            }
                            strApp += "[" + temp + "] ";


                        }
                        /*console.log("-----------------------------");
                        console.log("ORIG  -> " + item.formula);
                        console.log("BB.DD -> " + strBBD);
                        console.log("APP.  -> " + strApp);*/
                        if (strBBD.trim() === strApp.trim()) {
                            console.log(item.cod_variable + " OK!")
                            resolve({
                                tokensBB: tokens,
                                tokensApp: tokensApp
                            });                            
                        }
                        else {
                            console.log(item.cod_variable + " NOK! Distintos!!");
                            console.log("   -----------------------------");
                            console.log("   ORIG  -> " + item.formula);
                            console.log("   BB.DD -> " + strBBD);
                            console.log("   APP.  -> " + strApp);
                            reject();
                        }



                    }
                });
            }
            else {
                console.log(item.cod_variable + ' NOK!: Parser error ' + resultSyntaxParse.error + "(" + item.formula + ")");
                reject(item.cod_variable);
            }
        });
    }
}
