import { Component, OnInit, ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { FormulaService } from '../../../../services/arca_lab/formula.service';
import { BasicFilter, BasicTableModel } from '@arca/common';
import moment from 'moment';
import { ParserService } from 'src/app/services/arca_lab/parser.service';


@Component({
    selector: 'app-var-simple-manager',
    templateUrl: './var_simple.component.html',
    styleUrls: ['./var_simple.component.scss'],
    providers: [FormulaService]
})
export class VarSimpleComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filter;
    sort: string = "desc_variable ASC";
    filterSelection = [];
    tablaConstantes;
    formulaService: any;

    offset: number = 0;
    lisTipoVariable = [];
    lisOrigenes = [];
    variableSelected;
    visibleSelectorVariable: boolean = false;
    dataTree: any = [];
    //cod_variable;
    
    ngOnInit() {
        this.formulaService = this._injector.get(FormulaService);

        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, filters: [], sort: "desc_tipo_variable ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.lisTipoVariable = list;
                var params = { entity: constants.SGI.ENTITIES.ORIGENES, filters: [], sort: "desc_fuente_origen ASC" }
                this._basicEntityService.getData(params).subscribe(list => {
                    this.lisOrigenes = list;
                    resolve();
                });                
                
            });
        });
    }
    /*getNextId() {
        var self = this;
        return new Promise((resolve, reject) => {
            this.formulaService.getNextId({}).subscribe(id => {
                self.cod_variable = id;
                resolve(id);
            });
        });

    }*/
    searchData(pagination) {        
        this.filterSelection.push({ id: "cod_origen", type: "NULL", values: "NULL" });
        var params = { entity: constants.SGI.ENTITIES.VARIABLES, offset: this.offset, limit: constants.DEFAULT_LIMIT_PAGINATION, filters: this.filterSelection, sort: this.sort }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getDataPagination(params).subscribe(list => {
            if (pagination) {
                this.tablaConstantes.setDataAndLoadTable(list);
            }
            else {
                var params = { entity: constants.SGI.ENTITIES.VARIABLES, filters: this.filterSelection }
                this._basicEntityService.getDataPaginationCount(params).subscribe(total => {
                    this.tablaConstantes.setTotalElements(total);
                    this.tablaConstantes.setDataAndLoadTable(list);
                });
            }

        });
    }
    configureTable() {
        var self = this;
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "cod_variable", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                { id: "desc_variable", filter: true, displayed: true, translation: "Descripción" },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Desc Estación" },
                { id: "cod_estacion", filter: true, displayed: true, translation: "Cod Estación" },
                { id: "desc_tipo_variable", filter: true, displayed: true, translation: "Tipo Variable" },

            ],
            crud: {
                title: "Alta/Edición de Variables simples",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.VARIABLES },
                fields: [
                    { row: 1, id: "cod_variable", label: "Código", pk: true, mandatory: false, sequence: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "cod_tipo_variable", label: "Tipo de Variable", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.lisTipoVariable, code: 'cod_tipo_variable', desc: 'desc_tipo_variable', field_in_table: 'desc_tipo_variable' },
                    { row: 2, id: "desc_variable", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null },
                    { row: 2, id: "cod_punto_medida", label: "Código Punto medida", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    {
                        row: 3, id: "cod_estacion", label: "Estación", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'cod_estacion', desc: 'desc_estacion', field_in_table: 'desc_estacion',
                        fun: function (params) {
                            return new Promise((resolve, reject) => {
                                params.sort = "desc_estacion desc ";
                                params.searchField = "desc_estacion";
                                params.entity = constants.SGI.ENTITIES.ESTACIONES;
                                params.filters = [];
                                if (params.text) { params.filters = [{ id: "desc_estacion", type: "TEXT", values: params.text }]; }
                                self._basicEntityService.getDataPagination(params).subscribe(
                                    (list: any[]) => {
                                        var temp = [];
                                        for (var item of list) {
                                            temp.push({ "COD": item.cod_estacion, "DESCR": item.desc_estacion + " (" + item.cod_estacion + ")" });
                                        }
                                        resolve(temp);
                                    });
                            });
                        },
                    },                    
                    { row: 3, id: "cod_fuente_origen", label: "Origen", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.lisOrigenes, code: 'cod_fuente_origen', desc: 'desc_fuente_origen', field_in_table: 'desc_fuente_origen' },                    
                    { row: 4, id: "formula", label: "Fórmula", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.HIDDEN, maxLength: 4000, defaultValue: null, assistant: true, disabled: true },
                    { row: 4, id: "cod_origen", label: "Origen", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.HIDDEN, maxLength: 4000, defaultValue: null, assistant: true, disabled: true },
                ]
            },
            selectable: false,
            serverPagination: true,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "cod_variable",
                direction: "asc"
            }
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    configureFilters() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            group: 1, id: 't1.cod_estacion', label: 'Estación', searchOption: false, width: 300, disabled: false, fun: function (params) {
                return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self);
            }
        });

        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, sort : 'desc_tipo_variable' }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 400,
                id: 't1.cod_tipo_variable',
                label: 'Tipo de variable',
                data: list,
                code: 'cod_tipo_variable',
                desc: 'desc_tipo_variable',
                searchOption: true
            });
        });

        this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ORIGENES,sort : 'desc_fuente_origen' }).subscribe(list => {
            this.filter.addFilter({
                dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
                group: 1,
                width: 200,
                id: 't1.cod_fuente_origen',
                label: 'Origen de datos',
                data: list,
                code: 'cod_fuente_origen',
                desc: 'desc_fuente_origen',
                searchOption: true
            });
        });        
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(false);
    }
    onSort(event) {
        this.sort = event.active + " " + event.direction;
        this.searchData(true);
    }
    onPagination(event) {
        //console.log("del server");
        this.offset = constants.DEFAULT_LIMIT_PAGINATION * event.pageIndex;
        this.searchData(true);
    }
    afterExecuteCrudAction(event) {
        var self = this;
        /*if (event.action === "openAssistant") {
            this.openFormulaEditor();
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {
            console.log(this.tablaConstantes.crudModel.rowSelected.cod_variable);
            console.log(this.tokensFormula);
            console.log('actual tokens + edition');
            this.formulaService.upgradeTokensFormula({ tokens: this.tokensFormula, cod_variable: this.tablaConstantes.crudModel.rowSelected.cod_variable }).subscribe((tokens) => {
                //self.formulaService.canviarNivellsBottomUp(this.tablaConstantes.crudModel.rowSelected.cod_variable, []).then((result) => {
                console.log('actualizado!');
                //});
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.formulaService.upgradeTokensFormula({ tokens: this.tokensFormula, cod_variable: this.cod_variable }).subscribe((tokens) => {
                //self.formulaService.canviarNivellsBottomUp(this.tablaConstantes.crudModel.rowSelected.cod_variable, []).then((result) => {
                console.log('actualizado!');
                this.searchData(true);
                //});
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.DELETE) {
            var cod_variable = event.data.model.cod_variable.value;
            this.formulaService.tractarVarsAillades({ cod_variable: cod_variable }).subscribe((tokens) => {
                console.log('actualizado!');
            });

        }*/
    }
}
