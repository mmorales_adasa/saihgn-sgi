import { Component, OnInit, Input } from '@angular/core';
import { constants } from '../../../../../config/constants';
import { environment } from './../../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
@Component({
    selector: 'app-valores-umbral-manager',
    templateUrl: './valores-umbral.component.html',
    styleUrls: ['./valores-umbral.component.scss'],
    providers: []
})
export class ValoresUmbralComponent implements OnInit {
    @Input() data: any;
    listAnios = [];
    listTrimestres = [1, 2, 3, 4];
    listSemestres = [1, 2];
    listMeses = [{ id: '1', name: '01.Enero' }, { id: '2', name: '02.Febrero' }, { id: '3', name: '03.Marzo' },
    { id: '4', name: '04.Abril' }, { id: '5', name: '05.Mayo' }, { id: '6', name: '06.Junio' },
    { id: '7', name: '07.Julio' }, { id: '8', name: '08.Agosto' }, { id: '9', name: '09.Septiembre' },
    { id: '10', name: '10.Octubre' }, { id: '11', name: '11.Noviembre' }, { id: '12', name: '12.Diciembre' },

    ]



        ;
    public selectedAnioH = "";
    public selectedTrimestreH = "";

    constructor() {
    }
    ngOnInit() {
        for (var i = 2000; i < 2030; i++) {
            this.listAnios.push(i);
        }
        var index = 0;
        for (var item of this.data.umbrales) {
            console.log(item);
            item.position = index
            index++;
        }
    }
    deleteUmbral(umbral) {
        console.log(umbral.position);
        this.data.umbrales.splice(umbral.position, 1);
    }
    addUmbral() {
        var position = 0;
        if (this.data.umbrales.length > 0) {
            for (var item of this.data.umbrales) {
                if (item.position > position) { position = item.position };
            }
            position++;
        }
        this.data.umbrales.push({
            cod_valor_ref: this.data.conf.cod_valor_ref,
            anyo: null,
            anyoh: null,
            trimestre_h: null,
            mes: null,
            semana: null,
            dia: null,
            valor: null,
            observaciones: null
        });


    }

    /*    filterUmbrales() {
            var self = this;
            var ar = [];
            if (this.data.conf.cod_nivel_temp === 'AH') {
                ar = this.data.umbrales.filter(function (item) { return item.anyoh == self.selectedAnioH; });
            }
            else if (this.data.conf.cod_nivel_temp === 'TH') {
                ar = this.data.umbrales.filter(function (item) { return item.anyoh == self.selectedAnioH && item.trimestre_h == self.selectedTrimestreH; });
            }
    
            if (!ar) return [];
            else return ar;
        }*/

    getTextPeriodoSelecionado() {
        if (this.data.conf.cod_nivel_temp === 'AH' && this.selectedAnioH != "") {
            return "Año hidrológico " + this.selectedAnioH;
        }
        return "No hay periodo seleccionado.";
    }
}
