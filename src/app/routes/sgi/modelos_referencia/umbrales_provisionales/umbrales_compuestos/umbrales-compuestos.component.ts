import { Component, OnInit, Input } from '@angular/core';
import { constants } from '../../../../../config/constants';
import { environment } from './../../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
@Component({
    selector: 'app-umbrales-compuestos-manager',
    templateUrl: './umbrales-compuestos.component.html',
    styleUrls: ['./umbrales-compuestos.component.scss'],
    providers: []
})
export class UmbralesCompuestosComponent implements OnInit {
    @Input() data: any;
    listAnios = [];
    listTrimestres = [1, 2, 3, 4];
    public selectedAnioH = "";
    public selectedTrimestreH = "";

    constructor() {
    }
    ngOnInit() {
        for (var i = 2000; i < 2030; i++) {
            this.listAnios.push(i);
        }
        var index = 0;
        for (var item of this.data.umbrales) {
            item.position = index
            index++;
        }        

    }
    deleteUmbral(umbral) {
        this.data.umbrales.splice(umbral.position, 1);
    }
    addUmbral() {
        var position = 0;
        if (this.data.umbrales.length > 0) {
            for (var item of this.data.umbrales) {
                if (item.position > position) { position = item.position };
            }
            position++;
        }
        if (this.data.conf.cod_nivel_temp === 'AH') {
            if (this.selectedAnioH != "") {
                this.data.umbrales.push({
                    cod_valor_ref: this.data.conf.cod_valor_ref,
                    anyo: null,
                    anyoh: this.selectedAnioH,
                    trimestre_h: this.selectedTrimestreH,
                    valor_var1_min: null,
                    valor_var1_max: null,
                    valor_var2_min: null,
                    valor_var2_max: null,
                    valor: null,
                    observaciones: null
                });
            } else {
                alert("No hay seleccionado año hidrológico");
            }
        }
    }

    filterUmbrales() {
        var self = this;
        var ar = [];
        if (this.data.conf.cod_nivel_temp === 'AH') {
            ar = this.data.umbrales.filter(function (item) { return item.anyoh == self.selectedAnioH; });
        }
        else if (this.data.conf.cod_nivel_temp === 'TH') {
            ar = this.data.umbrales.filter(function (item) { return item.anyoh == self.selectedAnioH && item.trimestre_h == self.selectedTrimestreH; });
        }

        if (!ar) return [];
        else return ar;
    }

    getTextPeriodoSelecionado() {
        if (this.data.conf.cod_nivel_temp === 'AH' && this.selectedAnioH != "") {
            return "Año hidrológico " + this.selectedAnioH;
        }
        return "No hay periodo seleccionado.";
    }
}
