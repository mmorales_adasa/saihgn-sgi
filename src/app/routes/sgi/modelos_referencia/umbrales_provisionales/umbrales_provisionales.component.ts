import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { constants } from '../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-umbrales_provisionales-manager',
    templateUrl: './umbrales_provisionales.component.html',
    styleUrls: ['./umbrales_provisionales.component.scss'],
    providers: []
})
export class UmbralesProvisionalesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = [];
    baseURLServiceCrud = environment.RESTAPI_BASE_URL + '/lab/admin';
    tablaTiposConstantes;
    listSistemasEscenarios = [];
    listNivelTemp = [];
    listIndUmbr = [];
    dataUmbralesModal;
    @Input() umbralOficial: boolean = false;
    tipoUmbral = 'N';
    @ViewChild('modalSelectVariable') modalSelectVariable: any;
    @ViewChild('modalEdicionUmbrales') modalEdicionUmbrales: any;
    selectedVariable;
    selectedField;
    ngOnInit() {
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.MODELOS_REFERENCIA);
        this.profile = this._auditoriaService.getUserProfile();
        if (this.umbralOficial) {
            this.tipoUmbral = 'S';
        }
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.searchData();
        });
    }
    searchMasterData() {
        var self = this;
        return new Promise((resolve, reject) => {
            var obs1 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.SISTEMAS_ESCENARIOS, filters: [], sort: "cod_decreto ASC" });
            var obs2 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MODEL_NIVEL_TEMPORAL, filters: [], sort: "desc_nivel_temp ASC" });
            var obs3 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MODEL_INDICADOR_UMBRAL, filters: [], sort: "desc_indicador_umbral ASC" });

            return forkJoin([obs1, obs2, obs3]).subscribe(([res1, res2, res3]) => {
                this.listSistemasEscenarios = res1;
                this.listNivelTemp = res2;
                this.listIndUmbr = res3;
                // De momento restringo los niveles tempoerl
                if (this.tipoUmbral==='N'){
                    this.listNivelTemp = this.listNivelTemp.filter(function (item) { return (item.cod_nivel_temp == 'AH' || item.cod_nivel_temp == 'TH' ) });

                }
                else {
                    this.listNivelTemp = this.listNivelTemp.filter(function (item) { return (item.cod_nivel_temp == 'M' || item.cod_nivel_temp == 'AH' ) });
                }

                resolve();
            });
        });
    }
    configureTable() {

        var fieldsCrud: any = [{ row: 1, id: "cod_valor_ref", label: "Código", pk: true, sequence: true, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
        { row: 1, id: "cod_escenario_decreto", label: "Sistema Escenario ", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listSistemasEscenarios, code: 'cod_escenario_decreto', desc: 'cod_escenario_decreto', field_in_table: 'cod_escenario_decreto' },
        { row: 2, id: "cod_nivel_temp", label: "Nivel temporal", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listNivelTemp, code: 'cod_nivel_temp', desc: 'desc_nivel_temp', field_in_table: 'desc_nivel_temp' },
        { row: 2, id: "cod_indicador_umbral", label: "Umbral", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listIndUmbr, code: 'cod_indicador_umbral', desc: 'desc_indicador_umbral', field_in_table: 'desc_indicador_umbral' },
        { row: 1, id: "ind_valor_oficial", typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.HIDDEN, defaultValue: this.tipoUmbral }
        ];

        if (this.tipoUmbral === 'N') {
            fieldsCrud.push({ row: 3, id: "cod_var_ref1", label: "Variable Ref 1", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT, code: 'cod_variable', desc: 'desc_variable', field_in_table: 'cod_var_ref1' });
            fieldsCrud.push({ row: 3, id: "cod_nivel_temp1", label: "Nivel temporal 1", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listNivelTemp, code: 'cod_nivel_temp', desc: 'desc_nivel_temp', field_in_table: 'desc_nivel_temp1' });
            fieldsCrud.push({ row: 4, id: "cod_var_ref2", label: "Variable Ref 2", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT, code: 'cod_variable', desc: 'desc_variable', field_in_table: 'cod_var_ref2' });
            fieldsCrud.push({ row: 4, id: "cod_nivel_temp2", label: "Nivel temporal 2", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listNivelTemp, code: 'cod_nivel_temp', desc: 'desc_nivel_temp', field_in_table: 'desc_nivel_temp2' });
        }

        var tableConfiguration = {
            id: 'mr_mol',
            title: null,
            columns: [
                { id: "Umbrales", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Ver Umbrales", icon: "fa fa-list" } },
                { id: "cod_valor_ref", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_decreto", filter: true, displayed: true, translation: "Modelo" },
                { id: "desc_sistema", filter: true, displayed: true, translation: "Sistema" },
                { id: "desc_escenario", filter: true, displayed: true, translation: "Escenario" },
                { id: "desc_indicador_umbral", filter: true, displayed: true, translation: "Umbral" },
                { id: "desc_nivel_temp", filter: true, displayed: true, translation: "Variación Temporal" }

            ],
            crud: {
                title: this.tipoUmbral === 'N' ? "Alta/Edición de Umbrales provisionales" : "Alta/Edición de Umbrales oficiales",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.VALOR_REFERENCIA },
                fields: fieldsCrud
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "desc_decreto",
                direction: "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        this.filterSelection = [];
        this.filterSelection.push({ id: "ind_valor_oficial", type: "TEXT", values: this.tipoUmbral });

        var params = { entity: constants.SGI.ENTITIES.VALOR_REFERENCIA, filters: this.filterSelection, sort: "cod_valor_ref ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    }
    afterExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.upgradeVariablesRef(event.data).then((resp) => {
                this.searchData();
            });
        }
        if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {
            if (this.tipoUmbral === 'N') {
                this.upgradeVariablesRef(event.data);
            }
        }
        if (event.action === "openAssistant") {
            this.selectedField = event.data;
            this.openVariableSelector();
        }
    }
    upgradeVariablesRef(model) {
        return new Promise((resolve, reject) => {
            var self = this;
            console.log(model);
            this._basicEntityService.save({
                entity: constants.SGI.ENTITIES.MODEL_VARIABLE_REF,
                mode: constantsCommon.BASIC_CRUD_MODE.DELETE,
                model: {
                    cod_valor_ref: { value: model.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                }
            }).subscribe((result: any[]) => {
                self._basicEntityService.save({
                    entity: constants.SGI.ENTITIES.MODEL_VARIABLE_REF,
                    mode: constantsCommon.BASIC_CRUD_MODE.INSERT,
                    model: {
                        cod_valor_ref: { value: model.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                        cod_nivel_temp1: { value: model.cod_nivel_temp1, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                        cod_nivel_temp2: { value: model.cod_nivel_temp2, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                        cod_var_ref1: { value: model.cod_var_ref1, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                        cod_var_ref2: { value: model.cod_var_ref2, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                    }
                }).subscribe((result: any[]) => {
                    resolve()
                });
            });
        });
    }

    upgradeUmbralesCompuestos() {
        return new Promise((resolve, reject) => {
            var self = this;

            this._basicEntityService.save({
                entity: constants.SGI.ENTITIES.MODEL_UMBRAL_COMPUESTO,
                mode: constantsCommon.BASIC_CRUD_MODE.DELETE,
                model: {
                    cod_valor_ref: { value: self.dataUmbralesModal.conf.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                }
            }).subscribe((result: any[]) => {
                var operations = [];
                var results = [];
                if (this.dataUmbralesModal.umbrales.length > 0) {
                    for (var umbral of this.dataUmbralesModal.umbrales) {
                        var op = self._basicEntityService.save({
                            entity: constants.SGI.ENTITIES.MODEL_UMBRAL_COMPUESTO,
                            mode: constantsCommon.BASIC_CRUD_MODE.INSERT,
                            model: {
                                cod_valor_ref: { value: umbral.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                anyo: { value: umbral.anyo, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                anyoh: { value: umbral.anyoh, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                trimestre_h: { value: umbral.trimestre_h, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                valor_var1_min: { value: umbral.valor_var1_min, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                valor_var1_max: { value: umbral.valor_var1_max, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                valor_var2_min: { value: umbral.valor_var2_min, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                valor_var2_max: { value: umbral.valor_var2_max, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                valor: { value: umbral.valor, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                observaciones: { value: umbral.observaciones, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                            }
                        }).subscribe((result: any[]) => {

                        });
                        operations.push(op);
                        results.push({});
                    }
                    return forkJoin([operations]).subscribe(([results]) => {
                        resolve();

                    });
                }
                else {
                    resolve();
                }

            });
        });
    }

    upgradeValoresUmbral() {
        return new Promise((resolve, reject) => {
            var self = this;

            this._basicEntityService.save({
                entity: constants.SGI.ENTITIES.MODEL_VALORES_UMBRAL,
                mode: constantsCommon.BASIC_CRUD_MODE.DELETE,
                model: {
                    cod_valor_ref: { value: self.dataUmbralesModal.conf.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                }
            }).subscribe((result: any[]) => {
                var operations = [];
                var results = [];
                if (this.dataUmbralesModal.umbrales.length > 0) {
                    for (var umbral of this.dataUmbralesModal.umbrales) {
                        var op = self._basicEntityService.save({
                            entity: constants.SGI.ENTITIES.MODEL_VALORES_UMBRAL,
                            mode: constantsCommon.BASIC_CRUD_MODE.INSERT,
                            model: {
                                cod_valor_ref: { value: umbral.cod_valor_ref, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                anyo: { value: umbral.anyo, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                anyoh: { value: umbral.anyoh, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                trimestre_h: { value: umbral.trimestre_h, type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                                dia: { value: umbral.dia, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                mes: { value: umbral.mes, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                semana: { value: umbral.semana, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                valor: { value: umbral.valor, type: constantsCommon.TYPE_DATA_FILTER.TYPE_NUMBER },
                                observaciones: { value: umbral.observaciones, type: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                            }
                        }).subscribe((result: any[]) => {

                        });
                        operations.push(op);
                        results.push({});
                    }
                    return forkJoin([operations]).subscribe(([results]) => {
                        resolve();

                    });
                }
                else {
                    resolve();
                }

            });
        });
    }
    showUmbrales(event) {
        if (this.tipoUmbral === 'N') {
            this._basicEntityService.getData({
                entity: constants.SGI.ENTITIES.MODEL_UMBRAL_COMPUESTO,
                filters: [{ id: 'cod_valor_ref', type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, values: [event.row.cod_valor_ref] }]

            }).subscribe((result: any[]) => {
                result.sort(function (a, b) { if (a.valor_var1_min > b.valor_var1_min) { return 1; } if (a.valor_var1_min < b.valor_var1_min) { return -1; } return 0; });
                this.dataUmbralesModal = {
                    conf: event.row,
                    umbrales: result
                };
                this.modalService.open(this.modalEdicionUmbrales, { size: 'lg', centered: true });
            });
        }
        else{
            this._basicEntityService.getData({
                entity: constants.SGI.ENTITIES.MODEL_VALORES_UMBRAL,
                filters: [{ id: 'cod_valor_ref', type: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, values: [event.row.cod_valor_ref] }]

            }).subscribe((result: any[]) => {
                //result.sort(function (a, b) { if (a.valor_var1_min > b.valor_var1_min) { return 1; } if (a.valor_var1_min < b.valor_var1_min) { return -1; } return 0; });
                this.dataUmbralesModal = {
                    conf: event.row,
                    umbrales: result
                };
                this.modalService.open(this.modalEdicionUmbrales, { size: 'lg', centered: true });
            }); 
        }
    }
    openVariableSelector() {
        this.modalService.open(this.modalSelectVariable, { size: 'lg', centered: true });
    }
    onSelectVariable(event) {
        this.selectedVariable = event.cod_variable;
    }
    saveUmbrales(modal) {
        if (this.tipoUmbral === 'N') {
            this.upgradeUmbralesCompuestos().then((resp) => {
                modal.dismiss('Cross click');
            });
        }
        else{
            this.upgradeValoresUmbral().then((resp) => {
                modal.dismiss('Cross click');
            });            
        }


    }
    selectVariable(modal) {
        this.tablaTiposConstantes.setValueCrud(this.selectedField, this.selectedVariable);
        modal.dismiss('Cross click')
    }
}
