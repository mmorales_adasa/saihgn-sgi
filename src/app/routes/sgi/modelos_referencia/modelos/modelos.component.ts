import { Component, OnInit } from '@angular/core';
import { constants } from '../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
@Component({
    selector: 'app-modelos-manager',
    templateUrl: './modelos.component.html',
    styleUrls: ['./modelos.component.scss'],
    providers: []
})
export class ModelosComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    baseURLServiceCrud = environment.RESTAPI_BASE_URL + '/lab/admin';
    tablaTiposConstantes;
    listUnidades = [];
    ngOnInit() {
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.MODELOS_REFERENCIA);
        this.profile = this._auditoriaService.getUserProfile();
        this.configureTable();
        this.searchData();
    }
    configureTable() {
        var tableConfiguration = {
            id: 'mr_mol',
            title: null,
            columns: [
                { id: "cod_decreto", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_decreto", filter: true, displayed: true, translation: "Descripción" }
            ],
            crud: {
                title: "Alta/Edición de Modelos",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.MODELOS },
                fields: [
                    { row: 1, id: "cod_decreto",  label: "Código", pk: true, sequence : true, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "desc_decreto", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null }
                ]
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "cod_decreto",
                direction: "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.MODELOS, filters: this.filterSelection, sort: "desc_decreto ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    }
    afterExecuteCrudAction(event) {
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData();
        }
    }

}
