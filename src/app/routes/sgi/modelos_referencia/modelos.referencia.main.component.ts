import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-modelos-referencia-main',
    templateUrl: './modelos.referencia.main.component.html',
    styleUrls: ['./modelos.referencia.main.component.scss'],
    providers: []
})
export class ModelosRererenciaMainComponent implements OnInit {
    profile: any;
    selectedOption: string;
    rootSection = {
        title: "Modelos de Referencia",
        class: "fa fa-cube "
    }
    sections = [{
        title: "Gestión de modelos",
        route: "mr_mol",
        public: true,
        selected: this._route.snapshot.url[0].path == "mr_mol",
    },
    {
        title: "Gestión de Sistemas",
        route: "mr_sis",
        public: true,
        selected: this._route.snapshot.url[0].path == "mr_sis",
    },    
    {
        title: "Sistemas y escenarios",
        route: "mr_sis_esc",
        public: true,
        selected: this._route.snapshot.url[0].path == "mr_sis_esc",
    }, {
        title: "Umbrales oficiales",
        route: "mr_uo",
        public: true,
        selected: this._route.snapshot.url[0].path == "mr_uo",
    },
    {
        title: "Umbrales provisionales",
        route: "mr_up",
        public: true,
        selected: this._route.snapshot.url[0].path == "mr_up",
    }
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.MODELOS_REFERENCIA);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {  
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
