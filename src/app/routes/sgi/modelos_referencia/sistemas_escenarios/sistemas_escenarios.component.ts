import { Component, OnInit, ViewChild } from '@angular/core';
import { constants } from '../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { constants as constantsCommon } from '@arca/common';
import { BasicTableModel } from '@arca/common';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Component({
    selector: 'app-sistemas-escenarios-manager',
    templateUrl: './sistemas_escenarios.component.html',
    styleUrls: ['./sistemas_escenarios.component.scss'],
    providers: []
})
export class SistemasEscenariosComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    baseURLServiceCrud = environment.RESTAPI_BASE_URL + '/lab/admin';
    tablaTiposConstantes;
    @ViewChild('modalSelectVariable') modalSelectVariable: any;
    listModelos = [];
    listEscenarios = [];
    listSistemas = [];
    listNivelTemp = [];
    listIndVar = [];
    selectedVariable;
    ngOnInit() {
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.MODELOS_REFERENCIA);
        this.profile = this._auditoriaService.getUserProfile();
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.searchData();
        });
    }

    searchMasterData() {
        return new Promise((resolve, reject) => {
            var obs1 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MODELOS, filters: [], sort: "desc_decreto ASC" });
            var obs2 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.ESCENARIOS, filters: [], sort: "desc_escenario ASC" });
            var obs3 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.SISTEMAS, filters: [], sort: "desc_sistema ASC" });
            var obs4 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MODEL_NIVEL_TEMPORAL, filters: [], sort: "desc_nivel_temp ASC" });
            var obs5 = this._basicEntityService.getData({ entity: constants.SGI.ENTITIES.MODEL_INDICADOR_VARIABLE, filters: [], sort: "desc_indicador_variable ASC" });
            return forkJoin([obs1, obs2, obs3, obs4, obs5]).subscribe(([res1, res2, res3, res4, res5]) => {
                this.listModelos = res1;
                this.listEscenarios = res2;
                this.listSistemas = res3;
                this.listNivelTemp = res4;
                this.listIndVar = res5;
                resolve();
            });
        });
    }
    configureTable() {
        var tableConfiguration = {
            id: 'mr_mol',
            title: null,
            columns: [
                { id: "cod_escenario_decreto", filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "desc_decreto", filter: true, displayed: true, translation: "Modelo" },
                { id: "desc_sistema", filter: true, displayed: true, translation: "Sistema" },
                { id: "desc_escenario", filter: true, displayed: true, translation: "Escenario" },
                { id: "desc_variable", filter: true, displayed: true, translation: "Variable" },
                { id: "desc_nivel_temp", filter: true, displayed: true, translation: "N.Temp" },
                { id: "desc_indicador_variable", filter: true, displayed: true, translation: "Indicador" },
            ],
            crud: {
                title: "Alta/Edición de Sistemas / Escenarios",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.SISTEMAS_ESCENARIOS },
                fields: [
                    { row: 1, id: "cod_escenario_decreto", label: "Código", pk: true, mandatory: false, sequence : true,  bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "cod_decreto", label: "Modelo", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listModelos, code: 'cod_decreto', desc: 'desc_decreto', field_in_table: 'desc_decreto' },
                    { row: 2, id: "cod_sistema", label: "Sistema", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listSistemas, code: 'cod_sistema', desc: 'desc_sistema', field_in_table: 'desc_sistema' },
                    { row: 2, id: "cod_escenario", label: "Escenario", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listEscenarios, code: 'cod_escenario', desc: 'desc_escenario', field_in_table: 'desc_escenario' },
                    { row: 3, id: "cod_variable", label: "Variable", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.CUSTOM_ASSISTANT, code: 'cod_variable', desc: 'desc_variable', field_in_table: 'desc_variable'  },
                    { row: 3, id: "cod_nivel_temp", label: "Nivel temporal", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listNivelTemp, code: 'cod_nivel_temp', desc: 'desc_nivel_temp', field_in_table: 'desc_nivel_temp' },
                    { row: 4, id: "cod_indicador_variable", label: "Indicador de la variable", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listIndVar, code: 'cod_indicador_variable', desc: 'desc_indicador_variable', field_in_table: 'desc_indicador_variable' },


                ]
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "desc_escenario",
                direction: "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.SISTEMAS_ESCENARIOS, filters: this.filterSelection, sort: "desc_decreto ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    }
    afterExecuteCrudAction(event) {
        console.log(event);
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.searchData();
        }
        if (event.action === "openAssistant") {
            this.openVariableSelector();
        }
    }
    openVariableSelector() {
        this.modalService.open(this.modalSelectVariable, { size: 'lg', centered: true });
    }
    onSelectVariable(event){
        this.selectedVariable = event.cod_variable;
    }
    selectVariable(modal) {
        this.tablaTiposConstantes.setValueCrud("cod_variable", this.selectedVariable);
        modal.dismiss('Cross click')
    }
}
