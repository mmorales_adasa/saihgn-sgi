import { Component, OnInit } from '@angular/core';
import { constants } from '../../../../config/constants';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
@Component({
    selector: 'app-umbrales-oficiales-manager',
    templateUrl: './umbrales_oficiales.component.html',
    styleUrls: ['./umbrales_oficiales.component.scss'],
    providers: []
})
export class UmbralesOficialesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    ngOnInit() {
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.MODELOS_REFERENCIA);
        this.profile = this._auditoriaService.getUserProfile();
    }
}
