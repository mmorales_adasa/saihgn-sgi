import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from '../../../config/constants';
import { Router } from '@angular/router';
import { AuditoriaService } from '@arca/common';
@Component({
    selector: 'login-laboratorio-component',
    templateUrl: './login.component.html',
    providers: []
})
export class LoginLaboratorioComponent implements OnInit {    
    codeApp  = constants.SGI.COD_APP;
    constructor(
        private _auditoriaService: AuditoriaService,
        private translate: TranslateService,
        private _router: Router,
        private _layoutService: LayoutService
    ) {
        console.log('LoginLaboratorioComponent');
        this.translate.addLangs(['es', 'en']);
        this.translate.setDefaultLang('es');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/es|en/) ? browserLang : 'es');
    }
    changeLang(language: string) {
        this.translate.use(language);
    }
    ngOnInit() {
        this._auditoriaService.removeProfile();
        this._layoutService.setMenuSelected(constants.SGI.SECTIONS.LOGIN);
    }
    login(data) {
            this._layoutService.refreshLayoutCompoents(null);
            this._router.navigate([ constants.SGI.ROUTE_PREFIX + '/home']);
    }
}
