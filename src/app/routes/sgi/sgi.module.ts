import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { ValidacionMainComponent } from './validacion_manual/validacion.main.component';
import { ValidacionManualComponent } from './validacion_manual/validacion/val.manual.component';
import { ValidacionManualAquadam } from './validacion_manual/aquadam/val.aquadam.component';
import { VariablesCalculadasMainComponent } from './var_calculadas/var.calculadas.main.component';
import { ConfiguracionMainComponent } from './configuracion/configuracion.main.component';
import { EstacionesComponent } from './configuracion/estaciones/estaciones.component';
import { PermanenciaComponent } from './configuracion/permanencia/permanencia.component';
import { SumarizacionComponent } from './configuracion/sumarizacion/sumarizacion.component';
import { ValidacionComponent } from './configuracion/validacion/validacion.component';
import { CompresionComponent } from './configuracion/compresion/compresion.component';
import { ModelosRererenciaMainComponent } from './modelos_referencia/modelos.referencia.main.component';
import { ModelosComponent } from './modelos_referencia/modelos/modelos.component';
import { SistemasComponent } from './modelos_referencia/sistemas/sistemas.component';
import { SistemasEscenariosComponent } from './modelos_referencia/sistemas_escenarios/sistemas_escenarios.component';
import { UmbralesOficialesComponent } from './modelos_referencia/umbrales_oficiales/umbrales_oficialescomponent';
import { UmbralesProvisionalesComponent } from './modelos_referencia/umbrales_provisionales/umbrales_provisionales.component';
import { UmbralesCompuestosComponent } from './modelos_referencia/umbrales_provisionales/umbrales_compuestos/umbrales-compuestos.component';
import { ValoresUmbralComponent } from './modelos_referencia/umbrales_provisionales/valores_umbral/valores-umbral.component';
import { CurvaGastoComponent } from './curvas_gasto/curva.gasto.component';
import { ConstantesComponent } from './var_calculadas/constantes/constantes.component';
import { TiposConstantesComponent } from './var_calculadas/tipos_constantes/tipos.constantes.component';
import { VarCalculadasComponent } from './var_calculadas/var_calculadas/var_calculadas.component';
import { VarSimpleComponent } from './var_calculadas/var_simple/var_simple.component';
import { MonitorMainComponent } from './monitor/monitor.main.component';
import { MonitorProcComponent } from './monitor/procesos/monitor.proc.component';
import { MonitorOpsComponent } from './monitor/operaciones/monitor.ops.component';
import { SecurityMainComponent } from './security/security.main.component';
import { EsquemasMainComponent } from './esquemas/esquemas.main.component';
import { InformesMainComponent } from './informes/informes.main.component';
import { EsquemasComponent } from './esquemas/esquemas/esquemas.component';
import { InformesComponent } from './informes/informes/informes.component';
import { UsuariosComponent } from './security/usuarios/usuarios.component';
import { CamarasComponent } from './security/camaras/camaras.component';
import { StationManagerComponent } from './security/shared/stationManager/station.manager.component';
import { RolesComponent } from './security/roles/roles.component';
import { NivelesComponent } from './security/niveles/niveles.component';
import { ElementosComponent } from './security/elementos/elementos.component';
import { RedirectCasComponent } from './../cas/redirect.cas.component';
import { HomeLaboratorioComponent } from './home/home.component';
import { LoginLaboratorioComponent } from './login/login.component';
import { RecoverComponent } from './recover/recover.component';
import { LayoutComponent } from '../../layout/layout.component';
import { DatoBrutoService } from '../../services/arca_lab/data.bruto.service';
import { LayoutModule } from '../../layout/layout.module';
import { CasGuard } from '../../guard/cas.guard';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { MomentModule } from 'ngx-moment';
import { environment } from '../../../environments/environment';
import { constants } from '../../config/constants';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { MatExpansionModule } from '@angular/material/expansion';
import { LayoutCommonService } from '@arca/common';
import { GraphService } from '../../services/util/graph.service';
import { ArcaCommonModule } from '@arca/common';
import {
    MatFormFieldModule, MatSelectModule,
    MatInputModule, MatDatepickerModule, MatNativeDateModule,
    MatSortModule, MatTableModule, MatPaginatorModule, MatTooltipModule,
    MatCheckboxModule, MatButtonModule, MatIconModule,
    MatButtonToggleModule, MatTabsModule,
    MatMenuModule, MatSidenavModule,
} from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from '@arca/common/components/loginForm/loginForm.component';
export const routes: Routes = [
    { path: 'validateTicket', component: RedirectCasComponent },
    {
        path: '',//constants.SGI.ROUTE_PREFIX,
        component: LayoutComponent,
        data: { module: constants.SGI.COD_APP },
        children: [
             { path: '', component: HomeLaboratorioComponent, canActivate: [CasGuard], data: {  } },
             //{ path: 'login', component: LoginLaboratorioComponent, canActivate: [], data: {  } },
             { path: 'recover', component: RecoverComponent, canActivate: [], data: {  } },
             { path: 'home', component: HomeLaboratorioComponent, canActivate: [CasGuard], data: {  } },
             // Validación datos brutas
             { path: 'vm', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm', PERIODICIDAD : 'H' , RED : constants.SGI.TIPO_RED_VALIDACION.NORMAL} },
             { path: 'vm_aquadam', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_aquadam', PERIODICIDAD : 'H' , RED : constants.SGI.TIPO_RED_VALIDACION.SONDAS_MULTI } },     
             { path: 'vm_auscul', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_auscul', PERIODICIDAD : 'H'   , RED : constants.SGI.TIPO_RED_VALIDACION.AUSCULTACION} },     
             { path: 'vm_saihg', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_saihg', PERIODICIDAD : 'H'     , RED : constants.SGI.TIPO_RED_VALIDACION.SAIHG} },     
             { path: 'vm_piezo', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_piezo', PERIODICIDAD : 'H'   , RED : constants.SGI.TIPO_RED_VALIDACION.PIEZO} },     
             { path: 'vm_reacar', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_reacar', PERIODICIDAD : 'H'   , RED : constants.SGI.TIPO_RED_VALIDACION.REACAR} },     
             { path: 'vm_saica', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vm_saica', PERIODICIDAD : 'H'     , RED : constants.SGI.TIPO_RED_VALIDACION.SAICA} },     
             // Edición
             { path: 'ec', component: ValidacionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'ec', PERIODICIDAD : 'D' , RED : constants.SGI.TIPO_RED_VALIDACION.NORMAL } },
            // Configuración           
            { path: 'conf_est', component: ConfiguracionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'conf' } },
            { path: 'conf_comp', component: ConfiguracionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'conf' } },
            { path: 'conf_val', component: ConfiguracionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'conf' } },
            { path: 'conf_sum', component: ConfiguracionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'conf' } },
            { path: 'conf_per', component: ConfiguracionMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'conf' } },
            // Mantenimiento
            { path: 'mto', component: MonitorMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mto' } },
            { path: 'mto_ops', component: MonitorMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mto_ops' } },
            // Curva de gasto
            { path: 'cg', component: CurvaGastoComponent, canActivate: [CasGuard], data: {  PERMISSION: 'cg' } },
            // Modelos referencia
            { path: 'mr_mol', component: ModelosRererenciaMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mr' } },
            { path: 'mr_sis_esc', component: ModelosRererenciaMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mr' } },
            { path: 'mr_sis', component: ModelosRererenciaMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mr' } },
            { path: 'mr_uo', component: ModelosRererenciaMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mr' } },
            { path: 'mr_up', component: ModelosRererenciaMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'mr' } },
            // Variables y Variables calculadas
            { path: 'vc_tc', component: VariablesCalculadasMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vc' } },
            { path: 'vc_c', component: VariablesCalculadasMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vc' } },
            { path: 'vc_vc', component: VariablesCalculadasMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vc' } },
            { path: 'vc_vs', component: VariablesCalculadasMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'vc' } },
            // Seguridad
            { path: 'sec_users', component: SecurityMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'sec' } },
            { path: 'sec_niveles', component: SecurityMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'sec' } },
            { path: 'sec_elementos', component: SecurityMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'sec' } },
            { path: 'sec_camaras', component: SecurityMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'sec' } },
            // Esquemas
            { path: 'esquemas', component: EsquemasMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'esq' } },            
            // Informes
            { path: 'informes', component: InformesMainComponent, canActivate: [CasGuard], data: {  PERMISSION: 'esq' } },            

        ]
    },
];
@NgModule({
    imports: [
        LayoutModule,
        SharedModule,
        //RouterModule.forRoot(routes),
        RouterModule.forRoot(routes, {onSameUrlNavigation: "reload"}),
        CommonModule,
        TranslateModule,
        FormsModule,
        MomentModule,
        MatFormFieldModule, MatSelectModule,
        MatInputModule, MatDatepickerModule, MatNativeDateModule,
        MatSortModule, MatTableModule, MatPaginatorModule, MatTooltipModule,
        MatCheckboxModule, MatButtonModule,
        MatButtonToggleModule, MatTabsModule, MatExpansionModule, MatIconModule,
        MatMenuModule, MatSidenavModule,
        NgbModule, ChartsModule, NgMultiSelectDropDownModule.forRoot(),
        AngularMultiSelectModule,
        ArcaCommonModule.forRoot({
            auditoriaEndPoint: environment.RESTAPI_BASE_URL,
            laboratorioEndPoint: environment.RESTAPI_BASE_URL,
            auditaActivated: false
        })
    ],
    declarations:
        [LoginLaboratorioComponent, HomeLaboratorioComponent,RecoverComponent,
            RedirectCasComponent,
            ValidacionManualComponent, VariablesCalculadasMainComponent, 
            ConstantesComponent, TiposConstantesComponent, VarCalculadasComponent,VarSimpleComponent,
            EstacionesComponent, SumarizacionComponent, CompresionComponent, ValidacionComponent, PermanenciaComponent, ConfiguracionMainComponent,
            ModelosRererenciaMainComponent, ModelosComponent, SistemasComponent, SistemasEscenariosComponent, UmbralesOficialesComponent, UmbralesProvisionalesComponent,
            UmbralesCompuestosComponent,ValoresUmbralComponent,MonitorMainComponent,MonitorProcComponent,MonitorOpsComponent,
            CurvaGastoComponent,SecurityMainComponent, UsuariosComponent,CamarasComponent, RolesComponent, NivelesComponent, ElementosComponent,
            ValidacionMainComponent,ValidacionManualAquadam,StationManagerComponent, EsquemasMainComponent, EsquemasComponent, InformesMainComponent, InformesComponent
        ],
    exports: [
        RouterModule
    ],
    providers: [LayoutCommonService,  DatoBrutoService, GraphService ]
})

export class SGIModule {
    constructor() {
    }
}
