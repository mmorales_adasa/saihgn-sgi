import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-informes-component-main',
    templateUrl: './informes.main.component.html',
    styleUrls: ['./informes.main.component.scss'],
    providers: []
})
export class InformesMainComponent implements OnInit {
    profile: any;
    selectedOption: string = 'mesq';
    rootSection = {
        title: "Informes",
        class: "fa fa-file-pdf-o "
    }
    sections = [{
        title: "Gestión de informes",
        route: "informes",
        public: true,
        selected: this._route.snapshot.url[0].path == "informes",
    }
    ];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.INFORMES);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {  
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
