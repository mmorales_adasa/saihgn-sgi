import { Component, OnInit, Input,ViewChild } from '@angular/core';
import { constants } from './../../../../../config/constants';
import { environment } from './../../../../../../environments/environment';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
@Component({
    selector: 'app-station-manager',
    templateUrl: './station.manager.component.html',
    styleUrls: ['./station.manager.component.scss'],
    providers: []
})
export class StationManagerComponent implements OnInit {

    @Input() items : any = [];
    @Input() typeSelection;
    @Input() config : any = {
        typeSelection : 1,
        showPublic : false,
        showNivel: false,
    };

    allSelected :  boolean = false;
    constructor() {
    }    
    ngOnInit() {
        var self = this;
    }
    selectAll(evt){
        for (var i=0;i<this.items.length;i++){
            this.items[i].selected = this.allSelected;
        }
    }
}
