import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
import { toText } from '@amcharts/amcharts4/.internal/core/utils/Type';
@Component({
    selector: 'app-usuarios-manager',
    templateUrl: './usuarios.component.html',
    styleUrls: ['./usuarios.component.scss'],
    providers: []
})
export class UsuariosComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    tablaTiposConstantes;
    listNivelesAcceso = []; 
    rowSelected;      
    loadingPermisos : boolean = false;
    estacionesLoaded : boolean = false;    
    infoPermisosLoaded : boolean = false;
    infoVideosLoaded : boolean = false;
    infoInformesLoaded : boolean = false;

    infoUbicacionesLoaded : boolean = false;
    permisosElementos;
    permisosVideos;    
    permisosInformes;
    listUbicaciones = [];
    tiposEstaciones = [];
    rolesApp = [{COD : 1, DESCR : 'Registrador'}, {COD : 2, DESCR : 'Validador'}]
    msgSaving;
    configModalAsignacion = {
        typeSelection : 2,
        showPublic : true,
        showNivel : true,
        showName : true,        
    };
    configModalAsignacionPermisos = {
        typeSelection : 2,
        showPublic : true,
        showNivel : true,
        showName : false
    };  
    configModalAsignacionVideos = {
        typeSelection : 2,
        showPublic : true,
        showNivel : true,
        showName : true
    };  
    configModalAsignacionInformes = {
        typeSelection : 2,
        showPublic : true,
        showNivel : true,
        showName : true
    };     
    tableCrudFields;
    @ViewChild('modalPermisos') modalPermisos: any;
    @ViewChild('modalDuplicar') modalDuplicar: any;

    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.searchData();
            //this.configureFilters();

        });

        //this.configureTable();
            

    }

    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.NIVELES_ACCESO, filters: [], sort: "nombre ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.listNivelesAcceso = list;
                var params = { entity: constants.SGI.ENTITIES.TIPO_ESTACIONES_GENERAL, filters: [], sort: "cod_tipo_estacion ASC" }
                this._basicEntityService.getData(params).subscribe(list => {            
                    this.tiposEstaciones  = [];
                    for (var i=0;i<list.length;i++){
                        this.tiposEstaciones.push({name : list[i].descripcion,  cod : list[i].cod_tipo_estacion, list : [], selected :  []}); 
                    }
                    var params = { entity: constants.SGI.ENTITIES.BIBLIOTECA_UBICACIONES, filters: [], sort: "nombre_ubicacion ASC" }
                    this._basicEntityService.getData(params).subscribe(list => {     
                        this.listUbicaciones=list;
                        resolve(1);
                                        
                    });
                    
                });                
            });
        });
    }
    onSave(){
        
        this.savePermisosUsuario().then((list) => {
           // this.modalService.dismissAll();
        });
    }

    savePermisosUsuario(){
        this.msgSaving = 'Guardando datos en base de datos, espere por favor.';
        return new Promise((resolve, reject) => { 
            var estaciones = [];
            var permisos = [];
            var videos = [];
            var informes = [];
            for (var i=0;i<this.tiposEstaciones.length;i++){
                for (var item of this.tiposEstaciones[i].selected){    
                    if (item.selected=="0" || item.selected=="1"){
                        estaciones.push({cod  : item.cod, tipo : this.tiposEstaciones[i].cod, visible : item.selected})
                    }
                }            
            }            
            for (var i=0;i<this.permisosElementos.length;i++){
                if (this.permisosElementos[i].selected && this.permisosElementos[i].selected != -1){
                    this.permisosElementos[i].visible = this.permisosElementos[i].selected;
                        permisos.push(this.permisosElementos[i]);
                    }
            }
            for (var i=0;i<this.permisosVideos.length;i++){
                if (this.permisosVideos[i].selected && this.permisosVideos[i].selected != -1){
                    this.permisosVideos[i].visible = this.permisosVideos[i].selected;
                        videos.push(this.permisosVideos[i]);
                    }
            }
            for (var i=0;i<this.permisosInformes.length;i++){
                if (this.permisosInformes[i].selected && this.permisosInformes[i].selected != -1){
                    this.permisosInformes[i].visible = this.permisosInformes[i].selected;
                        informes.push(this.permisosInformes[i]);
                    }
            }                        
            this._securityService.savePermisosUsuario({cod_usuario : this.rowSelected.cod_usuario, 
                                                        estaciones : estaciones,
                                                        videos : videos,
                                                        informes : informes,
                                                        permisos :   permisos // this.listPermisos
                                                    }).subscribe(list => {                                
                this.msgSaving = 'Datos guardados correctamente';                                        
                resolve(1);
            });
            });
    } 

    configureTable() {
        var self = this;
        
        this.tableCrudFields = [
            { row: 1, id: "cod_usuario", label: "Código", pk: true, mandatory: false, sequence : true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
            { row: 1, id: "username",    label: "Login", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
            { row: 2, id: "password", label: "Password", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null, password : true },
            { row: 2, id: "email", label: "Email", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
            { row: 3, id: "nombre_completo", label: "Nombre completo", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
            { row: 3, id: "origen", label: "Origen", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 'db', DESCR: 'db' }, { COD: 'ldap', DESCR: 'ldap' }], code: 'COD', desc: 'DESCR', field_in_table: 'origen', defaultValue : 'db', showEmpty : true },
            { row: 4, id: "enabled", label: "Activo", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_enabled', defaultValue : '1', showEmpty : true },                    
            { row: 4, id: "enabled_sgi", label: "Acceso SGI (sólo internos)", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_enabled_sgi', defaultValue : '0', showEmpty : true },
            { row: 5, id: "cod_nivel_acceso", label: "Nivel de Acceso", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listNivelesAcceso, code: 'cod_nivel_acceso', desc: 'nombre', field_in_table: 'desc_nivel_acceso' },                    
            { row: 5, id: "enabled_app_aus", label: "Acceso APP Ausc. ", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_enabled_app_aus', defaultValue : '0', showEmpty : true },
            { row: 6, id: "usuario_telegram", label: "Id.Telegram", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null }, 
            { row: 6, id: "role_app_aus", label: "Rol APP Ausc. ", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this.rolesApp, code: 'COD', desc: 'DESCR', field_in_table: 'desc_role_app_aus', defaultValue : '0', showEmpty : true },            
            { row: 7, id: "enabled_mailing", label: "Notificaciones activadas ", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_enabled_mailing', defaultValue : '0', showEmpty : true },
             {row: 7, id: "ubicaciones", label: "Ubicaciones", pk: false, bypass: true, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, type: 
            constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'id_ubicacion', desc: 'nombre_ubicacion', 
             data: self.listUbicaciones}

            //{ row: 5, id: "enabled_visor", label: "Acceso Visor", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_enabled_visor', defaultValue : '1', showEmpty : true },                                        
            
        ];
        var tableConfiguration = {
            id: 'sec_users',
            title: null,
            columns: [
                { id: "Permisos", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Personalizar permisos", icon: "fa fa-lock", color: "green" } },
                { id: "Duplicate", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Duplicar", icon: "fa fa-clone", color: "orange" } },
                { id: "cod_usuario",  filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort : constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "username", filter: true, displayed: true, translation: "Login" },
                { id: "nombre_completo",  filter: true, displayed: true, translation: "Nombre completo" },
                { id: "origen",   filter: true, displayed: true, translation: "Origen" },
                { id: "email",   filter: true, displayed: true, translation: "Email" },
                { id: "enabled",  filter: true, displayed: false, translation: "Activo" },
                { id: "enabled_sgi",    filter: true, displayed: false, translation: "enabled_sgi" },
                { id: "role_app_aus",    filter: true, displayed: false, translation: "role_app_aus" },
                { id: "desc_enabled_sgi",  filter: true, displayed: true, translation: "Acceso SGI" },
                { id: "desc_enabled_app_aus",  filter: true, displayed: true, translation: "Acceso App Ausc" },
                { id: "desc_role_app_aus",    filter: true, displayed: true, translation: "Rol App Aus." },                
                { id: "desc_nivel_acceso", filter: true, displayed: true, translation: "Nivel Acceso" }, 
                { id: "desc_enabled",  filter: true, displayed: true, translation: "Activo" },
                 
            ],
            crud: {
                title: "Alta/Edición de Usuarios",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.USUARIOS },
                fields: this.tableCrudFields
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort :{
                active : "username",
                direction : "asc"
            }
        };

        // 
        /*if (this.tablaTiposConstantes.crudModel.rowSelected.id && this.tablaTiposConstantes.crudModel.rowSelected.origen==1){
            tableConfiguration.crud.fields.push({ row: 3, id: "origen", label: "Origen", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 'db', DESCR: 'DB' }, { COD: 'ldap', DESCR: 'LDAP' }], code: 'COD', desc: 'DESCR', field_in_table: 'origen', defaultValue : 'db', showEmpty : true });
        }*/
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }


    searchData() {
        var self =this;
        var params = { entity: constants.SGI.ENTITIES.USUARIOS, filters: this.filterSelection, sort: "username ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
        var params = { entity: constants.SGI.ENTITIES.ESTACIONES_GENERAL, filters: this.filterSelection, sort: "tipo asc, cod_estacion ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            for (var i=0;i<this.tiposEstaciones.length;i++){
                this.tiposEstaciones[i].list = list.filter(function (item) { return item.tipo == self.tiposEstaciones[i].cod });
                
            }
            self.estacionesLoaded = true;
        });        
    }
    searchPermisos(cod_usuario) {
        return new Promise((resolve, reject) => {        
            var params = { filters: [{id : "cod_usuario", type: "INTEGER", values : cod_usuario}], sort: "cod_elemento ASC"}        
        this._securityService.getPermisosUsuario(params).subscribe(list => {
            resolve(list);    
        });
        });
    }   

    searchVideos(cod_usuario) {
        return new Promise((resolve, reject) => {        
            var params = { filters: [{id : "cod_usuario", type: "INTEGER", values : cod_usuario}], sort: "cod_video ASC"}        
        this._securityService.getVideosUsuario(params).subscribe(list => {
            resolve(list);    
        });
        });
    } 
    searchInformes(cod_usuario) {
        return new Promise((resolve, reject) => {        
            var params = { filters: [{id : "cod_usuario", type: "INTEGER", values : cod_usuario}], sort: "cod_informe ASC"}        
        this._securityService.getInformesUsuario(params).subscribe(list => {
            resolve(list);    
        });
        });
    }     
    searchUbicaciones(cod_usuario) {
        return new Promise((resolve, reject) => {        
            var params = { filters: [{id : "cod_usuario", type: "INTEGER", values : cod_usuario}], sort: "cod_video ASC"}        
        this._securityService.getUbicacionesUsuario(params).subscribe(list => {
            resolve(list);    
        });
        });
    }     
    searchPermisosDataNivel(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_elemento ASC"}
        this._securityService.getPermisosDataNivel(params).subscribe(list => {
            resolve(list);    
        });
        });
        }       

    searchPermisosData(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_elemento ASC"}
        this._securityService.getPermisosDataUsuario(params).subscribe(list => {
            resolve(list);    
        });
        });


        

    }        
    beforeExecuteCrudAction(event){
        this.tablaTiposConstantes.crudModel.elementMap['ubicaciones'].value = [];
        if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.tablaTiposConstantes.crudModel.rowSelected.origen = "db";
            this.tablaTiposConstantes.crudModel.elementMap.password.validations.push("NN"); //Mandatory
            this.tablaTiposConstantes.crudModel.elementMap.password.disabled  = false;            
        }   
        else{
            
                this.searchUbicaciones(this.tablaTiposConstantes.crudModel.rowSelected.cod_usuario).then((list : any) => {
                    var enabledItems = [];
                    for (var item of list){
                        console.log(item);
                        if (item.visible){
                            enabledItems.push({'COD' : item.id_ubicacion});
                        }
                    }
                    this.tablaTiposConstantes.crudModel.elementMap['ubicaciones'].value = enabledItems;
                });

                //this.tablaTiposConstantes.crudModel.elementMap.password.disabled = (this.tablaTiposConstantes.crudModel.rowSelected.origen == "ldap");
                this.tablaTiposConstantes.crudModel.elementMap.password.validations = [];
                if (this.tablaTiposConstantes.crudModel.rowSelected.origen == "db"){
                    this.tablaTiposConstantes.crudModel.elementMap.password.validations.push("NN"); //Mandatory
                    this.tablaTiposConstantes.crudModel.elementMap.password.disabled  = false;
                }
                else{
                    this.tablaTiposConstantes.crudModel.elementMap.password.disabled  = true;
                }
                //this.tablaTiposConstantes.crudModel.elementMap.password.mandatory = !(this.tablaTiposConstantes.crudModel.rowSelected.origen == "ldap");
        }
    }
    changeCrudValue(event){        
        if (event.data.elementId=="origen"){
            this.tablaTiposConstantes.crudModel.elementMap.password.validations = [];
            var value = event.data.event.target.options[event.data.event.target.options.selectedIndex].text;
            if (value=="ldap"){
                this.tablaTiposConstantes.crudModel.elementMap.password.disabled  = true;
            }
            else{
                this.tablaTiposConstantes.crudModel.elementMap.password.disabled  = false;
                this.tablaTiposConstantes.crudModel.elementMap.password.validations.push("NN"); //Mandatory
            }
        }

        
    }
    afterExecuteCrudAction(event) {
        var params = {cod_usuario : event.data.cod_usuario,  ubicaciones : event.data.ubicaciones };
        console.log(params);
        this._securityService.savePermisosUbicaciones(params).subscribe(list => {});

        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
            this.searchData();
        }
    }

    onExecuteTableRowUtilityAction(event) {      
        var self = this;  
        this.infoPermisosLoaded = false;
        this.infoVideosLoaded = false;
        this.infoUbicacionesLoaded = false;
        this.rowSelected = event.row;
        this.permisosElementos = null;
        
        
        if (event.action == "Permisos") {
            this.loadingPermisos=true;
            //this.searchPermisosDataNivel(this.rowSelected.cod_nivel_acceso).then((permisosDataNivel : any) => { 
            //    this.searchPermisosDataNivel(0).then((permisosDataNivelPublico : any) => { 

                this.searchVideos(this.rowSelected.cod_usuario).then((permisosVideos : any) => {
                    this.loadingPermisos=false;
                   // this.listPermisos = list;
                    this.permisosVideos = []; 
                    for (var item of permisosVideos){
                        var item2 : any = {
                            cod : item.cod_video,
                            nombre : item.descripcion                        
                        }; 
                        item2.selected =  item.visible==1 || item.visible==0 ? item.visible : -1;
                        item2.visible_public = !item.visible_public ? 0 : 1;
                        item2.visible_nivel = (item.visible_nivel==0 || item.visible_nivel==1)  ? item.visible_nivel : null;
                        this.permisosVideos.push(item2);
                    }
                    this.infoVideosLoaded = true;
                
                });
                this.searchInformes(this.rowSelected.cod_usuario).then((permisosInformes : any) => {
                    this.loadingPermisos=false;
                   // this.listPermisos = list;
                    this.permisosInformes = []; 
                    for (var item of permisosInformes){
                        var item2 : any = {
                            cod : item.cod_informe,
                            nombre : item.descripcion                        
                        }; 
                        item2.selected =  item.visible==1 || item.visible==0 ? item.visible : -1;
                        item2.visible_public = !item.visible_public ? 0 : 1;
                        item2.visible_nivel = (item.visible_nivel==0 || item.visible_nivel==1)  ? item.visible_nivel : null;
                        this.permisosInformes.push(item2);
                    }
                    this.infoInformesLoaded = true;
                
                });                



            this.searchPermisos(this.rowSelected.cod_usuario).then((permisosElementos : any) => {
                this.loadingPermisos=false;
               // this.listPermisos = list;
                this.permisosElementos = []; 
                for (var item of permisosElementos){
                    var item2 : any = {
                        cod : item.cod_elemento,
                        nombre : item.cod_elemento                        
                    }; 
                    item2.selected =  item.visible==1 || item.visible==0 ? item.visible : -1;
                    item2.visible_public = !item.visible_public ? 0 : 1;
                    item2.visible_nivel = (item.visible_nivel==0 || item.visible_nivel==1)  ? item.visible_nivel : null;
                    this.permisosElementos.push(item2);
                }
                this.searchPermisosData(this.rowSelected.cod_usuario).then((permisosData : any) => {
                    //console.log(permisosData);
                    console.log(self.tiposEstaciones);
                    for (var i=0;i<self.tiposEstaciones.length;i++){
                        /*var aux = permisosDataNivel.filter(function (item) { return item.cod_tipo_item == self.tiposEstaciones[i].cod });
                        var codes = [];
                            for (var item of aux){ 
                                if (item.visible==1){
                                    codes.push(item.cod_item.trim())
                                }
                            }               
                            var aux2 = permisosDataNivelPublico.filter(function (item) { return item.cod_tipo_item == self.tiposEstaciones[i].cod });
                            var codes2 = [];
                                for (var item of aux2){ 
                                    if (item.visible==1){
                                        codes2.push(item.cod_item.trim())
                                    }
                                }       */                                                     
                        //var aux = permisosData.filter(function (item) { return item.cod_tipo_item == self.tiposEstaciones[i].cod });                        
                        // Obtenemos los seleccionados
                        console.log(self.tiposEstaciones[i].cod);
                        self.tiposEstaciones[i].selected=[];                        
                        for (var item of self.tiposEstaciones[i].list){  // Para cada elemento de la lista                          
                            
                            var aux = permisosData.find(function (p) { return p.cod_item.trim() == item.cod_estacion.trim() && 
                                                                                p.cod_tipo_item.trim() == self.tiposEstaciones[i].cod.trim() });
                            
                            if (!aux){
                                console.log('no encontrado item ' + item.cod_estacion);
                            }
                            
                            var item2 : any = {
                                cod : item.cod_estacion.trim(),
                                nombre : item.nombre,
                                visible_public :  aux ? (aux.visible_public ? 1 : 0) : '--',
                                visible_nivel  :  aux ? (aux.visible_nivel==1 || aux.visible_nivel== 0 ? aux.visible_nivel : -1) : '--',
                                selected :        aux && aux.visible==1 || aux.visible== 0 ? aux.visible : -1
                            }; 
                            //var f = permisosData.find(function (item3) { return item3.cod_item.trim() == item2.cod });                        
                            
                            //item2.selected = f ? f.visible : -1;
                            this.tiposEstaciones[i].selected.push(item2);
                            
                                                       
                        }
                    }
                    self.infoPermisosLoaded = true;                    
               // });     
            //});
            });      
        });      
            this.msgSaving = null;
            this.modalService.open(this.modalPermisos, { size: 'lg', centered: true });
        }
        else if (event.action == "Duplicate") {
            this.modalService.open(this.modalDuplicar, { size: 'sm', centered: true });
        }        
    }

    getDescriptionVisible(value){
        if (value==1){
            return "Visible";
        }
        else if (value==0){
            return "No visible";
        }
        else{
            return "Heredar";
        }
        
    }
    closeModal() {
        this.modalService.dismissAll();
    }

    onDuplicate(cod_usuario){
        return new Promise((resolve, reject) => {        
            var params = {"cod_usuario" : cod_usuario}        
            this._securityService.duplicateUser(params).subscribe(list => {                                
                this.modalService.dismissAll();
                this.searchData();

            });
        });        
    }

}
