import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
@Component({
    selector: 'app-niveles-manager',
    templateUrl: './niveles.component.html',
    styleUrls: ['./niveles.component.scss'],
    providers: []
})
export class NivelesComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    tablaTiposConstantes;
    listNivelesAcceso = [];  
    estacionesLoaded : boolean = false;    
    rowSelected;  
    msgSaving;
    configModalAsignacion = {
        typeSelection : 1,
        showPublic : false,
        showNivel : false,
        showName : true,        
    };
    configModalAsignacionPermisos = {
        typeSelection : 2,
        showPublic : false,
        showNivel : false,
        showName : false
    };    
    configModalAsignacionVideos = {
        typeSelection : 2,
        showPublic : false,
        showNivel : false,
        showName : true
    }; 

    configModalAsignacionInformes = {
        typeSelection : 2,
        showPublic : false,
        showNivel : false,
        showName : true
    };        
    
    tiposEstaciones = [];
    permisosElementos;
    permisosVideos;
    permisosInformes;
    @ViewChild('modalPermisos') modalPermisos: any;
    @ViewChild('modalDuplicar') modalDuplicar: any;
    ngOnInit() {
        this.searchMasterData().then((resp) => {
        this.configureTable();
            this.searchData();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.TIPO_ESTACIONES_GENERAL, filters: [], sort: "cod_tipo_estacion ASC" }
            this._basicEntityService.getData(params).subscribe(list => {            
                this.tiposEstaciones  = [];
                for (var i=0;i<list.length;i++){
                    this.tiposEstaciones.push({name : list[i].descripcion,  cod : list[i].cod_tipo_estacion, list : [], selected :  []}); 
                }
                resolve(1);
            });
        });
    }

    configureTable() {
        var tableConfiguration = {
            id: 'sec_niveles',
            title: null,
            columns: [
                { id: "Permisos", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Administrar permisos", icon: "fa fa-lock", color: "green" } },
                { id: "Duplicate", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Duplicar", icon: "fa fa-clone", color: "orange" } },
                { id: "cod_nivel_acceso",  filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort : constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "nombre", filter: true, displayed: true, translation: "Nivel Acceso" }                 
            ],
            crud: {
                title: "Alta/Edición de Niveles de Acceso",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.NIVELES_ACCESO },
                fields: [
                    { row: 1, id: "cod_nivel_acceso", label: "Código", pk: true, mandatory: false, sequence : true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "nombre",    label: "Nombre", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null }
                ]
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort :{
                active : "cod_nivel_acceso",
                direction : "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var self = this;
        var params = { entity: constants.SGI.ENTITIES.NIVELES_ACCESO, filters: this.filterSelection, sort: "nombre ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
        var params = { entity: constants.SGI.ENTITIES.ESTACIONES_GENERAL, filters: this.filterSelection, sort: "tipo asc, cod_estacion ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            for (var i=0;i<this.tiposEstaciones.length;i++){
                this.tiposEstaciones[i].list = list.filter(function (item) { return item.tipo == self.tiposEstaciones[i].cod });
                
            }
            this.estacionesLoaded = true;
        });

    } 
    searchPermisosNivel(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_elemento ASC"}
        this._securityService.getPermisosNivel(params).subscribe(list => {
            resolve(list);    
        });
        });
    } 

    searchVideosNivel(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_video ASC"}
        this._securityService.getVideosNivel(params).subscribe(list => {
            resolve(list);    
        });
        });
    }  
    searchInformesNivel(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_informe ASC"}
        this._securityService.getInformesNivel(params).subscribe(list => {
            resolve(list);    
        });
        });
    }           
    searchPermisosData(cod_nivel_acceso) {
        return new Promise((resolve, reject) => {
        var params = { filters: [{id : "cod_nivel_acceso", type: "INTEGER", values : cod_nivel_acceso}], sort: "cod_elemento ASC"}
        this._securityService.getPermisosDataNivel(params).subscribe(list => {
            resolve(list);    
        });
        });
    }    
    afterExecuteCrudAction(event) {
        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
            this.searchData();
        }
    }

    onExecuteTableRowUtilityAction(event) {        
        var self = this;
        this.permisosElementos = null;
        this.permisosVideos = null;
        this.permisosInformes = null;
        this.rowSelected = event.row;
        if (event.action == "Permisos") {
            this.searchVideosNivel(this.rowSelected.cod_nivel_acceso).then((permisosVideos : any) => {
                this.permisosVideos = []; 
                for (var item of permisosVideos){
                    var item2 : any = {
                        cod : item.cod_video,
                        nombre : item.descripcion
                                                  
                    }; 
                    if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                        this.configModalAsignacionVideos.typeSelection = 1;
                        this.configModalAsignacionVideos.showPublic = false;
                        item2.selected = !item.visible ? 0 : item.visible; 
                    }
                    else{
                        this.configModalAsignacionVideos.typeSelection = 2;
                        this.configModalAsignacionVideos.showPublic = true;
                        item2.selected = (item.visible && item.visible!=-1) ? item.visible : -1;
                        item2.visible_public = !item.visible_public ? 0 : 1;
                    }
                    this.permisosVideos.push(item2);
                } 
            });   
            
            this.searchInformesNivel(this.rowSelected.cod_nivel_acceso).then((permisosInformes : any) => {
                this.permisosInformes = []; 
                for (var item of permisosInformes){
                    var item2 : any = {
                        cod : item.cod_informe,
                        nombre : item.descripcion
                                                  
                    }; 
                    if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                        this.configModalAsignacionInformes.typeSelection = 1;
                        this.configModalAsignacionInformes.showPublic = false;
                        item2.selected = !item.visible ? 0 : item.visible; 
                    }
                    else{
                        this.configModalAsignacionInformes.typeSelection = 2;
                        this.configModalAsignacionInformes.showPublic = true;
                        item2.selected = (item.visible && item.visible!=-1) ? item.visible : -1;
                        item2.visible_public = !item.visible_public ? 0 : 1;
                    }
                    this.permisosInformes.push(item2);
                    console.log(this.permisosInformes);
                } 
            });  
            
                

            this.searchPermisosNivel(this.rowSelected.cod_nivel_acceso).then((permisosElementos : any) => {
                this.permisosElementos = []; 
                for (var item of permisosElementos){
                    var item2 : any = {
                        cod : item.cod_elemento,
                        nombre : item.cod_elemento                        
                    }; 
                    if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                        this.configModalAsignacionPermisos.typeSelection = 1;
                        this.configModalAsignacionPermisos.showPublic = false;
                        item2.selected = !item.visible ? 0 : item.visible; 
                    }
                    else{
                        this.configModalAsignacionPermisos.typeSelection = 2;
                        this.configModalAsignacionPermisos.showPublic = true;
                        item2.selected = (item.visible && item.visible!=-1) ? item.visible : -1;
                        item2.visible_public = !item.visible_public ? 0 : 1;
                    }
                    this.permisosElementos.push(item2);
                }

                this.searchPermisosData(this.rowSelected.cod_nivel_acceso).then((permisosData : any) => {
                    for (var i=0;i<self.tiposEstaciones.length;i++){
                        // Obtenemos los seleccionados
                        var aux = permisosData.filter(function (item) { return item.cod_tipo_item == self.tiposEstaciones[i].cod });                        
                        var codes = [];
                        if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){ // Visible o no
                            for (var item of aux){ 
                                if (item.visible==1){
                                    codes.push(item.cod_item.trim())
                                }
                            }                    
                        }
                        self.tiposEstaciones[i].selected=[];                        
                        for (var item of self.tiposEstaciones[i].list){  // Para cada elemento de la lista                          
                            var item2 : any = {
                                cod : item.cod_estacion.trim(),
                                nombre : item.nombre,
                                visible_public : item.visible_public,
                                selected : null
                            }; 
                            if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                                item2.selected = codes.includes(item.cod_estacion.trim());
                            }
                            else{
                                var f = permisosData.find(function (item3) { return item3.cod_item.trim() == item2.cod });                        
                                item2.selected = f ? f.visible : -1;
                            }
                            this.tiposEstaciones[i].selected.push(item2);                             
                        }
                    }
                    this.configModalAsignacion.typeSelection = 1;
                    this.configModalAsignacion.showPublic = false;
                    if (self.rowSelected.cod_nivel_acceso!=0){
                        this.configModalAsignacion.typeSelection=2;
                        this.configModalAsignacion.showPublic = true;
                    }                                        
                })                
            });
            this.msgSaving = null;
            this.modalService.open(this.modalPermisos, { size: 'lg', centered: true });            
        }
        else if (event.action == "Duplicate") {
            this.modalService.open(this.modalDuplicar, { size: 'sm', centered: true });

        }
    }

    closeModal() {
        this.modalService.dismissAll();
    }

    onDuplicate(cod_nivel_acceso){
        return new Promise((resolve, reject) => {        
            var params = {"cod_nivel_acceso" : cod_nivel_acceso}        
            this._securityService.duplicateNivelAcceso(params).subscribe(list => {                                
                this.modalService.dismissAll();
                this.searchData();

            });
        });        
    }   
    onSavePermisos(){
        this.savePermisosNivel().then((list) => {
           // this.modalService.dismissAll();
        });
    }    
    savePermisosNivel(){
        this.msgSaving = 'Guardando datos en base de datos, espere por favor.';
        return new Promise((resolve, reject) => { 
            var estaciones = [];
            var permisos = [];
            var videos  = [];
            var informes  = [];
            for (var i=0;i<this.tiposEstaciones.length;i++){
                for (var item of this.tiposEstaciones[i].selected){    
                    if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){ // Selected or not selected
                        if (item.selected){
                            estaciones.push({cod  : item.cod, tipo : this.tiposEstaciones[i].cod, visible : 1})
                        }
                    }
                    else{
                        if (item.selected=="0" || item.selected=="1"){
                            estaciones.push({cod  : item.cod, tipo : this.tiposEstaciones[i].cod, visible : item.selected})
                        }
                    }
                }            
            }            
            for (var i=0;i<this.permisosElementos.length;i++){
                if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                    // No vamos a meter los que son 0
                    if (this.permisosElementos[i].selected==1){
                        this.permisosElementos[i].visible = 1;
                        permisos.push(this.permisosElementos[i]);
                    }                    
                }else{
                    // No vamos a meter los que son -1
                    if (this.permisosElementos[i].selected && this.permisosElementos[i].selected != -1){
                        this.permisosElementos[i].visible = this.permisosElementos[i].selected;
                        permisos.push(this.permisosElementos[i]);
                    }
                }
            }
            for (var i=0;i<this.permisosVideos.length;i++){
                if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                    // No vamos a meter los que son 0
                    if (this.permisosVideos[i].selected==1){
                        this.permisosVideos[i].visible = 1;
                        videos.push(this.permisosVideos[i]);
                    }                    
                }else{
                    // No vamos a meter los que son -1
                    if (this.permisosVideos[i].selected && this.permisosVideos[i].selected != -1){
                        this.permisosVideos[i].visible = this.permisosVideos[i].selected;
                        videos.push(this.permisosVideos[i]);
                    }
                }
            }            
            // Informes
            for (var i=0;i<this.permisosInformes.length;i++){
                if (this.rowSelected.cod_nivel_acceso==constants.SGI.NIVEL_ACCESO_PUBLIC){
                    // No vamos a meter los que son 0
                    if (this.permisosInformes[i].selected==1){
                        this.permisosInformes[i].visible = 1;
                        informes.push(this.permisosInformes[i]);
                    }                    
                }else{
                    // No vamos a meter los que son -1
                    if (this.permisosInformes[i].selected && this.permisosInformes[i].selected != -1){
                        this.permisosInformes[i].visible = this.permisosInformes[i].selected;
                        informes.push(this.permisosInformes[i]);
                    }
                }
            }            
            this._securityService.savePermisosNivel({cod_nivel_acceso : this.rowSelected.cod_nivel_acceso, 
                                                        estaciones : estaciones,
                                                        videos : videos,
                                                        informes : informes,
                                                        permisos :   permisos // this.listPermisos
                                                    }).subscribe(list => {
                                
                this.msgSaving = 'Datos guardados correctamente';                                        
                resolve(1);
            });
            });
    } 

}
