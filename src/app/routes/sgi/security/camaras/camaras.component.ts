import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
import { toText } from '@amcharts/amcharts4/.internal/core/utils/Type';
import { BasicFilter,  BasicTableUtility  } from '@arca/common';

@Component({
    selector: 'app-camaras-manager',
    templateUrl: './camaras.component.html',
    styleUrls: ['./camaras.component.scss'],
    providers: []
})
export class CamarasComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    tablaTiposConstantes;
    listNivelesAcceso = []; 
    rowSelected;      
    loadingPermisos : boolean = false;
    msgSaving;
    tableCrudFields;
    filter;

    ngOnInit() {
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
            this.searchData();
        });

    }

    searchMasterData() {
        var self = this;
        return new Promise((resolve, reject) => {
            var params = { entity: constants.SGI.ENTITIES.NIVELES_ACCESO, filters: [], sort: "cod_nivel_acceso ASC" }
            this._basicEntityService.getData(params).subscribe(list => {                
                //this.listNivelesAcceso= self._utilService.convertToListForArray(list, "cod_nivel_acceso", "nombre");
                this.listNivelesAcceso = list;
                resolve();
            });
        });
    }
    onSave(){
        //this.savePermisosUsuario().then((list) => {
        //});
    }
    configureFilters() {
        var self;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.considerEmptyError = false;
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD,
            group: 1,
            width: 200,
            id: 't1.cod_video',
            label: 'Código',
            defaultValue: null,
        });        
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT,
            group: 1,
            width: 400,
            id: 'cod_nivel_acceso',
            label: 'Nivel acceso',
            data: this.listNivelesAcceso,
            code: 'cod_nivel_acceso',
            desc: 'nombre',
            searchOption: true
        });
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        if (this.tablaTiposConstantes.paginator){
            this.tablaTiposConstantes.paginator.pageIndex = 0;
        }
        this.searchData();
    }
    configureTable() {
        console.log(this.listNivelesAcceso);
        var self = this;
        this.tableCrudFields = [
            { row: 1, id: "cod_video", label: "Código", pk: true, mandatory: true, sequence : false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 20, defaultValue: null },
            { row: 1, id: "modelo", label: "Modelo", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 50, defaultValue: null },
            { row: 2, id: "descripcion",    label: "Descripción", pk: false, mandatory: true, bsWidth: 12, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 150, defaultValue: null },
            { row: 3, id: "ip", label: "IP", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 15, defaultValue: null, password : false },
            { row: 3, id: "mac", label: "mac", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 17, defaultValue: null, password : false },
            { row: 4, id: "timelapse", label: "Timelapse", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 0, DESCR: 'No' }, { COD: 1, DESCR: 'Sí' }], code: 'COD', desc: 'DESCR', showEmpty : false, defaultValue :0 },
            { row: 4, id: "destacado", label: "Destacado", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 0, DESCR: 'No' }, { COD: 1, DESCR: 'Sí' }], code: 'COD', desc: 'DESCR',  showEmpty : false, defaultValue : 0 },
            {row: 5, id: "niveles", label: "Niveles", pk: false, bypass: true, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_FILTER_ARRAY_STRING, type: 
            constantsCommon.TYPE_COMPONENT_FILTER.MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'cod_nivel_acceso', desc: 'nombre', 
             data: self.listNivelesAcceso}
        ];
        var tableConfiguration = {
            id: 'sec_users',
            title: null,
            columns: [
                { id: "cod_video",  filter: true, displayed: true,  translation: "Código", primaryKey: true, typeSort : constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT },
                { id: "descripcion", filter: true, displayed: true, translation: "Descricion" },
                { id: "ip",  filter: true, displayed: true, translation: "IP" },
                { id: "modelo",   filter: true, displayed: true, translation: "Modelo" },
                { id: "timelapse",   filter: true, displayed: true, translation: "Timelapse" },
                { id: "destacado",   filter: true, displayed: true, translation: "Destacado" },
                 
            ],
            crud: {
                title: "Alta/Edición de Cámaras",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.CAMARAS },
                fields: this.tableCrudFields
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort :{
                active : "cod_video",
                direction : "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var self =this;
        var params = { entity: constants.SGI.ENTITIES.CAMARAS, filters: this.filterSelection, sort: "cod_video ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });   
    }


    searchPermisosCamara(cod_video) {
        return new Promise((resolve, reject) => { 
        var params = { filters: [{id : "cod_video", type: "TEXT", values : cod_video}], sort: "cod_video ASC"}
        this._securityService.getPermisosVideo(params).subscribe(list => {
            resolve(list);    
        });
        });
    }  

    beforeExecuteCrudAction(event){
        //console.log(event);
        console.log(this.tablaTiposConstantes);
        this.tablaTiposConstantes.crudModel.elementMap['niveles'].value = [];
        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
           // Esas 3 líneas son para que el código en la inserción salga enabled
           this.tablaTiposConstantes.crudModel.elementMap.cod_video.disabled  = false;   
           this.tablaTiposConstantes.crudModel.elementMap.cod_video.insertDisabled  = false;   
           this.tablaTiposConstantes.crudModel.elementMap.cod_video.sequence  = false;   
          
        }
        else{
            this.searchPermisosCamara(this.tablaTiposConstantes.crudModel.rowSelected.cod_video).then((list : any) => {
                var enabledItems = [];
                for (var item of list){
                    if (item.visible){
                    enabledItems.push({'COD' : item.cod_nivel_acceso});
                    }
                }
                this.tablaTiposConstantes.crudModel.elementMap['niveles'].value = enabledItems;
            });
        }
    }

    
    afterExecuteCrudAction(event) {
        var params = {cod_video : event.data.cod_video,  niveles : event.data.niveles };
        this._securityService.savePermisosVideo(params).subscribe(list => {});

        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
            this.searchData();
        }

    }

    updateSecurity(){

    }

    getDescriptionVisible(value){
        if (value==1){
            return "Visible";
        }
        else if (value==0){
            return "No visible";
        }
        else{
            return "Heredar";
        }
        
    }
    closeModal() {
        this.modalService.dismissAll();
    }



}
