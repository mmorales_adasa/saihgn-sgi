import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { constants } from './../../../../config/constants';
import { environment } from './../../../../../environments/environment';
import { SGIListBaseComponent } from '../../../../shared/components/sgi.list.base.component';
import { BasicTableModel } from '@arca/common';
import { constants as constantsCommon } from '@arca/common';
@Component({
    selector: 'app-elementos-manager',
    templateUrl: './elementos.component.html',
    styleUrls: ['./elementos.component.scss'],
    providers: []
})
export class ElementosComponent extends SGIListBaseComponent implements OnInit {
    profile: any;
    filterOptions: any = { sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }] };
    filterSelection = {};
    tablaTiposConstantes;
    listPermisosNivel : any = [];  
    rowSelected;  
    loadingPermisos : boolean = false;

    @ViewChild('modalPermisos') modalPermisos: any;

    ngOnInit() {
        this.configureTable();
        this.searchData();
    }


    configureTable() {
        var tableConfiguration = {
            id: 'sec_elementos',
            title: null,
            columns: [
               // { id: "Permisos", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Administrar permisos", icon: "fa fa-lock", color: "green" } },
                { id: "cod_elemento",  filter: true, displayed: true, translation: "Código", primaryKey: true, typeSort : constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "notes", filter: true, displayed: true, translation: "Notas" },
               // { id: "visible",  filter: true, displayed: true, translation: "Visible" },
                { id: "app",  filter: true, displayed: true, translation: "App" },
               // { id: "desc_visible",  filter: true, displayed: true, translation: "Visible por defecto" }                 
            ],
            crud: {
                title: "Alta/Edición de Elementos del visor",
                editable: true,
                deletable: true,
                insertable: true,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.ELEMENTOS_VISOR },
                fields: [
                    { row: 1, id: "cod_elemento", label: "Código", pk: true, mandatory: false, sequence : false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "notes",    label: "Notas", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 100, defaultValue: null },
                   // { row: 2, id: "visible", label: "Visible por defecto", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : this._staticListService.getYesNoList(), code: 'COD', desc: 'DESCR', field_in_table: 'desc_visible', defaultValue : '1', showEmpty : true },
                    { row: 2, id: "app", label: "App", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,data : [{ COD: 'VISOR', DESCR: 'Visor' }, { COD: 'SGI', DESCR: 'SGI' }], code: 'COD', desc: 'DESCR', field_in_table: 'app', defaultValue : 'VISOR', showEmpty : true },


                ]
            },
            selectable: false,
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort :{
                active : "cod_elemento",
                direction : "asc"
            }
        };
        this.tablaTiposConstantes = new BasicTableModel(tableConfiguration);

    }
    searchData() {
        var params = { entity: constants.SGI.ENTITIES.ELEMENTOS_VISOR, filters: this.filterSelection, sort: "cod_elemento ASC" }
        this.tablaTiposConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaTiposConstantes.setDataAndLoadTable(list);
        });
    } 
    onSave(){
        this.savePermisosElementoNivel().then((list) => {
            this.modalService.dismissAll();
        });
    }
    savePermisosElementoNivel(){
        return new Promise((resolve, reject) => { 
        this._securityService.savePermisosElementoNivel(this.listPermisosNivel).subscribe(list => {
            resolve();
        });
        });
    }
    searchPermisosElemento(cod_elemento) {
        return new Promise((resolve, reject) => { 
        var params = { filters: [{id : "cod_elemento", type: "TEXT", values : cod_elemento}], sort: "cod_elemento ASC"}
        this._securityService.getPermisosElemento(params).subscribe(list => {
            resolve(list);    
        });
        });
    }    
    afterExecuteCrudAction(event) {
        if (event.action===constantsCommon.BASIC_CRUD_MODE.INSERT){
            this.searchData();
        }
    }

    onExecuteTableRowUtilityAction(event) {        
        this.rowSelected = event.row;
        if (event.action == "Permisos") {
            //alert(this.rowSelected.cod_nivel_acceso);
            this.loadingPermisos = true;
            this.searchPermisosElemento(this.rowSelected.cod_elemento).then((list) => {
                this.listPermisosNivel = list;
                this.loadingPermisos = false;                
            })            
            this.modalService.open(this.modalPermisos, { size: 'sm', centered: true });
        }

    }

    closeModal() {
        this.modalService.dismissAll();
    }

}
