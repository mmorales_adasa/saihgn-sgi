import { Component, OnInit, Input } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-security-main',
    templateUrl: './security.main.component.html',
    styleUrls: ['./security.main.component.scss'],
    providers: []
})
export class SecurityMainComponent implements OnInit {
    profile: any;
    selectedOption: string;
    rootSection = {
        title: "Seguridad",
        class: "fa fa-key "
    }
    
    sections = [        
        {
        title: "Usuarios",
        route: "sec_users",
        public: true,
        selected: this._route.snapshot.url[0].path == "sec_users",
    },
    {
        title: "Niveles de acceso",
        route: "sec_niveles",
        public: true,
        selected: this._route.snapshot.url[0].path == "sec_niveles",
    },
    {
        title: "Elementos del visor",
        route: "sec_elementos",
        public: true,
        selected: this._route.snapshot.url[0].path == "sec_elementos",
    },
    {
        title: "Cámaras",
        route: "sec_camaras",
        public: true,
        selected: this._route.snapshot.url[0].path == "sec_camaras",
    }    
];
    constructor(public _auditoriaService: AuditoriaService, private layoutService: LayoutService, private _router: Router, public _route: ActivatedRoute) {
    }
    ngOnInit() {
        this.selectedOption = this._route.snapshot.url[0].path;
        this.layoutService.setMenuSelected(constants.SGI.SECTIONS.USUARIOS);
        this.profile = this._auditoriaService.getUserProfile();

    }
    selectOption(option) {
        this._router.navigate([constants.SGI.ROUTE_PREFIX + '/' + option]);
    }
}
