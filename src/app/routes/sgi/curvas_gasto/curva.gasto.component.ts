import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AuditoriaService } from '@arca/common';
import { LayoutService } from './../../../layout/layout.service';
import { constants } from './../../../config/constants';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SGIListBaseComponent } from '../../../shared/components/sgi.list.base.component';
import { CurvaGastoService } from '../../../services/arca_lab/curv.gasto.service';
import { constants as constantsCommon } from '@arca/common';
import { environment } from './../../../../environments/environment';
import { BasicFilter, BasicTableModel, BasicTableUtility } from '@arca/common';
import moment from 'moment';
import { ParserService } from 'src/app/services/arca_lab/parser.service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { MAT_DATE_LOCALE } from '@angular/material/core'
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
import * as xml2js from 'xml2js';
import { Chart } from 'chart.js';
import value from '*.json';

@Component({
    selector: 'app-curva-gasto-main',
    templateUrl: './curva.gasto.component.html',
    styleUrls: ['./curva.gasto.component.scss'],
    providers: [CurvaGastoService]

})

export class CurvaGastoComponent extends SGIListBaseComponent implements OnInit {
    rootSection = {
        title: "Curvas de gasto",
        class: "fa fa-line-chart "
    }
    sections = [
    ];

    profile: any;
    filter;
    sort: string = "desc_variable ASC";
    filterSelection = [];
    tablaConstantes;
    offset: number = 0;
    lisTipoVariable = [];
    formulaEdition = "";
    variableSelected;
    // typeVariableSelected = "";
    visibleSelectorVariable: boolean = false;
    @ViewChild('modalRecalculo') modalRecalculo: any;
    @ViewChild('modalValores') modalValores: any;
    @ViewChild('modalImportXML') modalImportXML: any;
    msgErrorImport;
    visiblePanel = false;
    fileToUpload: File = null;

    dataTree: any = [];
    txtSearchTree;
    txtErroresFormula = "";
    txtFormulaTranslation = "";
    variableRecalculo;
    fechaIniRecalculo;
    errorFechaIniRecalculo;
    fechaFinRecalculo;
    errorFechaFinRecalculo;
    confTree: any = { isSelectable: false };
    loadingTree: boolean = false;
    recalculando: boolean = false;
    cod_variable;
    tokensFormula;
    nodos: any = {};
    listActivos = [];
    importUtility = new BasicTableUtility({
        id: "IMPORT",
        tooltip: "Alta de curva de gasto",
        faIcon: " fa-plus",
        innerHtml: null
    });
    curvaGastoService;
    valuesCurva;
    rowSelected;
    imported = false;
    dataImport: any = {};
    public chart = Chart;
    public context: CanvasRenderingContext2D;
    public chart2 = Chart;
    public context2: CanvasRenderingContext2D;
    ngOnInit() {
        this.curvaGastoService = this._injector.get(CurvaGastoService);
        this.searchMasterData().then((resp) => {
            this.configureTable();
            this.configureFilters();
        });
    }
    searchMasterData() {
        return new Promise((resolve, reject) => {
            this.listActivos = [{ cod: "S", desc: "Sí" }, { cod: "N", desc: "No" },]
            var params = { entity: constants.SGI.ENTITIES.TIPOS_VARIABLE, filters: [], sort: "desc_tipo_variable ASC" }
            this._basicEntityService.getData(params).subscribe(list => {
                this.lisTipoVariable = list;
                resolve();
            });
        });
    }
    getNextId() {
        var self = this;
    }
    searchData(pagination) {
        var params = { entity: constants.SGI.ENTITIES.CURVAS_GASTO, filters: this.filterSelection, sort: "desc_curva ASC" }
        this.tablaConstantes.startLoading();
        this._basicEntityService.getData(params).subscribe(list => {
            this.tablaConstantes.setDataAndLoadTable(list);
        });
    }
    configureTable() {
        var self = this;
        var tableConfiguration = {
            id: 'vc_c',
            title: null,
            columns: [
                { id: "Recalcular", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Especificar cálculo", icon: "fa fa-calendar" } },
                { id: "ExportCurva", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Exportar curva", icon: "fa fa-download", color: "green" } },
                { id: "ViewCurva", filter: true, displayed: true, translation: "", type: 'ROWUTILITY', attributes: { tooltip: "Ver valores de curva", icon: "fa fa-eye", color: "blue" } },
                { id: "cod_curva", filter: true, displayed: true, translation: "Cód. Curva", primaryKey: true, typeSort: constantsCommon.TYPE_DATA_FILTER.TYPE_INTEGER },
                { id: "data_alta_format", filter: true, displayed: true, translation: "F.Alta" },
                { id: "ind_activa", filter: true, displayed: false, translation: "Activa" },
                { id: "desc_activa", filter: true, displayed: true, translation: "Activa" },
                { id: "cod_estacion", filter: true, displayed: true, translation: "Cod. Estación" },
                { id: "desc_estacion", filter: true, displayed: true, translation: "Descripción Estación" },
                { id: "cod_variable_nivel", filter: true, displayed: true, translation: "Cod. Variable medida" },
                { id: "desc_variable_nivel", filter: true, displayed: true, translation: "Variable medida" },
                { id: "cod_variable_caudal", filter: true, displayed: true, translation: "Cod. Variable calculada" },
                { id: "desc_variable_caudal", filter: true, displayed: true, translation: "Variable calculada" },
                { id: "desc_curva", filter: true, displayed: true, translation: "Curva" }
            ],
            crud: {
                title: "Edición de Curva de gasto ",
                editable: true,
                deletable: true,
                insertable: false,
                service: environment.RESTAPI_BASE_URL + "/BasicCrud/save",
                serviceParams: { entity: constants.SGI.ENTITIES.CURVAS_GASTO },
                fields: [
                    { row: 1, id: "cod_curva", label: "Código Curva", pk: true, mandatory: false, sequence: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    { row: 1, id: "desc_curva", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 2, id: "cod_variable_nivel", label: "Cod. Variable medida", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 2, id: "desc_variable_nivel", label: "Desc. Variable medida", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 3, id: "cod_variable_caudal", label: "Cod. Variable calculada", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 3, id: "desc_variable_caudal", label: "Desc. Variable calculada", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 4, id: "data_inicial_format", label: "Fecha inicio", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 4, id: "data_final_format", label: "Fecha fin", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 5, id: "data_alta_format", label: "Fecha alta", pk: false, mandatory: false, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null, disabled: true },
                    { row: 5, id: "ind_activa", label: "Activa", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT, data: this.listActivos, code: 'cod', desc: 'desc', field_in_table: 'desc_activa' },
                    //{ row: 2, id: "desc_variable", label: "Descripcion", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 500, defaultValue: null },
                    /*{ row: 2, id: "cod_punto_medida", label: "Código Punto medida", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 30, defaultValue: null },
                    {
                        row: 3, id: "cod_estacion", label: "Estación", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT, maxLength: 30, defaultValue: null, code: 'cod_estacion', desc: 'desc_estacion', field_in_table: 'desc_estacion',
                        fun: function (params) {
                            return new Promise((resolve, reject) => {
                                params.sort = "desc_estacion desc ";
                                params.searchField = "desc_estacion";
                                params.entity = constants.SGI.ENTITIES.ESTACIONES;
                                params.filters = [];
                                if (params.text) { params.filters = [{ id: "desc_estacion", type: "TEXT", values: params.text }]; }
                                self._basicEntityService.getDataPagination(params).subscribe(
                                    (list: any[]) => {
                                        var temp = [];
                                        for (var item of list) {
                                            temp.push({ "COD": item.cod_estacion, "DESCR": item.desc_estacion + " (" + item.cod_estacion + ")" });
                                        }
                                        resolve(temp);
                                    });
                            });
                        },
                    },
                    { row: 3, id: "formula", label: "Formula", pk: false, mandatory: true, bsWidth: 6, typeData: constantsCommon.TYPE_DATA_FILTER.TYPE_TEXT, type: constantsCommon.TYPE_COMPONENT_FILTER.TEXT_FIELD, maxLength: 4000, defaultValue: null, assistant: true, disabled: true },*/
                ]
            },
            selectable: false,
            utilities: [this.importUtility],
            serverPagination: false,
            pageSize: constants.DEFAULT_LIMIT_PAGINATION,
            sort: {
                active: "desc_estacion",
                direction: "asc"
            }
        };
        this.tablaConstantes = new BasicTableModel(tableConfiguration);
    }
    sendXML() {
        this.imported = false;
        this.curvaGastoService.importXMLData(this.dataImport).subscribe((errores) => {
            if (errores.length > 0) {
                this.msgErrorImport = "No se ha podido carga la curva por lo siguientes errores:"
                this.msgErrorImport += "<ul>"
                for (var error of errores) {
                    this.msgErrorImport += " <li> " + error + "</li>";
                }
                this.msgErrorImport += "</ul>"

            } else {

                this.searchData(false);
                this.imported = true;
            }

        });
    }
    importXML() {
        var self = this;
        this.dataImport = {};
        this.msgErrorImport = null;
        this.visiblePanel = false;
        this.imported = false;
        this.dataImport = {};

        if (this.fileToUpload) {
            var name = this.fileToUpload.name;
            var ext = name.substring(name.lastIndexOf(".") + 1);
            if (ext.toUpperCase() != "XML") {
                this.msgErrorImport = "La extensión del fichero es incorrecta. Sólo ficheros XML son permitidos.";
            }
            else {
                let fileReader = new FileReader();
                fileReader.onload = (e) => {
                    self.parseXML(fileReader.result).then((result: any) => {

                        this.dataImport.codEstacion = result.$.Name;
                        this.dataImport.codVariableCaudal = result.Parameter[0].$.Name;
                        this.dataImport.codVariableNivel = result.Parameter[0].Rc[0].$.SrcParaName;
                        this.dataImport.nameCurva = result.Parameter[0].Rc[0].$.Name;
                        this.dataImport.dateEnd = moment(result.Parameter[0].Rc[0].Validities[0].Transition[0].$.Start, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        this.dataImport.dateStart = moment(result.Parameter[0].Rc[0].Validities[0].Valid[0].$.Start, 'YYYY-MM-DD').format('DD/MM/YYYY');

                        this.dataImport.data_final = moment(result.Parameter[0].Rc[0].Validities[0].Transition[0].$.Start, 'YYYY-MM-DD').format('YYYY-MM-DD');
                        this.dataImport.data_inicial = moment(result.Parameter[0].Rc[0].Validities[0].Valid[0].$.Start, 'YYYY-MM-DD').format('YYYY-MM-DD');

                        this.dataImport.values = [];

                        var values = result.Parameter[0].Rc[0].Version[0].Table[0].TablePoint;

                        for (var value of values) {
                            var v = {
                                caudal: value.$.Y,
                                nivel: value.$.X,
                            };
                            self.dataImport.values.push(v);
                        }
                        this.visiblePanel = true;
                        self.showChart();

                    }).catch((error: any) => {
                        this.msgErrorImport = "<b>Error cargando fichero, posible contenido incorrecto:</b> " + error;
                    });



                }
                fileReader.readAsText(this.fileToUpload);

            }

        }
        /*
        this.curvaGastoService.uploadFile(this.fileToUpload).subscribe(data => {
                
            }, error => {
              console.log(error);
            });    }*/
    }
    handleFileInput(files: FileList) {        
        this.fileToUpload = files.item(0);
        this.visiblePanel = false;
    }
    parseXML(data) {
        return new Promise(resolve => {

            var k: string | number,
                arr = [],
                parser = new xml2js.Parser(
                    {
                        trim: true,
                        explicitArray: true
                    });
            xml2js.parseString(data, function (err, result) {
                var obj = result.Station;
                /*for (k in obj.emp) {
                    var item = obj.emp[k];
                    arr.push({
                        id: item.id[0],
                        name: item.name[0],
                        gender: item.gender[0],
                        mobile: item.mobile[0]
                    });
                }*/
                resolve(obj);
            });
        });
    }
    getVisible() {
        if (this.visiblePanel) {
            return { display: 'block' };
        }
        else {
            return { display: 'none' };
        }
    }
    onExecuteTableUtilityAction(event) {
        var self = this;
        if (event.action == "IMPORT") {
            this.dataImport = {};
            this.msgErrorImport = null;
            this.visiblePanel = false;
            this.fileToUpload = null;
            this.imported = false;
            this.modalService.open(this.modalImportXML, { size: 'lg', centered: true });


        }
        else if (event.action == "SAVE") {

            /*var _json : any[] = [];
            json.forEach(function(element) {
                var object = {};
                var ii = 0;
                for(var i in displayedColumns) {
                    if(['EDIT', 'DELETE', 'SELECT'].includes(displayedColumns[i])) continue;
                    if(displayedColumns[i].startsWith('ROWUTILITY_')) continue;
                    object[ii] = element[displayedColumns[i]];
                    ii++;
                }
                _json.push(object);
            });*/


        }
    }
    configureFilters() {
        var self = this;
        this.filter = new BasicFilter({
            autoClose: false,
            initClosed: false,
            considerEmptyError: false,
            initSearch: true,
            sections: [{ id: 1, name: "Filtros", open: true, gridWidth: 12 }]
        })
        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.VS_MULTI_SELECT,
            group: 1, id: 't1.cod_estacion', label: 'Estación', searchOption: false, width: 400, disabled: false, fun: function (params) {
                return self._datoBrutoService.getDataSetEstacionesPaginacion(params, self);
            }
        });

        this.filter.addFilter({
            dataComponent: constantsCommon.TYPE_COMPONENT_FILTER.SINGLE_SELECT,
            group: 1,
            width: 400,
            id: 'ind_activa',
            label: 'Sólo curvas activas',
            code: 'COD',
            desc: 'DESCR',
            data: [{ COD: 'S', DESCR: 'Sí' }, { COD: 'N', DESCR: 'No' }, { COD: '', DESCR: '--Ver todas--' }]
        });
    }
    onSearch(event) {
        this.filterSelection = event.filterSelection;
        this.searchData(false);
    }
    afterExecuteCrudAction(event) {
        var self = this;
        if (event.action === "openAssistant") {
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {

        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {

        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.DELETE) {


        }
    }


    beforeExecuteCrudAction(event) {
        /*if (event.action === constantsCommon.BASIC_CRUD_MODE.INSERT) {
            this.tokensFormula = null;
            this.cod_variable = null;
            this.getNextId().then((id) => {
                this.tablaConstantes.crudModel.elementMap.cod_variable.value = id;
                this.cod_variable = id;
            });
        }
        else if (event.action === constantsCommon.BASIC_CRUD_MODE.EDITION) {
            this.tokensFormula = null;
            this.cod_variable = this.tablaConstantes.crudModel.rowSelected.cod_variable;
        }*/
    }


    exportarExcel() {
        var values = [];
        for (var value of this.valuesCurva) {
            values.push({ nivel: value.nivel, caudal: value.caudal });
        }
        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(values, { skipHeader: true });
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'csv', type: 'array' });
        const data: Blob = new Blob([excelBuffer], { type: EXCEL_TYPE });
        FileSaver.saveAs(data, 'ipol_' + this.rowSelected.desc_curva + '.' + 'csv');
    }


    onExecuteTableRowUtilityAction(event) {
        this.rowSelected = event.row;
        if (event.action == "ExportCurva") {
            this.curvaGastoService.getInterpolacion({ cod_curva: event.row.cod_curva }).subscribe((values) => {
                this.valuesCurva = values;
                this.exportarExcel();
            });
        }
        else if (event.action == "ViewCurva") {
            this.curvaGastoService.getInterpolacion({ cod_curva: event.row.cod_curva }).subscribe((values) => {
                this.valuesCurva = values;
                this.modalService.open(this.modalValores, { size: 'lg', centered: true });
                this.showChart2();
            });
        }
        else if (event.action == "Recalcular") {
            this.fechaIniRecalculo = moment(this.rowSelected.data_inicial_format, "DD/MM/YYYY").toDate();
            this.fechaFinRecalculo = moment(this.rowSelected.data_final_format, "DD/MM/YYYY").toDate();
            //this.fechaFinRecalculo = this.rowSelected.data_final_format;
            this.errorFechaFinRecalculo = null;
            this.errorFechaFinRecalculo = null;
            this.recalculando = false;
            this.modalService.open(this.modalRecalculo, { size: 'lg', centered: true });
        }


    }

    validateRecalculo() {
        this.errorFechaFinRecalculo = null;
        this.errorFechaFinRecalculo = null;
        if (!this.fechaIniRecalculo || this.fechaIniRecalculo == "") {
            this.errorFechaIniRecalculo = "Campo obligatorio";
        }
        if (!this.fechaFinRecalculo || this.fechaFinRecalculo == "") {
            this.errorFechaFinRecalculo = "Campo obligatorio";
        }
        if (this.errorFechaIniRecalculo || this.errorFechaFinRecalculo) {
            return false;
        }
        return true;
    }
    confirmRecalculo(modal) {
        if (this.validateRecalculo()) {
            this.recalculando = true;
            this.curvaGastoService.calcula({                
                fecha_ini: moment(this.fechaIniRecalculo).format("YYYY-MM-DD"), 
                fecha_fin: moment(this.fechaFinRecalculo).format("YYYY-MM-DD"),
                cod_curva: this.rowSelected.cod_curva, 
                cod_estacion : this.rowSelected.cod_estacion,
                cod_variable_caudal : this.rowSelected.cod_variable_caudal,
                cod_variable_nivel : this.rowSelected.cod_variable_nivel
            }).subscribe((result) => {
                this.recalculando = false;
                modal.dismiss('Cross click');                
                this.rowSelected.data_inicial_format = moment(this.fechaIniRecalculo).format("DD/MM/YYYY");
                this.rowSelected.data_final_format   = moment(this.fechaFinRecalculo).format("DD/MM/YYYY");
            });

            /*
            this.formulaService.setRecalculo({
                cod_variable: this.variableRecalculo.cod_variable,
                fecha_ini: moment(this.fechaIniRecalculo).format("YYYY-MM-DD"),
                fecha_fin: moment(this.fechaFinRecalculo).format("YYYY-MM-DD"),
            }).subscribe(list => {
                modal.dismiss('Cross click');
            });*/
        }
    }
    addToFormula(text) {
        if (!this.formulaEdition)
            this.formulaEdition = "";
        this.formulaEdition += " " + text + " ";
    }

    showChart() {
        var self = this;
        var data = [];
        for (var measure of this.dataImport.values) {
            data.push({
                x: measure.caudal,
                y: measure.nivel
            })
        }
       var  options =  {
        type: 'line',
           data: {
               datasets: [{
                   label: 'Curva de gasto',
                   data: data
               }]
           },
           options: {
               scales: {
                   xAxes: [{
                       type: 'linear',
                       position: 'bottom',
                       scaleLabel: {
                        display: true, 
                        labelString: 'Caudal (m³/s)'
                    },                       
                   }],
                   yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Lectura (m)'
                    }
                    }]                                   
               }
           }
       };
        this.chart = new Chart('graphContainer', options);
        var overlay: any = document.getElementById('overlay');
        var canvas: any = document.getElementById('graphContainer');
        overlay.width = canvas.width;
        overlay.height = canvas.height;
        this.chart.update();

    }

    showChart2() {
        var data = [];
        for (var measure of this.valuesCurva) {
            data.push({
                x: measure.caudal,
                y: measure.nivel
            })
        }
       var  options =  {
        type: 'line',
           data: {
               datasets: [{
                   label: 'Curva de gasto',
                   data: data
               }]
           },
           options: {
               scales: {
                   xAxes: [{
                       type: 'linear',
                       position: 'bottom',
                       scaleLabel: {
                        display: true, 
                        labelString: 'Caudal (m³/s)'
                    },                       
                   }],
                   yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Lectura (m)'
                    }
                    }]                                   
               }
           }
       };
         

        
        this.chart2 = new Chart('graphContainer2', options);
        var overlay: any = document.getElementById('overlay2');
        var canvas: any = document.getElementById('graphContainer2');
        overlay.width = canvas.width;
        overlay.height = canvas.height;
        this.chart2.update();

    }

}
