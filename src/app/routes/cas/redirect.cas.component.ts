import { Component, OnInit } from '@angular/core';
import { SecurityService } from './../../services/common/security.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'redirect-cas-component',
    templateUrl: './redirect.cas.component.html',    
    providers: []
})
export class RedirectCasComponent implements OnInit {
    loading : boolean = true;
    error;
    urlBack = environment.CAS_SERVER + "/logout?url=" + environment.CAS_SERVICE_SGI;;
    constructor(private _router: Router, public _securityService: SecurityService) {
    }
    ngOnInit() {
        this._securityService.closeLocalSesion();
        this._securityService.checkTicketFromURL().then((profile) => {
            this._router.navigate(['/home']);        
        }).catch(error => {  
            console.log(error);
            this.loading = false;
            this.error = error;
        });        
    }

}
   