import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LayoutComponent } from './../layout/layout.component';
import { DatoBrutoService } from '../services/arca_lab/data.bruto.service';
import { CommonModule } from '@angular/common';
import { DatoDiarioService } from '../services/arca_lab/data.diario.service';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { MomentModule } from 'ngx-moment';
import { environment } from './../../environments/environment';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { MatExpansionModule } from '@angular/material/expansion';
import { LayoutCommonService } from '@arca/common';
import { ArcaCommonModule } from '@arca/common';
import { AngularOpenlayersModule } from "ngx-openlayers";
import { A11yModule } from '@angular/cdk/a11y';
import { BidiModule } from '@angular/cdk/bidi';
import { ObserversModule } from '@angular/cdk/observers';
import { OverlayModule } from '@angular/cdk/overlay';
import { PlatformModule } from '@angular/cdk/platform';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { SGIModule } from './sgi/sgi.module';


import {
    MatFormFieldModule, MatSelectModule,
    MatInputModule, MatDatepickerModule,  MatNativeDateModule,
    MatSortModule, MatTableModule, MatPaginatorModule, MatTooltipModule,
    MatCheckboxModule, MatButtonModule,
    MatButtonToggleModule, MatTabsModule, MatIconModule,
    MatMenuModule,
} from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
@NgModule({
    imports: [
        A11yModule,
        BidiModule,
        ObserversModule,
        OverlayModule,
        PlatformModule,
        PortalModule,
        ScrollDispatchModule,
        CdkStepperModule,
        CdkTableModule,
        CdkTreeModule,
        SharedModule,
        CommonModule,
        TranslateModule,
        FormsModule,
        MomentModule,
        MatFormFieldModule, MatSelectModule,
        MatInputModule, MatDatepickerModule, MatNativeDateModule,
        MatSortModule, MatTableModule, MatPaginatorModule, MatTooltipModule,
        MatCheckboxModule, MatButtonModule,
        MatButtonToggleModule, MatTabsModule, MatExpansionModule,
        MatMenuModule, MatIconModule,
        NgbModule, ChartsModule, NgMultiSelectDropDownModule.forRoot(),
        AngularMultiSelectModule,
        AngularOpenlayersModule,
        SGIModule,
        ArcaCommonModule.forRoot({
            auditoriaEndPoint: environment.RESTAPI_BASE_URL,
            laboratorioEndPoint: environment.RESTAPI_BASE_URL,
            auditaActivated: false
        })
    ],
    declarations:
        [
        ],
    exports: [
        RouterModule
    ],
    providers: [LayoutCommonService,  DatoBrutoService,DatoDiarioService]
})

export class RoutesModule {
    constructor() {
    }
}
